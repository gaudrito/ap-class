unitsize(1cm);
import roundedpath;
settings.tex = "pdflatex";

pair rdev(real r) {
    return ((unitrand(),unitrand())-(0.5,0.5)) * r;
}

path rect(pair a, pair b, real r = 0.2) {
    return a+rdev(r) -- (a.x, b.y)+rdev(r) -- b+rdev(r) -- (b.x, a.y)+rdev(r) -- cycle;
}

path rellipse(pair a, pair b, real r = 0.2) {
    a = a+rdev(r);
    b = b+rdev(r);
    pair c = (a+b)/2;
    pair l = (b-a)/2;
    return shift(c)*rotate(20*r)*xscale(l.x)*yscale(l.y)*unitcircle;
}

void circledtext(picture pic, real size, string txt, pair a, pair b) {
    filldraw(pic, rellipse(a, b), white, black+1);
    label(pic, scale(size)*("\textsf{"+txt+"}"), (a+b)/2);
}

picture pic;
unitsize(pic, 1cm);
draw(pic, rect((-11.7,-0.2), (11.7,15.2), 0), invisible);

label(pic, scale(1.5)*"\textsf{TM/RAM}", (-0.5, 9.5));
label(pic, rotate(90)*scale(1)*"\textsf{uring}\phantom{dp}", (-1.6, 9.75), N);
label(pic, rotate(90)*scale(1)*"\textsf{achine}\phantom{dp}", (-1.12, 9.75), N);
label(pic, rotate(90)*scale(1)*"\textsf{andom}\phantom{dp}", (-0.34, 9.75), N);
label(pic, rotate(90)*scale(1)*"\textsf{ccess}\phantom{dp}", (0.07, 9.75), N);
label(pic, rotate(90)*scale(1)*"\textsf{achine}\phantom{dp}", (0.53, 9.75), N);
shipout("machines0", pic);

circledtext(pic, 1.5, "deterministic", (-10, 10), (-3, 12));
shipout("machines1", pic);
circledtext(pic, 1.1, "probabilistic", (-2.5, 12), (2.5, 13.5));
shipout("machines2", pic);
circledtext(pic, 0.8, "quantum", (5.5, 11.5), (9.5, 12.5));
shipout("machines3", pic);
circledtext(pic, 1.0, "...", (2.5, 10), (5.5, 10.7));
shipout("machines4", pic);

label(scale(2.5)*"\textsf{sequential}", (-5, 14.5));
filldraw(roundedpath(rect((-11,9), (11,14)), 0.2), palegray, black+2);
add(pic);
shipout("machines5");

label(scale(2.5)*"\textsf{concurrent}", (5, 0.5));
filldraw(roundedpath(rect((-11.5,1), (11.5,8)), 0.2), palegray, black+2);
shipout("machines6");

filldraw(roundedpath(rect((-11,1.3), (-0.5,7.7)), 0.2), white, black+1);
label(scale(1.8)*"\textsf{parallel}", (-6,7.3));
label(scale(1.4)*"\textsf{communication by shared memory}", (-6,5.6));
label(scale(1.4)*"\textsf{usually synchronous}", (-6,4.4));
label(scale(1.4)*"\textsf{reliable}", (-6,3.2));
label(scale(1.4)*"\textsf{PRAM model}", (-6,2));
shipout("machines7");

picture p2;
unitsize(p2, 1cm);
filldraw(p2, roundedpath(rect((0.5,1.3), (11,7.7)), 0.2), palered, darkred+1);
label(p2, scale(1.8)*"\textsf{distributed}", (6,7.3), darkred);
label(p2, scale(1.4)*"\textsf{communication by messages}", (6,5.6), darkred);
label(p2, scale(1.4)*"\textsf{usually asynchronous}", (6,4.4), darkred);
label(p2, scale(1.4)*"\textsf{unreliable}", (6,3.2), darkred);
label(p2, scale(1.4)*"\textsf{event model}", (6,2), darkred);
add(p2);
shipout("machines8");
label(p2, scale(0.3)*graphic("badge.png"), (9.4,2.8));
label(p2, rotate(20)*scale(3)*"\textsf{AC}", (9.4,2.8));
add(p2);
shipout("machines9");
shipout("machines10", p2);
