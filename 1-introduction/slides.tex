\documentclass[10pt,handout]{beamer} %,aspectratio=169,handout
	\usetheme{CambridgeUS} %CambridgeUS, Goettingen, Singapore, Madrid
	\setbeamertemplate{footline}[frame number]
	\setbeamercovered{transparent}
	\beamertemplatenavigationsymbolsempty

\usepackage{multimedia}
\usepackage{tabularx}
\usepackage{listings}
\usepackage{forloop}
\usepackage{mathtools}
\usepackage{tikz}
\newcolumntype{M}[1]{>{\centering\arraybackslash}m{#1}}
\setlength\tabcolsep{4pt}

\input{../macros}

\newcounter{prog}
\newcommand{\progress}[2]{$\forloop{prog}{0}{\value{prog} < #1}{\bullet}\forloop{prog}{0}{\value{prog} < #2}{\circ}$}
\newcounter{secslide}
\newcounter{sectotal}
\newcounter{secother}
\newcommand{\resetsec}[1]{\setcounter{secslide}{0}\setcounter{sectotal}{#1}}
\newcommand{\slidecount}{\addtocounter{secslide}{1}\setcounter{secother}{\value{sectotal} - \value{secslide}}\subsection[\progress{\value{secslide}}{\value{secother}}]{slide}}

\lstdefinelanguage{hfc}{
	basicstyle=\lst@ifdisplaystyle\small\fi\ttfamily,
	frame=single,
	basewidth=0.5em,
	sensitive=true,
	morestring=[b]",
	morecomment=[l]{//},
	morecomment=[n]{/*}{*/},
	commentstyle=\color{OliveGreen},
	keywordstyle=\color{blue}\textbf, keywords={def}, otherkeywords={=>},
	keywordstyle=[2]\color{red}\textbf, keywords=[2]{rep,nbr,if,let,in},
	keywordstyle=[3]\color{violet}, keywords=[3]{mux,nbrRange,sumHood,countHood,everyHood,anyHood,minHood,maxHood,foldHood,maxHoodPlusSelf},
	keywordstyle=[4]\color{orange}\textbf, keywords=[4]{spawn,share},
	keywordstyle=[5]\color{blue}, keywords=[5]{false,true,infinity}
}

\definecolor{darkgreen}{rgb}{0.0, 0.5, 0.0}
\definecolor{darkred}{rgb}{0.5, 0.0, 0.0}
\definecolor{darkblue}{rgb}{0.2, 0.2, 0.7}

\newcommand{\highlight}[1]{\textcolor{darkblue}{#1}}
\newcommand{\superlight}[1]{\textcolor{darkred}{#1}}
\newcommand{\greenlight}[1]{\textcolor{darkgreen}{#1}}
\newcommand{\DOI}[1]{\href{http://www.doi.org/#1}{\underline{#1}}}
\newcommand{\ACMDOI}[1]{\href{https://dl.acm.org/doi/#1}{\underline{#1}}}
\newcommand{\CITE}[5]{\ssymbol{#1}[#2. \emph{#3.} #4. \DOI{#5}]}

\theoremstyle{plain}
	\newtheorem{proposition}[theorem]{Proposition}
\theoremstyle{remark}
	\newtheorem{question}[theorem]{Question}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{Aggregate Programming for the Internet of Things}
\author{Giorgio Audrito, Ferruccio Damiani}
\institute{{\small Dipartimento di Informatica\\[3pt] Universit\`a degli Studi di Torino}}
%\date{}
\titlegraphic{
	%\raisebox{-0.5\height}{\includegraphics[width=2.0cm]{../images/logo_horizon.jpg}} \quad
	\raisebox{-0.5\height}{\includegraphics[width=2.0cm]{../images/logo_torino.pdf}} \quad
	%\raisebox{-0.5\height}{\includegraphics[width=2.0cm]{../images/logo_sanpaolo.png}}
}

\begin{document}
\lstset{language={hfc}}

\section[~]{Title}
	\begin{frame}
		\titlepage
	\end{frame}

	\begin{frame}{Conceptual map}
		\vspace{-3mm}
		\begin{block}{}
		\begin{center}
			\includegraphics[height=0.86\textheight]{../images/map0.pdf}
		\end{center}
		\end{block}
	\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\resetsec{6}
\section{What is Aggregate Computing about?}

	\slidecount{}
	\begin{frame}{Outline}
		\begin{itemize}
			\item \highlight{What is Aggregate Computing about?}
			\begin{itemize}
				\item Which computing systems?
				\item Which frameworks?
				\item What kind of problems?
			\end{itemize}
			\bigskip
			\item Why care about Aggregate Computing?
			\bigskip
			\item So, what Aggregate Computing is?
		\end{itemize}
	\end{frame}

	\slidecount{}
	\begin{frame}{Which computing systems?}
		\includegraphics<10>[height=8.0cm]{img/machines9.pdf}
		\includegraphics<1>[height=8.0cm]{img/machines0.pdf}
		\includegraphics<2>[height=8.0cm]{img/machines1.pdf}
		\includegraphics<3>[height=8.0cm]{img/machines2.pdf}
		\includegraphics<4>[height=8.0cm]{img/machines3.pdf}
		\includegraphics<5>[height=8.0cm]{img/machines4.pdf}
		\includegraphics<6>[height=8.0cm]{img/machines5.pdf}
		\includegraphics<7>[height=8.0cm]{img/machines6.pdf}
		\includegraphics<8>[height=8.0cm]{img/machines7.pdf}
		\includegraphics<9>[height=8.0cm]{img/machines8.pdf}
	\end{frame}

	\slidecount{}
	\begin{frame}{Which frameworks?}
		\begin{block}{}
			\centering
			\renewcommand{\arraystretch}{1.8}
			\begin{tabular}{M{0.2\textwidth}|M{0.35\textwidth}|M{0.35\textwidth}}
				& \textbf{centralised} & \textbf{decentralised} \\ \hline
				\emph{approach} & few special central units responsible of computing and deciding, most units only sensing and actuating & all units are equal \\ \hline \pause
				\emph{collaboration} & easy & hard \\ \hline \pause
				\emph{robustness} & lower & higher \\ \hline \pause
				\emph{privacy} & lower & higher \\ \hline
			\end{tabular}
		\end{block} \pause
		\begin{tikzpicture}[overlay]
			\draw<5->[red,ultra thick,rounded corners] (0.62\textwidth,0.07\textheight) rectangle (\textwidth,0.68\textheight);
		\end{tikzpicture} \\[-5pt]
		\centering
		\textbf{AC is a decentralised framework}
	\end{frame}

	\slidecount{}
	\begin{frame}{Which frameworks?}
		\begin{block}{}
			\centering
			\renewcommand{\arraystretch}{1.8}
			\begin{tabular}{M{0.2\textwidth}|M{0.35\textwidth}|M{0.35\textwidth}}
				& \textbf{``query-based''} & \textbf{``data-based''} \\ \hline
				\emph{approach} & computing required results whenever queries are issued & maintaining relevant data always updated and \newline locally accessible \\ \hline \pause
				\emph{requirements} & none & predictable queries \\ \hline \pause
				\emph{resource usage} & \qquad on demand \newline (best for fewer queries) & \qquad constant \newline (best for frequent queries) \\ \hline \pause
				\emph{responsiveness} & lower: requires routing & higher: data locally available \\ \hline \pause
				\emph{quality} & higher: fresh results & lower: older results \\ \hline
			\end{tabular}
		\end{block} \pause
		\begin{tikzpicture}[overlay]
			\draw<6->[red,ultra thick,rounded corners] (0.62\textwidth,0.07\textheight) rectangle (\textwidth,0.77\textheight);
		\end{tikzpicture} \\[-5pt]
		\centering
		\textbf{AC is designed for ``data-based'' frameworks} \\ \pause
		(extensions handling query-based frameworks are possible)
	\end{frame}

	\slidecount{}
	\begin{frame}[t]{What kind of problems?}
		\begin{block}{\only<8>{(not only)} spatial computing problems \phantom{(dp)}}
			position and time of devices are essential inputs of the computation
		\end{block} \pause

		\centering
		\raisebox{-0.5\height}{\includegraphics<1| handout:0>[width=5.0cm]{img/setting-swarm.jpg}}
		\raisebox{-0.5\height}{\includegraphics<1| handout:0>[width=5.0cm]{img/setting-drones.jpg}} \only<1| handout:0>{\\[2pt]}
		\raisebox{-0.5\height}{\includegraphics<1| handout:0>[width=5.0cm]{img/setting-smartcity.jpg}}
		\raisebox{-0.5\height}{\includegraphics<1| handout:0>[width=5.0cm]{img/setting-wsn.jpg}}

		\includegraphics<2| handout:0>[height=4cm]{img/algorithm-distance.png}
		\includegraphics<3| handout:0>[height=4cm]{img/algorithm-collection.png}
		\includegraphics<4| handout:0>[height=4cm]{img/algorithm-voronoi.png}
		\includegraphics<5| handout:0>[height=4cm]{img/algorithm-channel.png}
		\includegraphics<6| handout:0>[height=4cm]{img/algorithm-dispersion.png}
		\includegraphics<7| handout:0>[height=4cm]{img/algorithm-formation.png}
		\includegraphics<8>[height=4cm]{img/network.pdf}

		\begin{block}{}
			distance estimation, \pause data summarisation (event detection), \pause \\
			selecting areas (network partitioning, \pause channel establishment\ldots), \pause \\
			inducing shapes (crowd dispersion, \pause formation control\ldots)\ldots \pause \highlight{and others!}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{So, what is Aggregate Computing about?}
		\vspace{-2pt}
		\begin{block}{}
			\begin{itemize}
				\item Which computing systems? \highlight{$\longrightarrow$ distributed systems}
				\item Which frameworks? \highlight{$\longrightarrow$ decentralized, (mostly) data-based}
				\item What kind of problems? \highlight{$\longrightarrow$ born for (but not limited to) spatial computing }
			\end{itemize}
		\end{block}

		\begin{center}
			\includegraphics[height=4cm]{img/network.pdf}
		\end{center} \pause
		\vspace{-5pt}

		\begin{block}{is a recent topic\ldots}
			\begin{itemize}
				\item \superlight{cons:} not standard, not well-known, small research group
				\item \greenlight{pros:} plenty of things where research is needed
			\end{itemize}
		\end{block}
	\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\resetsec{8}
\section{Why care about Aggregate Computing?}

	\slidecount{}
	\begin{frame}{Outline}
		\begin{itemize}
			\item What is Aggregate Computing about?
			\bigskip
			\item \highlight{Why care about Aggregate Computing?}
			\begin{itemize}
				\item Why care for distributed systems at all?
				\item Why are distributed systems hard to deal with?
				\item How Aggregate Computing can help?
			\end{itemize}
			\bigskip
			\item So, what Aggregate Computing is?
		\end{itemize}
	\end{frame}

	\slidecount{}
	\begin{frame}{Why care for distributed systems at all?}
		\begin{block}{}
			\small
			\begin{itemize}\setlength\itemsep{0pt}
				\item increasing availability of wearable / mobile / embedded / flying devices
				\item increasing availability of heterogeneous wireless connectivity
				\item increasing availability of computational resources (device/edge/cloud)
				\item increasing production/analysis of data, everywhere, anytime
			\end{itemize}
		\end{block}
		\smallskip

		\centering
		\raisebox{-0.5\height}{\includegraphics[width=4.5cm]{img/setting-swarm.jpg}}
		\raisebox{-0.5\height}{\includegraphics[width=4.5cm]{img/setting-drones.jpg}} \\[2pt]
		\raisebox{-0.5\height}{\includegraphics[width=4.5cm]{img/setting-traffic.jpg}}
		\raisebox{-0.5\height}{\includegraphics[width=4.5cm]{img/setting-crowd.jpg}}
	\end{frame}

	\slidecount{}
	\begin{frame}{Why are distributed systems hard to deal with?}
		\begin{block}{diverse heterogeneous entities}
			\begin{itemize}
				\item different computing power
				\item sensing and actuation capabilities
			\end{itemize}
		\end{block} \pause
		\medskip

		\begin{center}
			\raisebox{-0.5\height}{\includegraphics[height=3cm]{img/challenge-heterogeneous.jpg}}
		\end{center}

		\begin{block}{we need\ldots}
			\begin{itemize}
				\item device \highlight{abstraction}
				\item multi-platform \highlight{frameworks}
				\item not too bad so far\ldots
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{Why are distributed systems hard to deal with?}
		\begin{block}{data security and privacy} \pause
			\begin{itemize}
				\item localised data may be highly personal \pause
				\item cryptography has got it covered, but hacker attacks do happen \pause
				\item can we really trust big companies having our data?
			\end{itemize}
		\end{block} \pause

		\begin{center}
			\raisebox{-0.5\height}{\includegraphics[height=3cm]{img/challenge-privacy.jpg}}
		\end{center}

		\begin{block}{in a decentralised system, we can\ldots}
			\begin{itemize}
				\item keep our data \highlight{locally} \pause
				\item where communication is needed, share \highlight{locally aggregated} neighbour data
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{Why are distributed systems hard to deal with?}
		\begin{block}{predictable code re-use}
			\begin{itemize}
				\item can break down complexity of problems \pause
				\item needs to avoid unwanted interferences
			\end{itemize}
		\end{block} \pause

		\begin{center}
			\raisebox{-0.5\height}{\includegraphics[height=3cm]{img/challenge-reuse.jpg}}
		\end{center}

		\begin{minipage}{0.48\linewidth}
			\begin{block}{in a sequential system\ldots}
				\begin{itemize}\setlength\itemsep{0pt}
					\item machine code is hard to re-use \pause
					\item \highlight{abstractions:} objects, mixins\ldots \pause
					\item all boil down to \highlight{functions} and composition
				\end{itemize}
			\end{block}
		\end{minipage} ~~ \pause
		\begin{minipage}{0.48\linewidth}
			\begin{block}{in a distributed system\ldots}
				\begin{itemize}\setlength\itemsep{0pt}
					\item need to \highlight{match} messages \pause
					\item not good with \highlight{process calculi} \pause
					\item \ldots how to handle multiple instances of same algorithm?
				\end{itemize}
			\end{block}
		\end{minipage}
	\end{frame}

	\slidecount{}
	\begin{frame}{Why are distributed systems hard to deal with?}
		\begin{block}{dynamic goals and environment}
			\begin{itemize}\setlength\itemsep{1pt}
				\item low-end devices may be unreliable\ldots \pause
				\item as low-power wireless communications between them \pause
				\item environmental factors may affect the computation
			\end{itemize}
		\end{block} \pause

		\begin{center}
			\raisebox{-0.5\height}{\includegraphics[height=3cm]{img/challenge-dynamic.jpg}}
		\end{center}

		\begin{minipage}{0.48\linewidth}
			\begin{block}{in a centralised system\ldots}
				\begin{itemize}\setlength\itemsep{0pt}
					\item changes in edge devices \greenlight{OK}
					\item changes in central units \superlight{BAD}
					\item connection loss \superlight{BAD}
				\end{itemize}
			\end{block}
		\end{minipage} ~~ \pause
		\begin{minipage}{0.48\linewidth}
			\begin{block}{in a decentralised system\ldots}
				\begin{itemize}\setlength\itemsep{0pt}
					\item any change \greenlight{OK} but \superlight{HARD} \pause
					\item need \highlight{re-usable} algorithms\ldots
					\item with precise \highlight{adaptivity guarantees}
				\end{itemize}
			\end{block}
		\end{minipage}
	\end{frame}

	\slidecount{}
	\begin{frame}{Why are distributed systems hard to deal with?}
		\begin{block}{collaboration vs selfishness}
			\begin{itemize}\setlength\itemsep{1pt}
				\item many devices and goals to be achieved
				\item competitive behaviour may prevent to achieve them \pause
				\item much easier in centralised systems
			\end{itemize}
		\end{block} \pause

		\begin{center}
			\raisebox{-0.5\height}{\includegraphics[height=3cm]{img/challenge-collaboration.jpg}}
		\end{center}

		\begin{block}{in a decentralised system\ldots}
			\begin{itemize}\setlength\itemsep{1pt}
				\item we want to achieve a given \highlight{global} behaviour, \pause
				\item by programming \highlight{local} interactions \pause
				\item seek for an automatic \highlight{global to local} translation
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{How Aggregate Computing can help?}
		\begin{minipage}{0.45\linewidth}
			\begin{block}{challenges are...}
				\begin{itemize}
					\item diverse \highlight{heterogeneous} entities
					\onslide<2->{\item data security and \highlight{privacy}}
					\onslide<3->{\item predictable code \highlight{re-use}}
					\onslide<4->{\item \highlight{dynamic} goals and environment}
					\onslide<5->{\item \highlight{collaboration} vs selfishness}
				\end{itemize}
			\end{block}
		\end{minipage} ~~
		\begin{minipage}{0.51\linewidth}
			\begin{block}{aggregate computing provides...}
				\begin{itemize}
					\item a \highlight{multi-platform} framework
					\onslide<2->{\item \ldots which is \highlight{decentralized}}
					\onslide<3->{\item \highlight{composable} abstraction of \highlight{behaviour}}
					\onslide<4->{\item \ldots and \highlight{guarantees}}
					\onslide<5->{\item with \highlight{global-to-local} translation}
				\end{itemize}
			\end{block}
		\end{minipage}
		\medskip

		\centering
		\raisebox{-0.5\height}{\includegraphics<1| handout:0>[height=4cm]{img/challenge-heterogeneous.jpg}}
		\raisebox{-0.5\height}{\includegraphics<2| handout:0>[height=4cm]{img/challenge-privacy.jpg}}
		\raisebox{-0.5\height}{\includegraphics<3| handout:0>[height=4cm]{img/challenge-reuse.jpg}}
		\raisebox{-0.5\height}{\includegraphics<4| handout:0>[height=4cm]{img/challenge-dynamic.jpg}}
		\raisebox{-0.5\height}{\includegraphics<5| handout:0>[height=4cm]{img/challenge-collaboration.jpg}}
	\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\resetsec{6}
\section{So, what Aggregate Computing is?}

	\slidecount{}
	\begin{frame}{Outline}
		\begin{itemize}
			\item What is Aggregate Computing about?
			\bigskip
			\item Why care about Aggregate Computing?
			\bigskip
			\item \highlight{So, what Aggregate Computing is?}
			\begin{itemize}
				\item A computation model for proximity-based networks
				\item What it provides?
				\item Properties
				\item How are programs built?
				\item How developing works?
			\end{itemize}
		\end{itemize}
	\end{frame}

	\slidecount{}
	\begin{frame}[t]{A computation model for proximity-based networks}
%		\begin{small}
			\vspace{-3mm}
		\begin{block}{Network of (possibly mobile) devices}
			\begin{itemize}
				\item
				dynamic topology --- induced by (physical or logical) proximity of devices
				\item
				each device can receive/send message to/from devices in the neighbourhood
			\end{itemize}
			\onslide<2->{where \highlight{each device executes a computation/coordination service $S$}}
		\end{block}

		\onslide<3->{\begin{block}{}
			\highlight{To execute $S$,} each device (asynchronously periodically) \superlight{fires}:
			\begin{enumerate}
				\item
				collects local (sensor, DB,...)  data and received messages
				\item
				executes a program \onslide<4->{--- \superlight{the same for all devices}$^1$}
				\item
				sends messages (and performs some actuation)
			\end{enumerate}
		\end{block}}

%			\vspace{1.3mm}
%
%			\onslide<5->{\superlight{Event}: beginning of a firing  --- event $\epsilon$ happens on device $\delta(\epsilon)$}
%
%			\vspace{1mm}
%
%			\onslide<6->{\superlight{Supplier event}:
%			 $\epsilon'$ supplier of  $\epsilon$ ($\epsilon'\rightsquigarrow\epsilon$) iff at  $\epsilon$:
%			\begin{itemize}
%			\item[]
%			a (not expired) message sent by $\epsilon'$ was the last from $\delta(\epsilon')$ able to reach $\delta(\epsilon)$
%			\end{itemize}
%			}
%		\end{small}
		------------\\
		\begin{scriptsize}
			\onslide<4->{
			$^1$ Supports \superlight{integrated development of every part of a distribute application} -- cf.\ \emph{macroprogramming} and \emph{multi-tier programming}:
			\\
			\highlight{$\bullet$ R. Newton and M.Welsh (2004). \emph{Region streams: Functional macroprogramming for sensor
			networks}. In: 1st Workshop on Data Management for Sensor Networks (MSN 2004), pp. 78–87. \url{https://doi.org/10.1145/1052199.1052213}}
			\\
			\highlight{$\bullet$ P. Weisenburger, M. Köhler, G. Salvaneschi (2018). \emph{Distributed system development
			with Scalaloci.} In: ACM Programming Languages, 2 (OOPSLA):129:1–129:30, 2018. \url{https://doi.org/10.1145/3276499} \\
			}}
		\end{scriptsize}
	\end{frame}

%	\slidecount{}
%	\begin{frame}{What it provides?}
%		\begin{block}{1. Formal language and models for studying algorithms and properties}
%			\centering
%			\small
%			$
%			\e \; \BNFcce \;  \xname \; \BNFmid \; \fvalue \; \BNFmid \; \dcOf{\dc}{\overline\e} \; \BNFmid \; \bname  \; \BNFmid \; \fname \; \BNFmid \; (\overline{\xname}) \; \toSymK{\e} \; \BNFmid \; \e(\overline{\e}) \; \BNFmid \; \nbrK\{\e\} \; \BNFmid \; \repK(\e)\{ \xname \toSymK{\e} \}\ldots
%			$
%		\end{block} \pause
%		\begin{block}{2. Language implementations (Proto, Protelis, ScaFi, FC++)}
%			\centering
%			\visible<2->{\raisebox{-0.5\height}{\includegraphics[height=2.2cm]{img/protelis.png}}}
%		\end{block} \pause
%		\begin{block}{3. Network simulator (Alchemist) or real deployment (BBN Technologies)}
%			\centering
%			\visible<3->{\raisebox{-0.5\height}{\includegraphics[height=1.9cm]{img/alchemist.png}}}
%		\end{block}
%	\end{frame}

	\slidecount{}
	\begin{frame}{What it provides?}
		\vspace{-4pt}
		1. Minimal core  language (field calculus)
		\vspace{-5pt}

		\begin{minipage}{0.54\linewidth}
			\begin{scriptsize}
			\begin{itemize}
				\item[]
				\item
				\superlight{Aggregate Programming = Macroprogramming + Field-based coordination}
				\item
				formal semantics
				\item
				properties
			\end{itemize}
			\end{scriptsize}
		\end{minipage}
		\begin{minipage}[t]{0.45\linewidth} \centering
			\vspace{-11mm}
			\raisebox{-0.5\height}{\includegraphics[height=1.9cm]{img/fc.png}}
		\end{minipage}

		\onslide<2->{
		\vspace{1pt}
		2. Implementations
		\vspace{-3pt}

		\begin{minipage}{0.4\linewidth}
			\begin{scriptsize}
			\begin{itemize}
				\item[]
				\item
				\emph{Protelis}: a Java external DSL \url{http://protelis.github.io}
				\item
				\emph{ScaFi}: a Scala internal DSL (library) \url{https://scafi.github.io}
				\item
				\superlight{\emph{FCPP}: a C++ internal DSL (library) \url{https://fcpp.github.io}}
				\item[]
			\end{itemize}
			\end{scriptsize}
		\end{minipage}
		\begin{minipage}[t]{0.59\linewidth} \centering
			\vspace{-10mm}
			\visible<3->{\raisebox{-0.5\height}{\includegraphics[height=1.9cm]{img/fcpp-exa.png}}}
		\end{minipage}
		}
		\vspace{-8pt}

		\onslide<4->{
		\vspace{0pt}
		3. Alchemist simulator (UniBo), FCPP simulator and deployment (UniTo), deployment (Raytheon BBN Technologies)$^1$

		\begin{center}
			\visible<4->{\raisebox{-0.5\height}{\includegraphics[height=1.9cm]{img/alchemist.png}}}
		\end{center}
		}

		------------\\
		\begin{scriptsize}
			\visible<4->{$^1$
			\highlight{DARPA project (\url{http://www.swarmtactics.com})}}\\
		\end{scriptsize}
	\end{frame}

		\slidecount{}
	\begin{frame}{How are programs built?}
		\centering
		\raisebox{-0.5\height}{\includegraphics[height=7cm]{img/layers.pdf}}
		\medskip

		stacked layers of abstraction thanks to \highlight{composition}
	\end{frame}

	\slidecount{}
	\begin{frame}[t]{Properties}
		\begin{itemize}
			\item
			\superlight{Self-stabilisation}: computation resilient to changes in devices state and network  topology
			\begin{footnotesize}(behaviour is guaranteed to eventually attain a correct and stable final state despite any transient perturbation in state or topology)\end{footnotesize}$^1$
			\onslide<2->{\item
			\superlight{Eventual consistency}: computation  independent from device number and density
			\begin{footnotesize}(behaviour converges to a final state that approximates a predictable limit, based on the continuous environment,
			as the density and speed of devices increases)\end{footnotesize}$^2$}
			\onslide<3->{\item
			\superlight{Certified error bounds}: how  computation reacts to changes \begin{footnotesize}(linking the quality of the services to the amount of computing resources dedicated)\end{footnotesize}$^3$
			\item
			\ldots
			\item[]}
		\end{itemize}

		------------\\
		\highlight{\begin{scriptsize}
			$^1$
			M. Viroli,  G. Audrito, J. Beal, F. Damiani,  D. Pianini (2018).
			\emph{Engineering resilient collective adaptive systems by self-stabilisation.} ACM Transactions on Modeling and Computer Simulation, 28(2), 2018.
			\url{https://doi.org/10.1145/3177774}
			\\
			\onslide<2->{
			$^2$
			J. Beal, M. Viroli, D. Pianini, F. Damiani (2017). \emph{Self-adaptation to device distribution in the internet of things.} ACM Transactions on Autonomous and Adaptive Systems 12(3), 2017.  \url{https://doi.org/10.1145/3105758}}
			\\
			\onslide<3->{
			$^3$
			G. Audrito, F. Damiani, M. Viroli, E. Bini (2018). \emph{Distributed Real-Time Shortest-Paths Computations with the Field Calculus.} In: IEEE 39th  Real-Time Systems Symposium (RTSS 2018).   \url{https://doi.org/10.1109/RTSS.2018.00013}}
			\\
		\end{scriptsize}}
	\end{frame}

	\slidecount{}
	\begin{frame}{How developing works?}
		\centering
		\raisebox{-0.5\height}{\includegraphics[height=7cm]{img/workflow.pdf}} \qquad\qquad
		\medskip
	\end{frame}


\section{}
	\slidecount{}
	\begin{frame}{Course technical details}
		\begin{block}{}
			\begin{itemize}
				\item 8 lectures $\times$ 3 hours = $24$ total hours planned, of which:
				\begin{itemize}
					\item 6 lectures $\times$ 3 hours = $18$ hours: actual classes
					\item 2 lectures $\times$ 3 hours = $6$ hours: students' seminars
				\end{itemize} \pause
				\item we may move lectures if needed
				\begin{itemize}
					\item because of reasons (with notice)
					\item because of many students missing (notify if you cannot attend)
				\end{itemize} \pause
				\item examination through a seminar on topic selected by the student
				\begin{itemize}
					\item list of algorithmic papers to choose from, in that case,
					\item \ldots it should include a simulation of the chosen algorithm
					\item or it can be another AP-related topic to agree with us
				\end{itemize} \pause
				\item course material available online:
				\begin{itemize}
					\item slides uploaded after lectures
					\item reference papers (don't need to read them for the class)
					\item seminar papers (let as know as soon as you select one, if you do)
				\end{itemize}
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{Course technical details}
		\begin{block}{Seminar papers on aggregate algorithms}
			\begin{itemize}
				\item
				\textbf{[Distance]} {\small G. Audrito, F. Damiani, M. Viroli.
				\emph{Optimally-Self-Healing Distributed Gradient Structures through Bounded Information Speed}. \\
				COORDINATION 2017. \DOI{10.1007/978-3-319-59746-1\_4}}
				\item
				\textbf{[Collection]} {\small G. Audrito, S. Bergamini, F. Damiani, M. Viroli. \\
				\emph{Resilient Distributed Collection through Information Speed Thresholds}. \\
				COORDINATION 2020. \DOI{10.1007/978-3-030-50029-0\_14}}
				\item
				\textbf{[Election]} {\small Y. Mo, G. Audrito, S. Dasgupta, J. Beal. \\
				\emph{A Resilient Leader Election Algorithm Using Aggregate Computing Blocks}. \\
				IFAC World Congress 2020. \DOI{10.1016/j.ifacol.2020.12.1497}}
				\item
				\textbf{[Centrality]} {\small G. Audrito, D. Pianini, F. Damiani, M. Viroli. \\
				\emph{Aggregate Centrality Measures for IoT-based Coordination}. \\
				Science of Computer Programming 203, 2021. \DOI{10.1016/j.scico.2020.102584}}
			\end{itemize}
		\end{block}
	\end{frame}

\end{document}
