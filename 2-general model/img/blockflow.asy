unitsize(1cm);
import roundedpath;

pair rdev(real r) {
    return ((unitrand(),unitrand())-(0.5,0.5)) * r;
}

path rect(pair a, pair b, real r = 0.2) {
    return a+rdev(r) -- (a.x, b.y)+rdev(r) -- b+rdev(r) -- (b.x, a.y)+rdev(r) -- cycle;
}

path P = roundedpath(rect((0,0), (2,1)), 0.2);

void filler(pair p, pen x1, pen x2, pen y1, pen y2) {
    if (y1 == y2) {
        filldraw(shift(p)*P, y1, black+0.5);
    } else {
        radialshade(shift(p)*P, y1, p+(0.8,0.55), 0.3, true, y2, p+(1.1,0.5), 1.5, true);
        draw(shift(p)*P, black+0.5);
    }
    if (x1 == x2) {
        if (x2 != y1) fill(circle(p+(0.8,0.55), 0.3), x1);
    } else {
        radialshade(shift(p)*P, x2, p+(0.8,0.55), 0.3, false, x1, p+(0.9,0.6), 0.05, true);
    }
}

void filler(pair p, pen x) {
    filldraw(shift(p)*P, x, black+0.5);
}

draw((-1.3,-1.4) -- (0,-1.5), black+1, EndArrow(8));
filldraw(shift((-1.3,-1.4))*rotate(-4)*rect((0.9,0.2), (-0.9,-0.2), 0), palegray, black+1.5);
label(rotate(-4)*scale(0.7)*"$\texttt{temperature}$", (-1.3,-1.4), black);

draw((1,0.5) -- (2.2,-0.5) -- (1,-1.5), black+1);
draw((2.4,-0.5) -- (3,-0.5), black+1, EndArrow(8));
filldraw(rect((2,-0.3), (2.4,-0.7), 0.0), palegray, black+1.5);
label("$\texttt{>}$", (2.22, -0.5), black);

filler((0,0), red);
filler((0,-2), yellow, red, red, blue);

draw((4,-0.5) -- (7.5,0), black+1, EndArrow(8));
filldraw(shift((6.1,-0.2))*rotate(7)*rect((0.8,0.2), (-0.8,-0.2), 0), palegray, black+1.5);
label(rotate(7)*scale(0.7)*"$\texttt{distanceTo}$", (6.1,-0.2), black);

draw((4,-0.5) -- (6.5,-1.3) -- (8.5,0), black+1);
draw((6.5,-1.3) -- (6.5,-2), black+1, EndArrow(8));
filldraw(rect((5.5,-1.1), (7.5,-1.5), 0), palegray, black+1.5);
label(scale(0.7)*"$\texttt{collectMax}$", (6.5,-1.3), black);

filler((3,-1), black, black, mediumgray, mediumgray);

filler((7.5,-0.5), yellow, yellow, yellow, green);

filler((5.5,-3), green);
