unitsize(1cm);
import math;
settings.tex = "pdflatex";


picture back;
unitsize(back, 1cm);

fill(back, (0,0) -- (4.5,1.5) -- (0,3) -- cycle, white*0.5+palered*0.5);
fill(back, (9,0) -- (4.5,1.5) -- (9,3) -- cycle, white*0.5+palegreen*0.5);
axialshade(back, (0,3) -- (4.5,1.5) -- (9,3) -- cycle, mediumgray, (4.5,1.5), white, (4.5,3));
axialshade(back, (0,0) -- (4.5,1.5) -- (9,0) -- cycle, mediumgray, (4.5,1.5), white, (4.5,0));
filldraw(back, shift(0,1.5)*scale(0.5,1.5)*unitcircle, palered, black+1.5);
filldraw(back, shift(9,1.5)*scale(0.5,1.5)*unitcircle, palegreen, black+1.5);
draw(back, (0.03,0.00) -- (8.97,3.00), black+1.5);
draw(back, (0.03,3.00) -- (8.97,0.00), black+1.5);


picture front;
unitsize(front, 1cm);

draw(front, (0,1.5) -- (9,1.5), black+.5, EndArrow(5));
draw(front, (4.5,-.3) -- (4.5,3.3), black+.5, Arrows(5));
label(front, scale(0.5)*"time-like", (8,1.4));
label(front, scale(0.5)*rotate(90)*"space-like", (4.4,2.6));

srand(2);
real[] K = {2.2,1.2,1.7,1.5,1.6,1.0,0.8};
real[] O = {4.6,4.0,4.4,4.5,4.9,4.8,5.0};
guide other, kitten;
for (int x=0; x<=6; ++x) {
    kitten = kitten {E}.. (x*1.5,K[x]);
}
for (int y=0; y<=6; ++y) {
    other = other {N}.. (O[y], y*0.5);
}
draw(front, kitten);
draw(front, other);

Label[] faces = {
    scale(0.08)*graphic("cactus.png"),
    scale(0.03)*graphic("turtle.png"),
    scale(0.04)*graphic("kitten.png"),
};
pair[] offs = {(0.04,0.3), (0.13,0.1), (0,0.3)};
for (int i=0; i<=4; ++i)
    label(front, faces[2], point(kitten, i*1.5) + offs[2]);

label(front, faces[0], point(other, 1) + offs[0]);
label(front, faces[1], point(other, 5) + offs[1]);

int f = 1;
for (int i=0; i<6; ++i) {
    if (i==5) f = 1000000000;
    picture pic;
    unitsize(pic, 1cm);
    if (i < 5)
        add(pic, shift(4.5,0)*xscale(1/f)*shift(-4.5,0)*back);
    axialshade(pic, (0,0) -- (4.5*(1-1/f),0) -- (4.5*(1-1/f),3) -- (0,3) -- cycle, palered, (4.5*(1-1/f),1.5), white, (0,1.5));
    axialshade(pic, (9,0) -- (4.5*(1+1/f),0) -- (4.5*(1+1/f),3) -- (9,3) -- cycle, palegreen, (4.5*(1+1/f),1.5), white, (9,1.5));
    draw(pic, shift(0,1.5)*scale(0.5,1.5)*unitcircle, invisible+1.5);
    draw(pic, shift(9,1.5)*scale(0.5,1.5)*unitcircle, invisible+1.5);

    add(pic, front);
    shipout("einstein" + string(i), pic);
    f *= 2;
}
