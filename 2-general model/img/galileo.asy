settings.tex = "pdflatex";

picture graph;
unitsize(graph, 1cm);
draw(graph, (0,0) -- (0,3), black+1.5, EndArrow(8));
draw(graph, (0,0) -- (9,0), black+1.5, EndArrow(8));
label(graph, scale(0.8)*"time", (8,-0.2));
label(graph, scale(0.8)*rotate(90)*"position", (-0.3,1.7));

for (int x=2; x<9; x+=2)
    draw(graph, (x,0) -- (x,3), gray+1+dotted);
for (int x=0; x<7; x+=2)
    label(graph, scale(0.6)*string(x#2), (x,-0.2));

srand(2);
real[] K = {2,0.3,2.4,0.7,0.2};
guide cactus, turtle, kitten;
for (int x=0; x<9; x+=2) {
    cactus = cactus -- (x,1.5);
    turtle = turtle :: (x,x*0.25+unitrand()*0.7+0.5-0.35);
    kitten = kitten {E}.. (x,K[x#2]);
}
path[] lines = {cactus, turtle, kitten};
Label[] faces = {
    scale(0.08)*graphic("cactus.png"),
    scale(0.03)*graphic("turtle.png"),
    scale(0.04)*graphic("kitten.png"),
};
pair[] offs = {(0.04,0.3), (0.13,0.1), (0,0.3)};

for (int t=0; t<10; ++t) {
    picture pic;
    unitsize(pic, 1cm);
    real x = point(lines[0], t/2).x;
    axialshade(pic, (0,0) -- (x,0) -- (x,3) -- (0,3) -- cycle, palered, (0,0), white, (0,3));
    axialshade(pic, (9,0) -- (x,0) -- (x,3) -- (9,3) -- cycle, palegreen, (9,0), white, (9,3));
    add(pic, graph);
    for (int i=2; i>=0; --i) {
        draw(pic, subpath(lines[i], 0, t/2));
        label(pic, faces[i], point(lines[i], t/2) + offs[i]);
    }
    if (t == 9) {
        label(pic, scale(0.3)*graphic("badge.png"), (4.5,1.5));
        layer(pic);
        fill(pic, circle((4.5,1.5), 1), lightgray);
        label(pic, rotate(30)*scale(1)*"\textsf{NOT OUR}",  (4.5,1.5) + rotate(30)*(0, 0.3));
        label(pic, rotate(30)*scale(1)*"\textsf{UNIVERSE}", (4.5,1.5) + rotate(30)*(0,-0.3));
    }
    shipout("galileo" + string(t), pic);
}
