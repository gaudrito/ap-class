unitsize(1cm);
include roundedpath;
include math;
settings.tex = "pdflatex";

string[] devs = {"desktop", "laptop", "tablet", "smartphone", "arduino", "camera", "smokedetector", "thermostat"};
for (int i=0; i<devs.length; ++i) devs[i] = graphic("dev-" + devs[i] + ".png");

real luminance(pen p) {
    real[] c = colors(rgb(p));
    return sqrt( 0.299*c[0]^2 + 0.587*c[1]^2 + 0.114*c[2]^2 );
}

pen darkest(pen a, pen b) {
    if (a == b) return a;
    if (a == olive) return b;
    if (b == olive) return a;
    return black;
    return luminance(a) < luminance(b) ? a : b;
}

pen lighter(pen a) {
    return 0.9*white + 0.1*a;
}

pair getdim(real[][] E) {
    pair dim = (0, 0.8*E.length);
    for (int i=0; i<E.length; ++i)
        for (int j=0; j<E[i].length; ++j)
            dim = (max(dim.x, E[i][j]), dim.y);
    return dim+(1.2,.6);
}

void drawarrow(picture pic, pair s, pair e, pen c = black, bool invert = false) {
    pair dir = unit(e-s);
    s = s+0.22*dir;
    e = e-0.22*dir;
    pair m = (s+e)/2 + rotate(90) * (invert ? -.1 : .1) * dir;
    draw(pic, s -- e, c);
    label(pic, rotate(degrees(dir))*scale(0.4)*"$<$", m);
}

void drawnode(picture pic, pair pos, string l, pen col = black) {
    pen fg = length(l)!=3 ? col : lighter(col);
    pen bk = length(l)!=3 ? lighter(col) : col;
    if (length(l) != 5) fill(pic, circle(pos, .19), fg);
    else {
        fill(pic, circle(pos, .18), fg);
        draw(pic, circle(pos, .20), fg);
    }
    fill(pic, circle(pos, .16), bk);
    if (length(l) > 0) label(pic, scale(0.7)*("$"+l+"$"),pos, fg);
}

int SEED = 5;
srand(SEED);


picture pic0;
unitsize(pic0, 1cm);
pair[] P;
for (int i=0; i<8; ++i) {
    P[i] = (i, 2*unitrand());
    drawnode(pic0, P[i], "\epsilon_" + string(i));
}
shipout("order0", pic0);

picture pic1;
unitsize(pic1, 1cm);
add(pic1, pic0);
for (int i=0; i<7; ++i) {
    drawarrow(pic1, P[i], P[i+1]);
}
shipout("order1", pic1);

picture pic2;
unitsize(pic2, 1cm);
add(pic2, pic0);
drawarrow(pic2, P[0], P[1], true);
drawarrow(pic2, P[0], P[2]);
drawarrow(pic2, P[1], P[3], true);
drawarrow(pic2, P[2], P[3]);
drawarrow(pic2, P[3], P[5], true);
drawarrow(pic2, P[4], P[5]);
drawarrow(pic2, P[4], P[6]);
drawarrow(pic2, P[5], P[7], true);
shipout("order2", pic2);

