\documentclass[10pt]{beamer} %,aspectratio=169,handout
	\usetheme{CambridgeUS} %CambridgeUS, Goettingen, Singapore, Madrid
	\setbeamertemplate{footline}[frame number]
	\setbeamercovered{transparent}
	\beamertemplatenavigationsymbolsempty

\usepackage{multimedia}
\usepackage{tabularx}
\usepackage{listings}
\usepackage{forloop}
\usepackage{mathtools}
\usepackage{graphicx}
\usepackage{tikz}
\newcolumntype{M}[1]{>{\centering\arraybackslash}m{#1}}
\setlength\tabcolsep{4pt}

\input{../macros}

\newcounter{prog}
\newcommand{\progress}[2]{$\forloop{prog}{0}{\value{prog} < #1}{\bullet}\forloop{prog}{0}{\value{prog} < #2}{\circ}$}
\newcounter{secslide}
\newcounter{sectotal}
\newcounter{secother}
\newcommand{\resetsec}[1]{\setcounter{secslide}{0}\setcounter{sectotal}{#1}}
\newcommand{\slidecount}{\addtocounter{secslide}{1}\setcounter{secother}{\value{sectotal} - \value{secslide}}\subsection[\progress{\value{secslide}}{\value{secother}}]{slide}}

\lstdefinelanguage{hfc}{
	basicstyle=\lst@ifdisplaystyle\small\fi\ttfamily,
	frame=single,
	basewidth=0.5em,
	sensitive=true,
	morestring=[b]",
	morecomment=[l]{//},
	morecomment=[n]{/*}{*/},
	commentstyle=\color{OliveGreen},
	keywordstyle=\color{blue}\textbf, keywords={def}, otherkeywords={=>},
	keywordstyle=[2]\color{red}\textbf, keywords=[2]{rep,nbr,if,let,in},
	keywordstyle=[3]\color{violet}, keywords=[3]{mux,nbrRange,sumHood,countHood,everyHood,anyHood,minHood,maxHood,foldHood,maxHoodPlusSelf},
	keywordstyle=[4]\color{orange}\textbf, keywords=[4]{spawn,share},
	keywordstyle=[5]\color{blue}, keywords=[5]{false,true,infinity}
}

\definecolor{darkgreen}{rgb}{0.0, 0.5, 0.0}
\definecolor{darkred}{rgb}{0.5, 0.0, 0.0}
\definecolor{darkblue}{rgb}{0.2, 0.2, 0.7}

\newcommand{\highlight}[1]{\textcolor{darkblue}{#1}}
\newcommand{\superlight}[1]{\textcolor{darkred}{#1}}
\newcommand{\greenlight}[1]{\textcolor{darkgreen}{#1}}
\newcommand{\DOI}[1]{\href{http://www.doi.org/#1}{\underline{#1}}}
\newcommand{\ACMDOI}[1]{\href{https://dl.acm.org/doi/#1}{\underline{#1}}}
\newcommand{\CITE}[5]{\ssymbol{#1}[#2. \emph{#3.} #4. \DOI{#5}]}

\theoremstyle{plain}
	\newtheorem{proposition}[theorem]{Proposition}
\theoremstyle{remark}
	\newtheorem{question}[theorem]{Question}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{Aggregate Programming for the Internet of Things (part 2)}
\author{Ferruccio Damiani, ProgMob (a.a., 2022-2023)}
\institute{{\small Dipartimento di Informatica\\[3pt] Universit\`a degli Studi di Torino}}
%\date{}
\titlegraphic{
	%\raisebox{-0.5\height}{\includegraphics[width=2.0cm]{../images/logo_horizon.jpg}} \quad
	\raisebox{-0.5\height}{\includegraphics[width=2.0cm]{../images/logo_torino.pdf}} \quad
	%\raisebox{-0.5\height}{\includegraphics[width=2.0cm]{../images/logo_sanpaolo.png}}
}

\begin{document}
\lstset{language={hfc}}

\section[~]{Title}
	\begin{frame}
		\titlepage
	\end{frame}

%	\begin{frame}{Conceptual map}
%		\vspace{-3mm}
%		\begin{block}{}
%		\begin{center}
%			\includegraphics[height=0.86\textheight]{../images/map1.pdf}
%		\end{center}
%		\end{block}
%	\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\resetsec{6}
\section{An informal view of distributed computation}

	\slidecount{}
	\begin{frame}{Outline}
		\begin{itemize}
			\item \textcolor{darkblue}{An informal view of distributed computation}
			\begin{itemize}
				\item What we have
				\item What we need
				\item What physics can say
			\end{itemize}
			\bigskip
			\item A formal view of distributed computation
			\bigskip
			\item Conclusions
		\end{itemize}
	\end{frame}

	\slidecount{}
	\begin{frame}{What we have}
		\includegraphics<1| handout:0>[scale=0.51]{img/machines9.pdf}

		\begin{block}{} \pause
			\begin{itemize}
				\item many computing units, \pause each executing sequentially \textcolor{darkblue}{$\longrightarrow$ boring} \pause
				\item they sometimes send/receive messages \textcolor{darkblue}{$\longrightarrow$ interesting!} \pause
				\item computation is \textcolor{darkblue}{invisible} in the system until a message is \textcolor{darkblue}{sent} \pause
			\end{itemize}
		\end{block}

		\begin{center}
			\includegraphics[scale=0.51]{img/machines10.pdf}
		\end{center}

		\begin{block}{}
			\begin{itemize}
				\item focus on \textcolor{darkblue}{events} (when a message is sent) \pause
				\item \textcolor{darkblue}{messages} are coming out from an event and reaching other devices, influencing their \textcolor{darkblue}{following events}
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{What we need}
		\begin{block}{predictable code re-use}
			\begin{itemize}
				\item can break down complexity of problems
				\item needs to avoid unwanted interferences
			\end{itemize}
		\end{block}

		\begin{center}
			\raisebox{-0.5\height}{\includegraphics[height=3cm]{img/challenge-reuse.jpg}}
		\end{center}

		\begin{minipage}{0.48\linewidth}
			\begin{block}{in a sequential system\ldots}
				\begin{itemize}\setlength\itemsep{0pt}
					\item machine code is hard to re-use
					\item \textcolor{darkblue}{abstractions:} objects, mixins\ldots
					\item all boil down to \textcolor{darkblue}{functions} and composition
				\end{itemize}
			\end{block}
		\end{minipage} ~~
		\begin{minipage}{0.48\linewidth}
			\begin{block}{in a distributed system\ldots}
				\begin{itemize}\setlength\itemsep{0pt}
					\item need to \textcolor{darkblue}{match} messages
					\item not good with \textcolor{darkblue}{process calculi}
					\item \ldots how to handle multiple instances of same algorithm?
				\end{itemize}
			\end{block}
		\end{minipage}
	\end{frame}

	\slidecount{}
	\begin{frame}{What we need}
		\begin{block}{today's goal}
			\begin{itemize}
				\item present an \textcolor{darkblue}{abstraction} of distributed function behaviour
				\item that is \textcolor{darkblue}{composable} without interferences
			\end{itemize}
		\end{block} \pause

		\begin{center}
			\includegraphics[height=3.0cm]{img/blockflow.pdf}
		\end{center}

		\begin{block}{which abstraction?}
			\begin{itemize}
				\item there may be \textcolor{darkblue}{many} \pause
				\item today we start with the most \textcolor{darkblue}{general} and \textcolor{darkblue}{concrete} (based on \textcolor{darkblue}{events}) \pause
				\item later we'll see others (\textcolor{darkblue}{limit models}) further raising the abstraction level
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{What physics can say}
		\begin{minipage}{0.29\linewidth}
			\includegraphics[width=0.95\linewidth]{img/galileo.jpg}
		\end{minipage}
		\begin{minipage}{0.7\linewidth}
			\begin{block}{Galileo, help!}
				\begin{itemize}
					\item \textcolor{darkblue}{events} are pairs position, time $\ap{\vec{x}, t}$ \pause \pause
					\item position is relative, but time is \textcolor{darkblue}{universal} \pause \pause
					\item past, present and future are \textcolor{darkblue}{linear} \pause \pause \pause
				\end{itemize}
			\end{block}
		\end{minipage}

		\begin{center}
			\includegraphics<1| handout:0>[width=\linewidth]{img/galileo0.pdf}
			\includegraphics<2| handout:0>[width=\linewidth]{img/galileo1.pdf}
			\includegraphics<3| handout:0>[width=\linewidth]{img/galileo2.pdf}
			\includegraphics<4| handout:0>[width=\linewidth]{img/galileo3.pdf}
			\includegraphics<5| handout:0>[width=\linewidth]{img/galileo4.pdf}
			\includegraphics<6| handout:0>[width=\linewidth]{img/galileo5.pdf}
			\includegraphics<7| handout:0>[width=\linewidth]{img/galileo6.pdf}
			\includegraphics<8| handout:0>[width=\linewidth]{img/galileo7.pdf}
			\includegraphics<9| handout:0>[width=\linewidth]{img/galileo8.pdf}
			\includegraphics<10>[width=\linewidth]{img/galileo9.pdf}

			\textbf{sounds like a sequential/parallel system...}
		\end{center}
	\end{frame}

	\slidecount{}
	\begin{frame}{What physics can say}
		\begin{minipage}{0.29\linewidth}
			\includegraphics[width=0.95\linewidth]{img/einstein.jpg}
		\end{minipage}
		\begin{minipage}{0.7\linewidth}
			\begin{block}{Einstein, help!}
				\begin{itemize}
					\item position and time are both \textcolor{darkblue}{relative} \pause
					\item no clock, only \textcolor{darkblue}{relation} between events \pause
					\item some events are just \textcolor{darkblue}{not comparable} \pause
				\end{itemize}
			\end{block}
		\end{minipage}

		\begin{center}
			\includegraphics<1-4>[width=\linewidth]{img/einstein0.pdf}
			\includegraphics<5| handout:0>[width=\linewidth]{img/einstein1.pdf}
			\includegraphics<6| handout:0>[width=\linewidth]{img/einstein2.pdf}
			\includegraphics<7| handout:0>[width=\linewidth]{img/einstein3.pdf}
			\includegraphics<8| handout:0>[width=\linewidth]{img/einstein4.pdf}
			\includegraphics<9| handout:0>[width=\linewidth]{img/einstein5.pdf}

			\textbf{sounds weird just like a distributed system!}
		\end{center}
	\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\resetsec{6}
\section{A formal view of distributed computation}

	\slidecount{}
	\begin{frame}{Outline}
		\begin{itemize}
			\item An informal view of distributed computation
			\bigskip
			\item \textcolor{darkblue}{A formal view of distributed computation}
			\begin{itemize}
				\item Partially ordered sets
				\item Directed graphs
				\item Event structures
				\item Space-time values
				\item Space-time functions
			\end{itemize}
			\bigskip
			\item Conclusions
		\end{itemize}
	\end{frame}

	\slidecount{}
	\begin{frame}{Partially ordered sets}
		\begin{block}{}
			\begin{itemize}
				\item relation between events ``$\eventId_1$ is in the \textcolor{darkblue}{past} of $\eventId_2$'' \pause
				\item looks like an \textcolor{darkblue}{order} relation $\eventId_1 < \eventId_2$ \pause
				\item more precisely, a \textcolor{darkblue}{partial order}
			\end{itemize}
		\end{block} \pause

		\begin{center}
			\includegraphics<1| handout:0>[width=0.9\linewidth]{img/order0.pdf}
			\includegraphics<2| handout:0>[width=0.9\linewidth]{img/order1.pdf}
			\includegraphics<3->[width=0.9\linewidth]{img/order2.pdf}
			\newline
			\begin{tikzpicture}[overlay]
				\fill<7->[orange,rounded corners] (-0.2\textwidth,1.1cm) rectangle (0.2\textwidth,1.7cm);
				\node<7-> at (0,1.4cm) {\textcolor{white}{not really a partial order}};
			\end{tikzpicture}
		\end{center}

		\begin{block}{a partial order is...}
			\begin{itemize}
				\item \textcolor{darkblue}{transitive}: if $\eventId_1 < \eventId_2$ and $\eventId_2 < \eventId_3$ then $\eventId_1 < \eventId_3$ \pause
				\item \textcolor{darkblue}{antisymmetric}: $\eventId_1 < \eventId_2$ and $\eventId_2 < \eventId_1$ cannot both be \textcolor{darkblue}{true} \pause
				\item \ldots but can both be \textcolor{darkblue}{false}!
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}[t]{Directed graphs}
		\begin{block}{}
			\begin{itemize}
				\item a set $V$ of \textcolor{darkblue}{nodes} $\bp{u, v, \ldots}$ \pause and a set $E \subseteq (V \times V)$ of \textcolor{darkblue}{edges} $e = \ap{u,v}$ \pause
				\item we write $u \neigh v$ to mean that $\ap{u,v} \in E$ \pause
				\item is a \textcolor{darkblue}{directed acyclic graph} (DAG) if it has no \textcolor{darkblue}{cycles} of edges \\
				$v_1 \neigh v_2 \neigh v_3 \neigh \ldots \neigh v_{n-1} \neigh v_n \neigh v_1$ all in $E$
			\end{itemize}
		\end{block} \pause

		\begin{center}
			\includegraphics<1-4| handout:0>[width=0.6\linewidth]{img/structure.pdf}
			\includegraphics<5->[width=0.32\linewidth]{img/structure.pdf}
		\end{center}

		\only<5->{
		\begin{block}{transitive closure}
			\begin{itemize}
				\item every DAG as a transitive closure, which is a \textcolor{darkblue}{partial order} $<$ \pause
				\item $u < v$ iff there exists a \textcolor{darkblue}{chain} $u \neigh w_0 \neigh \ldots \neigh w_n \neigh v$ \pause
				\item many DAGs share the \textcolor{darkblue}{same} transitive closure (DAG has more information!)
			\end{itemize}
		\end{block}
		}
	\end{frame}

	\slidecount{}
	\begin{frame}{Event structures}
		\begin{block}{}
			\begin{itemize}
				\item a set of \textcolor{darkblue}{events} $\eventS$ \pause
				\item a DAG of \textcolor{darkblue}{messages} $\neigh$ \pause
				\item a \textcolor{darkblue}{causality} partial order $<$ (transitive closure of $\neigh$)
			\end{itemize}
		\end{block}

		\begin{center}
			\textcolor{darkred}{past} - concurrent - \textcolor{darkgreen}{future}

			\includegraphics<1-3| handout:0>[width=0.73\linewidth]{img/estruct_gossip.pdf}
			\includegraphics<4->[width=0.73\linewidth]{img/estruct_gossip_labels.pdf}
			\newline
			\begin{tikzpicture}[overlay]
				\fill<5| handout:0>[orange,rounded corners] (-0.15\textwidth,3.0cm) rectangle (0.15\textwidth,3.6cm);
				\node<5| handout:0> at (0,3.3cm) {\textcolor{white}{``light'' is slow here\ldots}};
			\end{tikzpicture}
		\end{center}
	\end{frame}

	\slidecount{}
	\begin{frame}{Space-time values}
		\begin{block}{}
			\begin{itemize}
				\item \textcolor{darkblue}{maps} from events in event structures to values $\dvalue : \eventS \to X$
			\end{itemize}
		\end{block} \pause

		\begin{center}
			\includegraphics<1-2>[width=0.73\linewidth]{img/estruct_gossip.pdf}
			\includegraphics<3| handout:0>[width=0.73\linewidth]{img/estruct_input.pdf}
			\raisebox{2.5cm}{\rotatebox{90}{\textbf{\phantom{d}\only<2| handout:0>{gossip}\only<3| handout:0>{input}\phantom{p}}}}
		\end{center}
		\vspace{-5pt}

		\begin{block}{$\uparrow$ space-time value resulting from a ``gossip-min''}
			\emph{every device gossips the minimum of an \textcolor{darkblue}{input value} and neighbours' \textcolor{darkblue}{gossips}}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{Space-time functions}
		\begin{block}{}
			\begin{itemize}
				\item \textcolor{darkblue}{maps} from space-time values to space-time values $f(\dvalue_\text{in}, \ldots) = \dvalue_\text{out}$
			\end{itemize}
		\end{block}
		\bigskip

		\begin{tabular}{c@{\hspace{-2mm}}c@{\hspace{0mm}}c}
			\raisebox{-0.5\height}{\includegraphics[width=0.45\linewidth]{img/estruct_input.pdf}} &
			\raisebox{-0.5\height}{$\stackrel{\texttt{gossip-min}}{\longrightarrow}$} &
			\raisebox{-0.5\height}{\includegraphics[width=0.45\linewidth]{img/estruct_gossip.pdf}} \\
			$\dvalue_\text{in}$ & & $\texttt{gossip-min}(\dvalue_\text{in}) = \dvalue_\text{out}$
		\end{tabular} \pause
		\bigskip

		\begin{block}{}
			\begin{itemize}
				\item they're just \textcolor{darkblue}{plain} old functions (maybe with weird domain) \pause
				\item we can \textcolor{darkblue}{compose} them as usual $f(g(\dvalue_1),  h(\dvalue_2), \dvalue_3)$
			\end{itemize}
		\end{block}
	\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\resetsec{4}
\section{Conclusions}

	\slidecount{}
	\begin{frame}{Outline}
		\begin{itemize}
			\item An informal view of distributed computation
			\bigskip
			\item A formal view of distributed computation
			\bigskip
			\item \textcolor{darkblue}{Conclusions}
			\begin{itemize}
				\item What we have and what we are missing
				\item How to express space-time functions to start with?
			\end{itemize}
		\end{itemize}
	\end{frame}

	\slidecount{}
	\begin{frame}{What we have and what we are missing}
		\begin{block}{we have:}
			\begin{itemize}
				\item event structures $\ap{\eventS, \neigh, <}$
				\onslide<2->{\item space-time values $\dvalue : \eventS \to X$}
				\onslide<3->{\item composable space-time functions $f(\dvalue_\text{in}) = \dvalue_\text{out}$}
			\end{itemize}
		\end{block}

		\begin{center}
			\raisebox{-0.5\height}{\includegraphics[width=0.35\linewidth]{img/estruct_input.pdf}}
			\raisebox{-0.5\height}{$\stackrel{f}{\longrightarrow}$}
			\raisebox{-0.5\height}{\includegraphics[width=0.35\linewidth]{img/estruct_gossip.pdf}}
		\end{center} \pause\pause\pause
		\vspace{-10pt}

		\begin{block}{we miss:}
			\begin{itemize}
				\item how to express space-time functions to \textcolor{darkblue}{start with}? \pause
				\item space-time functions on event structures are still \textcolor{darkblue}{hard to grasp} \pause
				\item \ldots we would like something more \textcolor{darkblue}{intuitive}! \pause (limit models and guarantees)
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{How to express space-time functions to start with?}
		\begin{block}{global-to-local, I choose you!}
			\begin{itemize}
				\item basic functions need to be expressed \textcolor{darkblue}{locally} to be executable
				\onslide<2->{\item \ldots and we cannot really solve global-to-local \textcolor{darkblue}{in general}}
				\onslide<3->{\item did I \textcolor{darkblue}{lie} by saying that in AC we solve it?}
			\end{itemize}
		\end{block}

		\begin{center}
			\raisebox{-0.5\height}{\includegraphics[width=0.35\linewidth]{img/estruct_input.pdf}}
			\raisebox{-0.5\height}{$\stackrel{f}{\longrightarrow}$}
			\raisebox{-0.5\height}{\includegraphics[width=0.35\linewidth]{img/estruct_gossip.pdf}}
		\end{center} \pause\pause\pause
		\vspace{-10pt}

		\begin{block}{sort of, but not exactly\ldots}
			\begin{itemize}
				\item \textcolor{darkblue}{basic algorithms} are locally expressed with nice global interpretation \pause
				\item \ldots so you can program thinking \textcolor{darkblue}{globally} as far as you compose them \pause
				\item for something new, you still need to solve the \textcolor{darkblue}{global-to-local} yourself
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{How to express space-time functions to start with?}
		\superlight{so, how do we express them locally?} \pause

		\begin{block}{in field calculus!}
			\begin{center}
				\small
				$
				\e \; \BNFcce \;  \xname \; \BNFmid \; \fvalue \; \BNFmid \; \dcOf{\dc}{\overline\e} \; \BNFmid \; \bname  \; \BNFmid \; \fname \; \BNFmid \; (\overline{\xname}) \; \toSymK{\e} \; \BNFmid \; \e(\overline{\e}) \; \BNFmid \; \nbrK\{\e\} \; \BNFmid \; \repK(\e)\{ \xname \toSymK{\e} \}\ldots
				$
			\end{center} \pause
			\begin{itemize}
				\item standard \textcolor{darkblue}{functional} language core (function call, recursion...) \pause
				\item standard \textcolor{darkblue}{branching}, with a special meaning:
				\begin{itemize}
					\item different branches \textcolor{darkblue}{cannot exchange} messages
					\item ``splitting'' the network in computing \textcolor{darkblue}{areas}
				\end{itemize} \pause
				\item $\nbrK\{ \e \}$ for \textcolor{darkblue}{communication}, which:
				\begin{itemize}
					\item \textcolor{darkblue}{computes} $\e$ to a value $\anyvalue$, then \textcolor{darkblue}{broadcasts} it to neighbours
					\item \textcolor{darkblue}{collects} values received from neighbours into a \textcolor{darkblue}{neighbouring field} $\fvalue$
				\end{itemize} \pause
				\item \ldots more communication constructs: $\repK$, $\shareK$... \textbf{we'll see it next time!}
			\end{itemize}
			\centering
		\end{block} 
%\pause
%
%		\begin{block}{\superlight{IMPORTANT! before next meeting follow setup instructions from:}}
%			\highlight{\small\url{https://github.com/fcpp/fcpp-exercises}}
%		\end{block}
	\end{frame}


\end{document}
