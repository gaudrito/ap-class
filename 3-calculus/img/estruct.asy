unitsize(1cm);
include roundedpath;
include math;


real luminance(pen p) {
    real[] c = colors(rgb(p));
    return sqrt( 0.299*c[0]^2 + 0.587*c[1]^2 + 0.114*c[2]^2 );
}

pen darkest(pen a, pen b) {
    if (a == b) return a;
    if (a == olive) return b;
    if (b == olive) return a;
    return black;
    return luminance(a) < luminance(b) ? a : b;
}

pen lighter(pen a) {
    return 0.9*white + 0.1*a;
}

pair getdim(real[][] E) {
    pair dim = (0, 0.8*E.length);
    for (int i=0; i<E.length; ++i)
        for (int j=0; j<E[i].length; ++j)
            dim = (max(dim.x, E[i][j]), dim.y);
    return dim+(1.2,.6);
}

void drawarrow(picture pic, pair s, pair e, pen c) {
    pair dir = unit(e-s);
    s = s+0.22*dir;
    e = e-0.22*dir;
    real w = 0.05;
    int n = max((int)(length(e-s)/w-3), 0);
    path p = s;
    for (int i=0; i<n; ++i) p = p -- s+(w/2+w*i)*dir+rotate(i%2==0 ? 90 : -90)*(w/2*dir);
    p = p -- s+(w*n*dir) -- e;
    draw(pic, p, c, EndArrow);
}

void drawnode(picture pic, pair pos, string l, pen col) {
    if (length(l) == 0) {
        fill(pic, circle(pos, .19), gray);
        return;
    }
    pen fg = length(l)<5 ? col : lighter(col);
    pen bk = length(l)<5 ? lighter(col) : col;
    if (l != " 2") fill(pic, circle(pos, .19), fg);
    else {
        fill(pic, circle(pos, .18), fg);
        draw(pic, circle(pos, .20), fg);
    }
    fill(pic, circle(pos, .16), bk);
    if (length(l) > 0) label(pic, scale(0.7)*("$"+l+"$"),pos, fg);
}

void drawgrid(picture pic, pair pos, pair dim, bool dev, real[][] E) {
    draw(pic, pos -- pos+(dim.x-0.5,0), EndArrow);
    if (dev) {
        draw(pic, pos -- pos+(0,dim.y-0.5), EndArrow);
        for (int i=0; i<E.length; ++i) {
            label(pic, scale(0.5)*string(i+1),  pos+(0.1,0.8*i+.55));
            draw(pic, pos+(0.15,0.8*i+.4) -- pos+(dim.x-0.5,0.8*i+.4), mediumgray+dotted);
            draw(pic, pos+(0,0.8*i+.4) -- pos+(0.15,0.8*i+.4));
        }
        label(pic, rotate(90)*scale(0.7)*"device",pos+(-0.2,dim.y-1.5));
        label(pic,            scale(0.7)*"time",  pos+(dim.x-1.3,-0.2));
    } else {
        label(pic,            scale(0.7)*"past",  pos+(       .8,-0.2));
        label(pic,            scale(0.7)*"future",pos+(dim.x-1.2,-0.2));
    }
}

void plot(picture pic, pair pos, bool dev, real[][] E, pen[][] C, string[][] V, int[][][] G) {
    pair dim = getdim(E);
    drawgrid(pic, pos, dim, dev, E);
    pair[][] Ec;

    for (int i=0; i<E.length; ++i) {
        Ec[i] = new pair[];
        for (int j=0; j<E[i].length; ++j)
            Ec[i][j] = pos+(E[i][j],0.8*i+(dev ? .4 : 0.8*unitrand()));
    }
    for (int i=0; i<E.length; ++i) {
        for (int j=0; j<E[i].length; ++j) {
            drawnode(pic, Ec[i][j], V[i][j], C[i][j]);
            if (G.length > 0) {
                int x = 0;
                for (int k=0; k<E.length; ++k) if (G[i][j][k] > 0) {
                    for (int h=0; h+G[i][j][k]<=E[k].length; ++h)
                        if (E[k][h+G[i][j][k]-1] < E[i][j] && (h+G[i][j][k] == E[k].length || E[k][h+G[i][j][k]] >= E[i][j]))
                            drawarrow(pic, Ec[k][h], Ec[i][j], darkest(C[k][h], C[i][j]));
                    ++x;
                }
            }
        }
    }
}

picture main(bool dev, pen[][] colors, string[][] contents) {
    real[][]   Events = {
                        {1.4,2.5,3.4,4.5,5.4},
                        {0.7,1.5,3.3,4.0,5.35,6.2},
                        {2.6,3.8,4.5,5.7},
                        {1.0,1.8,3.0,4.0,5.0,6.2},
                        {1.7,3.7,5.9}
                        };
    int[][][]  Graph  = {
                        {{0,0,0,0,0},{1,1,0,0,0},{1,2,1,0,0},{1,1,0,0,0},{1,2,0,0,0}},
                        {{0,0,0,0,0},{1,1,0,0,0},{0,0,0,0,0},{1,1,1,0,0},{2,1,1,0,0},{1,1,1,0,0}},
                        {{0,0,0,0,0},{0,1,1,1,0},{0,1,1,1,0},{0,1,1,1,0}},
                        {{0,0,0,0,0},{0,2,0,1,0},{0,1,1,1,1},{0,0,2,1,2},{0,0,1,1,1},{0,0,1,1,0}},
                        {{0,0,0,1,0},{0,0,0,0,1},{0,0,0,1,1}}
                        };

    DefaultHead.size=new real(pen p=currentpen) {return 1.2mm;};
    srand(5);
    picture pic;
    unitsize(pic, 1cm);
    plot(pic, (0,0), dev, Events, colors, contents, Graph);
    return pic;
}

pen[][]    colors;
string[][] contents;

colors = new pen[][]{
    {brown,black,black,deepcyan,deepcyan},
    {brown,brown,brown,deepcyan,deepcyan,deepcyan},
    {brown,darkmagenta,deepcyan,deepcyan},
    {brown,brown,brown,black,deepcyan,deepcyan},
    {brown,black,deepcyan}
};
contents = new string[][]{
    {"1","  2  ","  3  ","4","5"},
    {"1","2","1","2","3","4"},
    {"1"," 2","3","4"},
    {"1","2","3","  4  ","5","6"},
    {"1","  2  ","3"}
};
shipout("structure", main(true, colors, contents));

colors = new pen[][]{
    {black,black,black,black,black},
    {black,black,black,black,black,black},
    {black,black,black,black},
    {black,black,black,black,black,black},
    {black,black,black}
};
contents = new string[][]{
    {"f","f","","",""},
    {"f","f","f","f","",""},
    {"f"," f","",""},
    {"f","f","","","",""},
    {"f","f",""}
};
shipout("splitting1", main(true, colors, contents));

contents = new string[][]{
    {"","","g","g","g"},
    {"","","","","g","g"},
    {"","","g","g"},
    {"","","g","g","g","g"},
    {"","","g"}
};
shipout("splitting2", main(true, colors, contents));
