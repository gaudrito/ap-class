unitsize(1cm);
import math;
import roundedpath;
settings.tex = "pdflatex";



pen p0 = palegray;
pen ps = palered;
pen p1 = black;
pen p2 = brown+1;
pen p3 = red+2;

// posizioni dei vertici
pair[] P = {(0,0), (2,2), (4,1), (8,0), (8.5,3), (8.8,-0.8), (11,1), (12,-1.5), (1.5,-0.6)};

// label dei vertici
real[]  VL = {3.3, 1.5, 0, 3.1, 6.5, 2.9, 5.0, 0, 2.7};

// colori dei vertici
pen[]  colV = {p0,p0,ps,p0,p0,p0,p0,ps,p0};

// archi del grafo
int[][] E = {{1,0},{0,2},{0,8},{2,1},{1,8},{8,2},{2,3},{4,3},{3,5},{5,4},{5,6},{5,7}};

// label degli archi
real[] EL = {1.9, 3.3, 0.8, 1.5, 2.5, 2.7, 4.1, 3.5, 0.2, 3.6, 2.1, 2.9};

// colori degli archi
pen[]  colE = {p1};

real node_size = .4; // dimensione nodi
real txt_sizeV = 1.; // dimensione label nodi
real txt_sizeE = .8; // dimensione label archi
real max_bend  = .05; // massimo bending per un arco



// angolo dei self-loop e bending degli archi (automatico)
real[] deg, bend;

// disegno un nodo
void node(pair pos, string txt = "", pen color = black, pen background = white) {
	path c = circle(pos, node_size);
	fill(c, background);
	draw(c, black+1);
	label(scale(txt_sizeV)*txt, pos, color);
}

// disegno un arco
void edge(pair start, pair end, string txt = "", pen p = black, real deg = 0, real bending = 0, bool arrow = true) {
    path l;
    if (start != end) {
        pair d = (end-start);
        pair vec = (-d.y, d.x);
        d = unit(d)*node_size;
        pair mid = (start+end)/2;
        l = (start+d) .. (mid+vec*bending) .. (end-d);
    } else {
        pair center = start+dir(deg)*node_size*sqrt(2);
        if (arrow) l = arc(center, node_size, deg-135, deg+90) -- start+dir(deg+45)*node_size;
        else l = arc(center, node_size, deg-135, deg+135);
    }
    if (arrow) draw(l, p, EndArrow(21*node_size));
    else draw(l, p);
    label(scale(txt_sizeE)*txt, l, p);
}

// interfaccia per disegnare un arco che usa parametri ottimali calcolati
void ed(int i, string txt = "", pen p = black, bool arrow = true) {
    edge(P[E[i][0]], P[E[i][1]], txt, p, deg[E[i][0]], bend[i], arrow);
}

// riscala angolo in 0..360
real clockify(real x) {
    while (x<0)   x+=360;
    while (x>360) x-=360;
    return x;
}

// calcolo angoli e bending ottimali
void calcdeg() {
    bool[] hasloop, isdouble;
    real[][] deglist;
    for (int i=0; i<P.length; ++i) {
        hasloop[i] = false;
        deglist[i] = new real[];
        deg[i] = 0;
    }
    for (int i=0; i<E.length; ++i) {
        bend[i] = 0;
        if (E[i][0] == E[i][1]) hasloop[E[i][0]] = true;
        else {
            deglist[E[i][0]].push(degrees(P[E[i][1]]-P[E[i][0]]));
            deglist[E[i][1]].push(degrees(P[E[i][0]]-P[E[i][1]]));
        }
        isdouble[i] = false;
        for (int j=0; j<i; ++j) if (E[i][0] == E[j][1] && E[j][0] == E[i][1])
            isdouble[i] = isdouble[j] = true;
    }
    for (int i=0; i<P.length; ++i) {
        if (deglist[i].length == 0) continue;
        if (hasloop[i]) {
            real[] l = sort(deglist[i]);
            l.push(l[0]+360);
            int imax = 0;
            for (int i=1; i<l.length-1; ++i)
                if (l[i+1]-l[i] > l[imax+1]-l[imax]) imax = i;
            deg[i] = (l[imax]+l[imax+1])/2;
            deglist[i].push(clockify(deg[i]-45));
            deglist[i].push(clockify(deg[i]+45));
        }
        deglist[i] = sort(deglist[i]);
        deglist[i].push(deglist[i][0]+360);
        deglist[i].insert(0, deglist[i][deglist[i].length-2]-360);
    }
    for (int i=0; i<E.length; ++i) if (E[i][0] != E[i][1]) {
        real cws, cwe, ccs, cce, dg;
        int k0, k1, start = E[i][0], end = E[i][1];
        dg = degrees(P[E[i][1]]-P[E[i][0]]);
        for (k0=1; deglist[start][k0] < dg; ++k0);
        if (abs(deglist[start][k0-1]-dg) < abs(deglist[start][k0]-dg)) --k0;
        k1 = k0;
        if (isdouble[i]) {
            if (abs(deglist[start][k0+1]-dg) < abs(deglist[start][k0-1]-dg)) ++k1;
            else --k0;
        }
        cws = deglist[start][k1+1]-dg;
        ccs = dg-deglist[start][k0-1];
        dg = degrees(P[E[i][0]]-P[E[i][1]]);
        for (k0=1; deglist[end][k0] < dg; ++k0);
        if (abs(deglist[end][k0-1]-dg) < abs(deglist[end][k0]-dg)) --k0;
        k1 = k0;
        if (isdouble[i]) {
            if (abs(deglist[end][k0+1]-dg) < abs(deglist[end][k0-1]-dg)) ++k1;
            else --k0;
        }
        cwe = dg-deglist[end][k0-1];
        cce = deglist[end][k1+1]-dg;
        real cw = min(cws, cwe);
        real cc = min(ccs, cce);
        if (isdouble[i]) cc = 0;
        bend[i] = (min(cw/90,1)-min(cc/90,1))*max_bend;
    }
}

// calcolo parametri
for (int i=colV.length; i<P.length; ++i) colV[i] = colV[i-1];
for (int i=colE.length; i<E.length; ++i) colE[i] = colE[i-1];
calcdeg();
real ymax = 0;
for (int i=0; i<P.length; ++i) ymax = max(ymax, P[i].y);

// disegno nodi e archi
for (int i=0; i<P.length; ++i)
    node(P[i], string(VL[i]), black, colV[i]);
for (int i=0; i<E.length; ++i)
    ed(i, string(EL[i]), colE[i], false);

shipout("gradient");

path rect(pair a, pair b) {
    return roundedpath(a -- (a.x, b.y) -- b -- (b.x, a.y) -- cycle, 0.2);
}

picture pic;
unitsize(pic, 1cm);
draw(pic, rect(P[3]-(1.3,1.3)*node_size, P[3]+(1.3,1.3)*node_size), red+4);
add(pic, currentpicture);
shipout("gradient_highlight", pic);

colE[1]  = heavyred+2;
colE[3]  = heavyred+2;
colE[5]  = heavyred+2;
colE[8]  = heavyred+2;
colE[9]  = heavyred+2;
colE[10] = heavyred+2;
colE[11] = heavyred+2;
for (int i=0; i<E.length; ++i)
    ed(i, string(EL[i]), colE[i], false);
shipout("gradient_tree");
