import roundedpath;
unitsize(1cm);

pair SPACE = (0.4,2.5);
pair NSIZE = (1.2,0.6);

real THRESHOLD = 3*NSIZE.y + SPACE.y/2;

pair rdev(real r) {
    return ((unitrand(),unitrand())-(0.5,0.5)) * r;
}

path rect(pair a, pair b, real r = 0.1) {
    return roundedpath(a+rdev(r) -- (a.x, b.y)+rdev(r) -- b+rdev(r) -- (b.x, a.y)+rdev(r) -- cycle, 0.2);
}

real scaler(string s) {
    picture pic;
    unitsize(pic, 1cm);
    label(pic, "$"+s+"$", (0,0));
    return 2*min(NSIZE.x/size(pic, true).x, NSIZE.y/size(pic, true).y);
}

picture mkvt(string v ... picture[] subtrees) {
    picture pic;
    unitsize(pic, 1cm);

    bool lastsmall = false, thismall;
    real xoffs = 0, lastmax = 0;
    for (picture t : subtrees) {
        thismall = size(t, true).y < THRESHOLD;
        if (thismall && lastmax > 0) xoffs += NSIZE.x - lastmax;
        xoffs += max(t, true).x - (lastsmall ? -NSIZE.x : min(t, true).x) + SPACE.x;
        lastsmall = thismall;
        lastmax = thismall ? 0 : max(t, true).x;
    }
    if (lastmax > 0) xoffs += NSIZE.x - lastmax;
    xoffs -= SPACE.x;
    lastsmall = false;
    lastmax = 0;
    xoffs = -xoffs/2;
    for (picture t: subtrees) {
        thismall = size(t, true).y < THRESHOLD;
        if (thismall && lastmax > 0) xoffs += NSIZE.x - lastmax;
        real sx = -min(t, true).x;
        if (lastsmall) sx = NSIZE.x;
        pair s = (xoffs + sx,SPACE.y);
        draw(pic, s -- (0,0), black+2);
        add(pic, shift(s)*t);
        xoffs += max(t, true).x - (lastsmall ? -NSIZE.x : min(t, true).x) + SPACE.x;
        lastsmall = thismall;
        lastmax = thismall ? 0 : max(t, true).x;
    }

    filldraw(pic, rect(-NSIZE, NSIZE), palegray, black+2);
    label(pic, scale(0.9*scaler(v))*("$"+v+"$"), (0,0));
    return pic;
}

add(mkvt("3.1", mkvt("\infty"), mkvt("3.1", mkvt("\mathtt{mux}"), mkvt("\mathtt{false}"), mkvt("0"), mkvt("3.1", mkvt("\mathtt{minHood}"), mkvt("\phi_{tot}", mkvt("+"), mkvt("\phi_{d}", mkvt("3.1")), mkvt("\phi_{dist}", mkvt("\mathtt{nbrDist}"), mkvt("\phi_{dist}")), mkvt("\phi_{tot}")), mkvt("3.1")), mkvt("3.1"))));
