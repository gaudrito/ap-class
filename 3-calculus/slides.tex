\documentclass[10pt,xcolor=dvipsnames,usenames,dvipsnames,svgnames,table]{beamer} %,aspectratio=169,handout
	\usetheme{CambridgeUS} %CambridgeUS, Goettingen, Singapore, Madrid
	\setbeamertemplate{footline}[frame number]
	\setbeamercovered{transparent}
	\beamertemplatenavigationsymbolsempty

\usepackage{multimedia}
\usepackage{tabularx}
\usepackage{listings}
\usepackage{forloop}
\usepackage{mathtools}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{wasysym}
\newcolumntype{M}[1]{>{\centering\arraybackslash}m{#1}}
\setlength\tabcolsep{4pt}

\input{../macros}

\newcounter{prog}
\newcommand{\progress}[2]{$\forloop{prog}{0}{\value{prog} < #1}{\bullet}\forloop{prog}{0}{\value{prog} < #2}{\circ}$}
\newcounter{secslide}
\newcounter{sectotal}
\newcounter{secother}
\newcommand{\resetsec}[1]{\setcounter{secslide}{0}\setcounter{sectotal}{#1}}
\newcommand{\slidecount}{\addtocounter{secslide}{1}\setcounter{secother}{\value{sectotal} - \value{secslide}}\subsection[\progress{\value{secslide}}{\value{secother}}]{slide}}

\definecolor{darkgreen}{rgb}{0.0, 0.5, 0.0}
\definecolor{darkred}{rgb}{0.5, 0.0, 0.0}
\definecolor{darkblue}{rgb}{0.2, 0.2, 0.7}

\lstdefinelanguage{hfc}{
	basicstyle=\lst@ifdisplaystyle\small\fi\ttfamily,
	frame=single,
	basewidth=0.5em,
	sensitive=true,
	morestring=[b]",
	morecomment=[l]{//},
	morecomment=[n]{/*}{*/},
	commentstyle=\color{darkgreen},
	keywordstyle=\color{blue}\textbf, keywords={def}, otherkeywords={=>},
	keywordstyle=[2]\color{red}\textbf, keywords=[2]{rep,nbr,share,if,else,let,in},
	keywordstyle=[3]\color{violet}, keywords=[3]{mux,sumHood,countHood,everyHood,anyHood,minHood,maxHood,foldHood,maxHoodPlusSelf,nbrDist},
	keywordstyle=[4]\color{orange}\textbf, keywords=[4]{spawn},
	keywordstyle=[5]\color{blue}, keywords=[5]{false,true,infinity}
}
\lstdefinelanguage{fcpp}{
  keywords={class,struct,template,public,return,if,for,while,typename,using,namespace},
  keywordstyle=\color{blue}\textbf,
  otherkeywords={=>,<-,<\%,<:,>:,\#,@},
  keywordstyle=[2]\color{red},
  keywords=[2]{old,nbr,oldnbr,spawn},
  keywordstyle=[3]\color{violet},
  keywords=[3]{abf,min_hood,nbr_dist,and,or},
  keywordstyle=[4]\color{Emerald},
  keywords=[4]{void,bool,int,double,P,F,T,Ts,node_t,trace_t,trace_call,field,tuple,array,vector,vec},
  keywordstyle=[5]\color{Brown},
  keywords=[5]{FUN,GEN,FUN_EXPORT,GEN_EXPORT,ARGS,CODE,CALL,INF,true,false},
  sensitive=true,
  morecomment=[l]{//},
  morecomment=[n]{/*}{*/},
  commentstyle=\color{OliveGreen},
  morestring=[b]",
  morestring=[b]',
  morestring=[b]"""
}

\newcommand{\highlight}[1]{\textcolor{darkblue}{#1}}
\newcommand{\superlight}[1]{\textcolor{darkred}{#1}}
\newcommand{\greenlight}[1]{\textcolor{darkgreen}{#1}}
\newcommand{\DOI}[1]{\href{http://www.doi.org/#1}{\underline{#1}}}
\newcommand{\ACMDOI}[1]{\href{https://dl.acm.org/doi/#1}{\underline{#1}}}
\newcommand{\CITE}[5]{\ssymbol{#1}[#2. \emph{#3.} #4. \DOI{#5}]}

\theoremstyle{plain}
	\newtheorem{proposition}[theorem]{Proposition}
\theoremstyle{remark}
	\newtheorem{question}[theorem]{Question}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand\itemref[1]{\hspace{-5pt}{\renewcommand{\insertenumlabel}{\ref{#1}}\usebeamertemplate{enumerate item}}}
        
\title{Aggregate Programming for the Internet of Things}
\author{Giorgio Audrito, Ferruccio Damiani, Gianluca Torta}
\institute{{\small Dipartimento di Informatica\\[3pt] Universit\`a degli Studi di Torino}}
%\date{}
\titlegraphic{
	%\raisebox{-0.5\height}{\includegraphics[width=2.0cm]{../images/logo_horizon.jpg}} \quad
	\raisebox{-0.5\height}{\includegraphics[width=2.0cm]{../images/logo_torino.pdf}} \quad
	%\raisebox{-0.5\height}{\includegraphics[width=2.0cm]{../images/logo_sanpaolo.png}}
}

\begin{document}
\lstset{language={hfc}}

\section[~]{Title}
	\begin{frame}
		\titlepage
	\end{frame}

	\begin{frame}{Conceptual map}
		\vspace{-3mm}
		\begin{block}{}
		\begin{center}
			\includegraphics[height=0.86\textheight]{../images/map4.pdf}
		\end{center}
		\end{block}
	\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\resetsec{4}
\section{Implementing the model}

	\slidecount{}
	\begin{frame}{Outline}
		\begin{itemize}
			\item \highlight{Implementing the model}
			\begin{itemize}
				\item An abstract model
				\item A simple concrete model
			\end{itemize}
			\bigskip
		      \item The Field Calculus language
                        \bigskip
                      \item The eXchange Calculus (a short intro)
			\bigskip
			\item Concrete Implementations
		\end{itemize}
	\end{frame}

	\slidecount{}
	\begin{frame}[t]{An abstract model}
		\begin{block}{formal model of distributed computation}
			\begin{itemize}
				\item event structures: $\eventS$ \highlight{events}, $\neigh$ \highlight{message} DAG and $<$ \highlight{causality} partial order
				\onslide<2->{\item \highlight{space-time values} $\dvalue : \eventS \to X$ are labelling (functions) of event structures}
				\onslide<3->{\item composable \highlight{space-time functions} $f(\dvalue_\text{in}) = \dvalue_\text{out}$ are functions on s-t values}
			\end{itemize}
		\end{block}

		\begin{center}
			\raisebox{-0.5\height}{\includegraphics[width=0.35\linewidth]{img/estruct_input.pdf}}
			\raisebox{-0.5\height}{$\stackrel{f}{\longrightarrow}$}
			\raisebox{-0.5\height}{\includegraphics[width=0.35\linewidth]{img/estruct_gossip.pdf}}
		\end{center} \pause\pause\pause

		\begin{block}{}
			\begin{itemize}
				\item \highlight{programs} built by composing functions with nice global interpretation \pause
				\item \ldots we need a way to express basic functions via \highlight{local interactions}
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}[t]{An abstract model}
		\begin{block}{formal model of distributed computation}
			\begin{itemize}
				\item the abstract model doesn't care how the functions may be \highlight{implemented}
				\onslide<2->{\item \ldots even \highlight{C programs} sending custom network packets as they please}
				\onslide<3->{\item \ldots but composition in C \highlight{doesn't match} with model composition \frownie}
			\end{itemize}
		\end{block}

		\begin{center}
			\raisebox{-0.5\height}{\includegraphics[width=0.35\linewidth]{img/estruct_input.pdf}}
			\raisebox{-0.5\height}{$\stackrel{f}{\longrightarrow}$}
			\raisebox{-0.5\height}{\includegraphics[width=0.35\linewidth]{img/estruct_gossip.pdf}}  \pause\pause\pause
			\bigskip
			\bigskip

			\textbf{better try to use something more high-level!}
		\end{center}
	\end{frame}

	\slidecount{}
	\begin{frame}{A simple concrete model}
		\begin{block}{simplifying assumptions\ldots}
			\begin{itemize}
				\item the \highlight{same program} is executed in every event \pause
				\item \ldots can still execute different code through \highlight{branching} \pause
				\item messages are sent through \highlight{broadcast} (can extend to pointwise messages)
			\end{itemize}
		\end{block} \pause
		\bigskip

		\begin{minipage}{0.6\linewidth}
			\begin{block}{Round:}
				\begin{enumerate}
					\item \highlight{gather} data received, stored and sensed \pause
					\item compute the \highlight{program} \pause
					\item \highlight{broadcast} the result to neighbours \pause
					\item perform \highlight{actuation} as computed \pause
					\item receive messages while \highlight{sleeping}
				\end{enumerate}
			\end{block}
		\end{minipage} ~~
		\begin{minipage}{0.35\linewidth}
			\centering
			\includegraphics[width=0.7\linewidth]{img/dev-nei.png}
		\end{minipage}
	\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\resetsec{16}
\section{The Field Calculus language}

	\slidecount{}
	\begin{frame}{Outline}
		\begin{itemize}
			\item Implementing the model
			\bigskip
			\item \highlight{The Field Calculus language}
			\begin{itemize}
				\item Language features
				\item Field Calculus
				\item Coordination constructs
			\end{itemize}
			\bigskip
                        \item The eXchange Calculus (a short intro)
			\bigskip
			\item Concrete Implementations
		\end{itemize}
	\end{frame}

	\slidecount{}
	\begin{frame}{Language features}
		\begin{block}{}
			\begin{itemize}
				\item \highlight{implicit} messages via constructs with automatic send/receive matching \pause
				\item \ldots using received data to generate the message to send \pause
				\item \highlight{language} composition matching with \highlight{model} composition \pause
				\item \textbf{[universality]} shows that we can still express all s-t functions
			\end{itemize}
		\end{block} \pause

		\begin{center}
			\includegraphics[width=0.2\linewidth]{img/dev-nei.png}
		\end{center}

		\begin{block}{\ldots but how?}
			\begin{itemize}
				\item using \highlight{evaluation trees} to guide matching \pause
				\item modelling messages with \highlight{neighbouring field values}
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{Language features}
		\begin{minipage}{0.59\linewidth}
			\begin{block}{evaluation trees (value-tree for short)}
				\begin{itemize}
					\item in \highlight{functional} programming, a program is an \highlight{expression} that is \highlight{evaluated} \pause
					\item the resulting \highlight{value} is generated by evaluating sub-expressions \pause
					\item overall it produces an \highlight{ordered tree} of values
				\end{itemize}
			\end{block} \pause
		\end{minipage} ~
		\begin{minipage}{0.38\linewidth}
			\includegraphics[width=\linewidth]{img/valuetree.pdf}
		\end{minipage}
		\bigskip

		\begin{block}{implicit messages matching}
			\begin{itemize}
				\item messages are identified with the \highlight{nodes} in the tree originating them \pause
				\item \ldots basically with their \highlight{stack trace} \pause
				\item different branches of \texttt{if} are considered \highlight{different} \pause
				\item different call, different traces, no \highlight{interference} \pause
				\item works well with function \highlight{composition} and \highlight{branching}!
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{Language features}
		\begin{block}{messages as neighbouring field values}
			\begin{itemize}
				\item $\anyvalue$ are usual values (Boolean, integer, tuple\ldots) \pause
				\item $\fvalue = \envmap{\overline\deviceId}{\overline\anyvalue}$\footnote{we use $\overline{x}$ to denote the sequence $x_1, \ldots, x_n$.} is a map from \highlight{device} identifiers of neighbours $\overline\deviceId$ to \highlight{values} $\overline\anyvalue$ \pause
				\item we say that $\fvalue$ has \highlight{type} $\ftypeOf{\type}$ if all values $\overline\anyvalue$ have type $\type$ \pause
				\item regular values can be interpreted as \highlight{constant} neighbouring field values \pause
				\item $\fvalue$ represents \highlight{incoming} messages (\highlight{outgoing} if pointwise messages are allowed) \pause
				\item functions are overloaded to act \highlight{pointwise} on neighbouring field values: \\
				$\fvalue_1 + \fvalue_2 = \fvalue_3$ s.t. $\fvalue_3(\deviceId) = \fvalue_2(\deviceId) + \fvalue_1(\deviceId)$ for all $\deviceId$ \pause
				\item binary functions can be used to \highlight{aggregate} neighbouring field values: \\
				$\foldK(\min, \fvalue) = \minHood(\fvalue) = \min \bp{\fvalue(\deviceId) : ~ \deviceId \text{ neighbour}}$
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}[t]{Field Calculus}
		\begin{block}{a value $\anyvalue$ can be:}
			\begin{center}
				\footnotesize
				$
				\anyvalue \; \BNFcce \;  \lvalue \; \BNFmid \; \fvalue \; \BNFmid \; (\overline\xname) \toSymK \e \; \BNFmid \; \bname \; \BNFmid \; \funvalue
				$
			\end{center} \pause
			\vspace{-10pt}
			\begin{itemize}
				\item plain old value $\lvalue$ (\highlight{local} values) \pause
				\item \highlight{neighbouring field} value $\fvalue$ \pause
				\item \highlight{anonymous} function $(\overline\xname) \toSymK \e$ \pause
				\item \highlight{function} name (built-in $\bname$ or \highlight{defined} $\funvalue$): $\defK \funvalue(\overline\xname) \{ \e \}$
			\end{itemize}
		\end{block} \pause

		\begin{block}{an expression $\e$ can be:}
			\begin{center}
				\scriptsize
				$ \hspace{-1cm}
				\e \; \BNFcce \;  \xname \; \BNFmid \; \anyvalue \; \BNFmid \; \e(\overline{\e}) \; \BNFmid \; \letK \xname = \e \inK \e \; \BNFmid \; \ifK (\e) \{ \e \} \elseK \{ \e \} \; \BNFmid \; \repK(\e)\{ \e \} \; \BNFmid \; \nbrK\{\e\} \; \BNFmid \; \shareK(\e)\{ \e \} \hspace{-1cm}
				$
			\end{center} \pause
			\vspace{-10pt}
			\begin{itemize}
				\item \highlight{variable} $\xname$ (should be bounded in programs) \pause
				\item \highlight{value} $\anyvalue$ (no neighbouring $\fvalue$ in programs) \pause
				\item function \highlight{application} $\e(\overline\e)$ \pause
				\item \highlight{let} expression $\letK \xname = \e \inK \e$ \pause
				\item \highlight{branching} $\ifK$ and other \highlight{coordination} constructs $\repK$, $\nbrK$, $\shareK$ (we'll see)
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{Coordination constructs: $\repK(\e_i)\{ \e_f \}$}
		\begin{block}{}
			\begin{itemize}
				\item represents \highlight{state evolution} between rounds of computation \pause
				\item starts from an \highlight{initial value} $\e_i$ \pause
				\item repeatedly alters the state using an \highlight{update} function $\e_f$ \pause
			\end{itemize}
		\end{block}
		\bigskip

		\centerline{\framebox[\linewidth]{
			$\defK \; \texttt{counter}() \; \{ \repK(0) \{(\xname) \toSymK{\xname + 1} \} \}$
		}}
		\smallskip
		\emph{counts how many computational rounds have elapsed since the beginning.}
		\pause
		\bigskip

		\begin{minipage}{0.6\linewidth}
			\centering
			\includegraphics[width=0.9\linewidth]{img/structure.pdf}
		\end{minipage}
		\begin{minipage}{0.39\linewidth}
			\begin{block}{}
				\centering
				$\repK (0) \{ (\xname) \toSymK \xname + 1 \}$
				$$\longrightarrow \cp{(\xname) \toSymK \xname + 1}(1)$$
				$$\longrightarrow 1+1$$
				$\longrightarrow 2$, store 2
			\end{block}
		\end{minipage}
	\end{frame}

	\slidecount{}
	\begin{frame}{Coordination constructs: $\nbrK\{\e\}$}
		\begin{block}{}
			\begin{itemize}
				\item represents \highlight{interaction} between neighbour devices \pause
				\item sends result of $\e$ to neighbours (\highlight{duality} outgoing - incoming) \pause
				\item collects neighbour's values for the same $\e$ into a \highlight{neighbouring field} \pause
			\end{itemize}
		\end{block}
		\bigskip

		\centerline{\framebox[\linewidth]{
			\only<6>{$\defK \; \texttt{nbrCount}() \; \{ \texttt{sumHood}( \nbrK\{1\} ) \}$}\only<0>{;}
			\only<1-5>{$\nbrK\{\texttt{counter}()\}$}
		}}
		\smallskip
		\only<6->{\emph{counts the number of neighbours}}\only<6-|handout:0>{.}\only<0>{;}
		\only<1-5>{\emph{neighbouring field of counters.}}
		\pause
		\bigskip

		\begin{minipage}{0.6\linewidth}
			\centering
			\includegraphics[width=0.9\linewidth]{img/structure.pdf}
		\end{minipage}
		\begin{minipage}{0.39\linewidth}
			\begin{block}{}
				\centering
				$\e \longrightarrow 2$, broadcast 2
				$$\nbrK\{\e\} \longrightarrow \fvalue \text{ where}$$
				$$\fvalue = \deviceId_2 \mapsto 1, \deviceId_3 \mapsto 2, \deviceId_4 \mapsto 3$$
			\end{block}
		\end{minipage}
	\end{frame}

	\slidecount{}
	\begin{frame}{Coordination constructs: $\ifK ( \e ) \{ \e_\top \} \elseK \{ \e_\bot \}$}
		\begin{block}{}
			\begin{itemize}
				\item $\nbrK\{\e\}$ collects values among neighbours which calculated the \highlight{same} $\e$ \pause
				\item within a \highlight{branch}, if a neighbour chose another branch it is \highlight{not considered} \pause
				\item \ldots$\e(\overline\e)$ may also be a branch if $\e$ is not \highlight{constant} \pause
				\item the result is a restriction of the \highlight{domain} of the neighbouring field \pause
				\item \ldots may result in errors combining neighbouring fields with different domain \pause
				\item \ldots is a \highlight{feature} enabling computation in sub-networks \pause
			\end{itemize}
		\end{block}
		\bigskip

		\centerline{\framebox[\linewidth]{$
			\ifK (active) \{ \texttt{program}() \} \{ \texttt{null} \}
		$}}
		\smallskip
		\emph{executes a program on the sub-network of \emph{active} devices.}

		\begin{center}
			\includegraphics[width=0.39\linewidth]{img/splitting1.pdf} ~~
			\includegraphics[width=0.39\linewidth]{img/splitting2.pdf}
		\end{center}
	\end{frame}

	\slidecount{}
	\begin{frame}{Coordination constructs: $\shareK(\e_i)\{ \e_f \}$}
		\begin{block}{}
			\begin{itemize}
				\item represents \highlight{shared state evolution} \pause
				\item works as $\repK$ ($\e_i$ initial value, $\e_f$ update expression) \pause
				\item $\e_f$ is applied to the \highlight{neighbouring field} of values shared by neighbours \pause
				\item result is \highlight{shared} with neighbours in return \pause
			\end{itemize}
		\end{block}
		\smallskip

		\centerline{\framebox[\linewidth]{
			$\defK \; \texttt{gossip-min}(v) \; \{ \shareK (v) \{ (x) \toSymK \texttt{min}(\texttt{minHood}(x), v) \}$
		}}
		\medskip
		\emph{gossips the minimum value in a network.} \pause
		\bigskip

		\begin{minipage}{0.6\linewidth}
			\centering
			\includegraphics[width=0.9\linewidth]{img/estruct_gossip.pdf}
		\end{minipage}
		\begin{minipage}{0.39\linewidth}
			\begin{block}{}
				\centering
				\footnotesize
				$\texttt{gossip-min}(4) \longrightarrow$
				$$\shareK (4) \{ (x) \toSymK \texttt{min}(\texttt{minHood}(x), 4)\}$$
				$$\longrightarrow ((x) \toSymK \texttt{min}(\texttt{minHood}(x), 4))(\fvalue)$$
				$$\longrightarrow \texttt{min}(\texttt{minHood}(\fvalue), 4)$$
				$$\longrightarrow \texttt{min}(3, 4) \longrightarrow 3$$
				$$\fvalue = \deviceId_2 \mapsto 3, \deviceId_3 \mapsto 4, \deviceId_4 \mapsto 4$$
			\end{block}
		\end{minipage}
	\end{frame}

	\slidecount{}
	\begin{frame}[t]{The key  programming abstraction: computational field}
	     \vspace{-2mm}
		\begin{minipage}{0.75\linewidth}
			\alert{Program executed by a network of devices:}
			\vspace{-1mm}
			\begin{itemize}
				\item
				dynamic topology --- induced by (physical or logical) proximity of devices
				\vspace{-1mm}
				\item
				each device can:
				receive/send message to/from neighbours
				\vspace{-1mm}
				\item
				each device can: (asynchronously periodically) fire
			\end{itemize}
		\end{minipage}
		\begin{minipage}[t]{0.24\linewidth} \centering
			\visible<2->{
			\vspace{-11mm}
			\raisebox{-0.5\height}{\includegraphics[height=1.6cm]{img/network-abstraction.png}}
			}
		\end{minipage}
		\visible<3->{\vspace{1mm}
		\alert{Computational field}: (network global) distributed data structure mapping each device to a value}

		\visible<4->{\vspace{1mm}
		\alert{Neighbouring value}: (device local)  data structure, arising at each event $\epsilon$, mapping each supplier event device to a value (extracted from the message received)}

		\visible<5->{
		\vspace{2mm}
		\textbf{EXAMPLE}
		\vspace{3mm}

		\begin{minipage}{0.34\linewidth}
			\vspace{-1mm}
			\begin{itemize}
				\item[(a)] continuous computation of a temperature threshold
				\item[(b)] computed by a discrete network of devices
				\item[(c)] producing an approximation
			\end{itemize}
		\end{minipage}
		\begin{minipage}[t]{0.65\linewidth} \centering
			\raisebox{-0.4\height}{\includegraphics[height=2.5cm]{img/temperature-field.png}}
		\end{minipage}
		}
	\end{frame}

	\slidecount{}
\begin{frame}[fragile]{Examples}
	\begin{block}{A program that computes whether temperature is high}
\begin{lstlisting}[]
temperature() > 20 // bool
\end{lstlisting}
	\end{block} \pause

	\bigskip

	\begin{block}{A program that shows whether temperature is high}
\begin{lstlisting}[]
if (temperature() > 20) {set-led("green")} else {set-led("orange")}
\end{lstlisting}
	\end{block}
\end{frame}

	\slidecount{}
\begin{frame}[fragile]{Another example: Adaptive Bellman-Ford}
	\begin{block}{implementation with $\repK$ and $\nbrK$}
\begin{lstlisting}[]
def distanceTo(source) {
  rep (infinity) { (d) =>
    mux (source, 0, minHood(nbr{d} + nbrDist()))
  }
}
\end{lstlisting}
		\lstinline|nbrDist| returns a neighbouring field of distances from neighbours
	\end{block}
	\medskip

	\includegraphics[width=0.7\linewidth]{img/gradient.pdf}
	\includegraphics<2->[width=0.29\linewidth]{img/valuetree.pdf}
\end{frame}

	\slidecount{}
\begin{frame}[fragile]{Another example: Adaptive Bellman-Ford}
	\begin{block}{implementation with $\shareK$}
\begin{lstlisting}[]
def distanceTo(source) {
  share (infinity) { (d) =>
    mux (source, 0, minHood(d + nbrDist()))
  }
}
\end{lstlisting}
		\lstinline|nbrDist| returns a neighbouring field of distances from neighbours
	\end{block}
	\medskip

	\includegraphics[width=0.7\linewidth]{img/gradient.pdf}
	\includegraphics[width=0.29\linewidth]{img/valuetree.pdf}
\end{frame}

    \slidecount{}
	\begin{frame}{One more example: channel with obstacles (simulation)}
			\includegraphics[width=\textwidth]{img/SIMULATION-channel.png}
	\end{frame}

	\slidecount{}
	\begin{frame}[fragile]{}
\begin{small}
A function that broadcasts a value from a source
\begin{lstlisting}[]
def broadcast(source, value) { // (bool, num) -> num
  snd(share (pair(infinity, value)) { (x) =>
     mux(source, pair(0, value), minHood(pair(fst(x) + 1, snd(x)))) })
}
\end{lstlisting}

\smallskip

A function that computes the distance
\begin{lstlisting}[]
def distance(source, destination) { // (bool, bool) -> num
    broadcast(source, distanceTo(destination))
}
\end{lstlisting}

\smallskip

A function that computes a channel
\begin{lstlisting}[]
def channel(source, destination, width) { // (bool, bool, num) -> bool
  distanceTo(source) + distanceTo(destination)
    < width + distance(source, destination)
}
\end{lstlisting}

\smallskip

A function for a channel avoiding obstacles
\begin{lstlisting}[]
def channelAvoidingObstacles(o, s, d, w) {
   if (o) { false } else { channel(s,d,w) }
} // (bool, bool, bool, num) -> bool
\end{lstlisting}
\end{small}
	\end{frame}

	\slidecount{}
	\begin{frame}[fragile]{A wrong example}
A wrong function for a channel avoiding obstacles \\
\begin{lstlisting}[]
// (bool, bool, bool, num) -> bool
def channelAvoidingObstacles(o, s, d, w) {
   mux( o, false, channel(s,d,w) )
}
\end{lstlisting}
Do you see why it is wrong?
	\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\resetsec{5}
\section{The eXchange Calculus (a short intro)}

\slidecount{}
	\begin{frame}{Outline}
		\begin{itemize}
			\item Implementing the model
			\bigskip
		        \item The Field Calculus language
			  \bigskip
                        \item \highlight{The eXchange Calculus (a short intro)}
			  \bigskip
			\item Concrete implementations
		\end{itemize}
	\end{frame}

\slidecount{}
\begin{frame}{Round Execution Model}
	\begin{minipage}{0.48\linewidth}
		\begin{block}{}
			\begin{enumerate}
				\item \label{start} bind \highlight{context} from exports \pause
				\item \highlight{execute} the program \pause
				\item share \highlight{exports} \pause
				\item \highlight{sleep} until back to \itemref{start}
			\end{enumerate}
		\end{block} \pause
	\end{minipage}
	~~
	\begin{minipage}{0.48\linewidth}
		\begin{block}{}
			\begin{itemize}
				\item \highlight{sharing} by messages or other means \pause
				\item never during program \highlight{execution} \pause
				\item no wait, no \highlight{deadlocks}
			\end{itemize}
		\end{block}
	\end{minipage}
	\bigskip
	\begin{center}
		\includegraphics<1>[height=4cm]{./img/rounds1.pdf}\only<2->{\!\!\!\!}
		\includegraphics<2>[height=4cm]{./img/rounds2.pdf}\only<3->{\!\!\!\!}
		\includegraphics<3>[height=4cm]{./img/rounds3.pdf}\only<4->{\!\!}
		\includegraphics<4->[height=4cm]{./img/rounds4.pdf}
	\end{center}
\end{frame}

\slidecount{}
\begin{frame}{Sharing}
	\begin{block}{}
		\begin{center}
			$\dExchange{\e_i}{\nname}{\e_r}{\e_s}$
		\end{center} %\pause
%		\begin{itemize}
%			\item gather \highlight{received messages} in $\nname$ (using $\e_i$ as \highlight{default}) \pause
%			\item \highlight{send} $\e_s$ to neighbours, then \highlight{return} $\e_r$
%		\end{itemize}
	\end{block}
	\bigskip
	\centering
	\includegraphics[width=0.7\linewidth]{./img/exchange.pdf}
\end{frame}


\slidecount{}
\begin{frame}[b]{Neighbouring Values}
	\begin{block}{Design goals}
		\begin{itemize}
			\item collection of sent/received \highlight{messages} \pause
			\item handle neighbours \highlight{collectively}, not individually \pause
			\item cannot mention \highlight{specific} devices under \superlight{unreliability}
		\end{itemize}
	\end{block}
	\bigskip
	\bigskip
	\bigskip
	\centering
	\includegraphics[height=2.5cm]{./img/nvalue.pdf}
\end{frame}


\slidecount{}
\begin{frame}[b]{Neighbouring Values at Work}
%	\begin{block}{construction}
%		from exchange or \highlight{promoting} a local to $\lvalue[]$
%	\end{block} \pause
%	\smallskip
%
	\begin{block}{Pointwise map}
		\[
		2[\deviceId_1 \mapsto 3, \deviceId_2 \mapsto 1] + 20[\deviceId_1 \mapsto 10] = 22[\deviceId_1 \mapsto 13, \deviceId_2 \mapsto 21]
		\]
	\end{block} \pause
	\bigskip

	\begin{block}{Fold (in $\deviceId_1$) over neighbours ($\deviceId_2, \deviceId_3$)}
		\[
		\foldK(\max, 22[\deviceId_1 \mapsto 13, \deviceId_2 \mapsto 21], 0) = \max(0, 21, 22) = 22
		\]
	\end{block}
	\bigskip
	\centering
	\includegraphics[height=2.5cm]{./img/nvalue.pdf}
\end{frame}


\slidecount{}
\begin{frame}[fragile]{Message Alignment}
	\begin{block}{}
		\begin{itemize}
			\item each exchange instruction shares an \highlight{implicit message} \pause
			\item different \highlight{exchange} \superlight{$\longrightarrow$ separate messages} \pause
			\item \highlight{matched} by:
			\quad \usebeamertemplate{itemize item}{} position in program
			\quad \usebeamertemplate{itemize item}{} stack trace
			\quad \superlight{$\longrightarrow$ enables composition}
		\end{itemize}
	\end{block}
	\bigskip

	\begin{center}
		\includegraphics<2->[height=3.5cm]{./img/alignment.png}
		\vspace{99cm}
	\end{center}
\end{frame}

\resetsec{8}
\section{Concrete implementations}

\slidecount{}
	\begin{frame}{Outline}
		\begin{itemize}
			\item Implementing the model
			\bigskip
		        \item The Field Calculus language
			  \bigskip
                        \item The eXchange Calculus (a short intro)
			  \bigskip
			\item \highlight{Concrete implementations}
			\begin{itemize}
				\item Language incarnations
				\item Protelis/Scafi + Alchemist
				\item FCPP language, simulator and deployment
			\end{itemize}
		\end{itemize}
	\end{frame}

	\slidecount{}
	\begin{frame}{Language incarnations} \small
		\begin{block}{Proto (2008)}
			\vspace{-9pt}
			\begin{center}
				\superlight{\texttt{( rep x 0 (+ x 1) )}}
			\end{center}
			\vspace{-13pt}
			\begin{itemize}
				\item based on \highlight{Scheme} (not very intuitive), out of support
			\end{itemize}
		\end{block} \pause

		\begin{block}{Protelis (2014)}
			\vspace{-9pt}
			\begin{center}
				\superlight{\texttt{rep (x <- 0) \{ x + 1 \}}}
			\end{center}
			\vspace{-13pt}
			\begin{itemize}
				\item custom language based on \highlight{Java}
				\item not typed, very inefficient, but \highlight{used} at BBN technology
			\end{itemize}
		\end{block} \pause

		\begin{block}{ScaFi (2016)}
			\vspace{-9pt}
			\begin{center}
				\superlight{\texttt{rep (0.0) \{ x => x + 1 \}}}
			\end{center}
			\vspace{-13pt}
			\begin{itemize}
				\item internal language in \highlight{Scala}
				\item typed but complex, reasonably efficient, \highlight{semantic issues}
			\end{itemize}
		\end{block} \pause

		\begin{block}{FCPP (2020)}
			\vspace{-4pt}
			\begin{center}
				\superlight{\texttt{old(CALL, 0.0, [\&](double x) -> double \{ return x+1; \})}}
			\end{center}
			\vspace{-12pt}
			\begin{itemize}
				\item internal language in \highlight{C++}
				\item typed but complex, optimised for efficiency
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{Protelis/Scafi languages + Alchemist simulator}
		\begin{block}{main features}
			\begin{itemize}
				\item run on \highlight{JVM}:
				\begin{itemize}
					\item values are Java/Scala \highlight{objects}
					\item can use Java/Scala \highlight{code} \pause
					%\superlight{\texttt{import java.lang.Double.*}}
					%\item can \highlight{call} non-static methods on objects: \superlight{\texttt{x.getDistance(y)}}
					\item \ldots not very \highlight{efficient}
				\end{itemize} \pause
				\item special objects to access \highlight{built-in} functions and \highlight{simulator} features \pause
%				\item \highlight{\texttt{self}} special object for built-in functions: \superlight{\texttt{self.getCurrentTime()}} \pause
%				\item \highlight{\texttt{env}} special object for simulator communication: \superlight{\texttt{env.put("test", 0)}}
				\item simulation of linear movement on 2D \highlight{euclidean} space or \highlight{street maps} \pause
				\item interactive \highlight{GUI} or simulation \highlight{batches} \pause
				\item tutorial available at:
				\highlight{\small\url{https://bitbucket.org/gaudrito/alchemist-example}}
			\end{itemize}
		\end{block}

		\bigskip
		\begin{center}
			\raisebox{-0.5\height}{\includegraphics[height=1.9cm]{img/alchemist.png}}
		\end{center}
	\end{frame}

%	\slidecount{}
%	\begin{frame}{Protelis}
%		\begin{block}{syntax}
%			\begin{itemize}
%				\item {import Protelis module:} \superlight{\texttt{import protelis:lang:utils}} \pause
%				\item {function definition:} \superlight{\texttt{def f(x) = e}} or \superlight{\texttt{def f(x) \{ x+1 \}}} for complex body \pause
%				\item {anonymous functions:} \superlight{\texttt{\{x -> x+1\}}} or \superlight{\texttt{\{e\}}} if no arguments \pause
%				\item {sequencing:} \superlight{\texttt{x.f(); x.g()}} \pause
%				\item {let-assignment:} \superlight{\texttt{let x = 0; x+42}} \pause
%				\item {tuples:} \superlight{\texttt{[ 0, true, 4.2, "hello" ]}} \pause
%				\item {branching:}
%				\begin{itemize}
%					\item \superlight{\texttt{if (c) \{0\} else \{1\}}} (splits computation)
%					\item \superlight{\texttt{mux (c) \{0\} else \{1\}}} (computes both branches)
%				\end{itemize} \pause
%				\item {coordination:}
%				\begin{itemize}
%					\item \superlight{\texttt{nbr(x)}}
%					\item \superlight{\texttt{rep (x <- 0) \{x+1\}}}
%					\item \superlight{\texttt{share (x <- 0) \{x+1\}}}
%				\end{itemize}
%			\end{itemize}
%		\end{block}
%	\end{frame}
%
%	\slidecount{}
%	\begin{frame}{Alchemist setup}
%		\begin{block}{install:}
%			\begin{itemize}
%				\item
%				\highlight{Java SE JDK} 11:
%				\superlight{\url{https://www.oracle.com/technetwork/java/javase/downloads}}
%
%				\item
%				\highlight{Eclipse} editor:
%				\superlight{\url{https://www.eclipse.org/downloads}}
%
%				\item
%				\highlight{YAML} editor of your choice from \texttt{Help > Eclipse Marketplace}
%
%				\item
%				\highlight{Protelis} from \texttt{Help > Eclipse Marketplace}
%			\end{itemize}
%		\end{block} \pause
%
%		\begin{block}{then (tomorrow):}
%			\begin{itemize}
%				\item
%				download the \highlight{sample project} at:
%				\superlight{\url{https://bitbucket.org/gaudrito/alchemist-example}}
%
%				\item
%				import it as \highlight{gradle project} with \texttt{File > Import} \\
%				using type \texttt{Gradle > Gradle Project}
%
%				\item
%				\textbf{\ldots you're ready to go!}
%			\end{itemize}
%		\end{block}
%	\end{frame}

	\slidecount{}
	\begin{frame}{FCPP language, simulator and deployment platform}
		\begin{block}{main features}
			\begin{itemize}
				\item \highlight{C++ library} used to develop distributed programs using it
				\begin{itemize}
					\item manipulates C/C++ \highlight{values}
					\item can use external C/C++ \highlight{code}
					\item portable to any architecture with C++ \highlight{compiler}
				\end{itemize} \pause
				\item \highlight{performance optimized} at compile-time \pause
				\item \highlight{extensible} component-based architecture
			\end{itemize}
		\end{block}

		\bigskip
		\begin{center}
			\raisebox{-0.5\height}{\includegraphics[width=0.8\textwidth]{img/architecture.pdf}}
		\end{center}
	\end{frame}

	\slidecount{}
	\begin{frame}{FCPP language, simulator and deployment platform}
		\begin{block}{application scenarios}
			\begin{itemize}
				\item \highlight{dynamic HPC} computations (in development) \pause
				\item \highlight{microcontroller} (WSN) distributed networks (Miosix, Contiki) \pause
				\item \highlight{batches} of simulations of WSN on HPC platforms \pause
				\item \highlight{3D interactive} graphical simulations of WSN
			\end{itemize}
		\end{block}

		\bigskip
		\begin{center}
			\raisebox{-0.5\height}{\includegraphics[width=0.8\textwidth]{img/fcpp.jpg}}
		\end{center}
	\end{frame}

	\slidecount{}
	\begin{frame}{FCPP language, simulator and deployment platform}
		\begin{block}{simulation features}
			\begin{itemize}
				\item \highlight{2D and 3D} physics with acceleration and friction
				\begin{itemize}
					\item \superlight{missing:} rotation/angular momentum
					\item \superlight{missing:} collisions (with obstacles/between nodes)
					\item \superlight{missing:} force fields (gravity)
				\end{itemize} \pause
				\item simplified \highlight{wireless connection} models
				\begin{itemize}
					\item \superlight{missing:} realistic wireless connection
					\item \superlight{missing:} logical connection models
				\end{itemize} \pause
				\item random or regular \highlight{geometric} deployments
				\begin{itemize}
					\item \superlight{missing:} dynamic/reactive deployments
				\end{itemize}
			\end{itemize}
		\end{block}

		\begin{center}
			\raisebox{-0.5\height}{\includegraphics[width=0.6\textwidth]{img/fcpp.jpg}}
		\end{center}
	\end{frame}

	\slidecount{}
	\begin{frame}[fragile]{FCPP language, simulator and deployment platform}
		\begin{block}{main language features}
			\begin{itemize}
				\item special \superlight{\tt node} object to access \highlight{built-in} functions and \highlight{simulator} features \pause
				\item \superlight{\tt field<T>} type for neighbouring values \pause
				\item history access with \superlight{\tt old} ($\repK$ in field calculus) \pause
				\item neighbour access with \superlight{\tt nbr} ($\nbrK$, $\shareK$ in field calculus) \pause
				\item \superlight{\tt FUN}, \superlight{\tt ARGS}, \superlight{\tt CODE}, \superlight{\tt CALL} macros \pause
				\item basic programming syntax inherited from \superlight{C/C++} \pause
				\item exercises project available at:
				\highlight{\small\url{https://github.com/fcpp/fcpp-exercises}}
			\end{itemize}
		\end{block}

		\smallskip
		\begin{center}
			\only<1-2| handout:0>{\raisebox{-0.5\height}{\includegraphics[height=3cm]{img/architecture.pdf}}\bigskip\bigskip}
\begin{lstlisting}[language=fcpp]
FUN double abf(ARGS, bool source) { CODE
  return nbr(CALL, INF, [&] (field<double> d) {
    double v = source ? 0.0 : INF;
    return min_hood(CALL, d + node.nbr_dist(), v);
  });
}
\end{lstlisting}
		\end{center}
	\end{frame}

	\slidecount{}
	\begin{frame}{FCPP language, simulator and deployment platform}
		\begin{block}{reduced syntax}
			\begin{itemize}
				\item {aggregate function:} \superlight{\texttt{FUN t f(ARGS, t x$\ast$) \{ CODE i \}}} \pause
				\item {anonymous functions:} \superlight{\texttt{[\&](t x$\ast$)\{ i \}}} where \superlight{\tt i} instruction \pause
				\item {function call:} \superlight{\tt f(CALL, v$\ast$)} \emph{(aggregate)} \superlight{\tt f(v$\ast$)} \emph{(anonymous/pure)} \pause
				\item {instruction:} \superlight{\tt t x = e; i} or \superlight{\tt return e;} where \superlight{\tt e} expression \pause
				\item {branching:}
				\begin{itemize}
					\item \superlight{\texttt{c~?~e~:~e}} (splits computation)
					\item \superlight{\texttt{mux(c, e, e)}} (computes both branches)
				\end{itemize} \pause
				\item {coordination:}
				\begin{itemize}
					\item \superlight{\texttt{old(CALL, sensor\_value\textcolor{gray}{, base\_value})}}
					\item \superlight{\texttt{old(CALL, 0, [\&](int x) \{return x+1;\})}}
					\item \superlight{\texttt{nbr(CALL, 1\textcolor{gray}{, base\_value})}}
					\item \superlight{\texttt{nbr(CALL, 0, [\&](field<int> x) \{return max\_hood(CALL, x)+1;\})}}
				\end{itemize} \pause
				\item library coordination functions in the \superlight{\texttt{coordination}} namespace \pause
				\item sensors/actuators as \superlight{\texttt{node}} methods from the \superlight{\texttt{component}} namespace
			\end{itemize}
		\end{block}
	\end{frame}

\end{document}
