\documentclass[a4paper,oneside]{article}
\usepackage[a4paper,top=2cm,bottom=2cm,right=2.5cm,left=2.5cm]{geometry}
\usepackage[english]{babel}
\usepackage{hyperref}
\usepackage{graphicx}
\renewcommand{\familydefault}{cmss}

\newcommand\footer[1]{%
  \begingroup
  \renewcommand\thefootnote{}\footnotetext{#1}%
  \addtocounter{footnote}{-1}%
  \endgroup
}


\begin{document}

\title{\vspace{-1cm}Alchemist Simulation Basics}
\author{Giorgio Audrito}
\maketitle
\footer{\begin{center}
	\includegraphics[height=0.8cm]{img/creative_commons.png}
\end{center}}

\section{Setup}

\begin{enumerate}
	\item Install the virtual machine \emph{Java SE 11 JDK}:\\[-16pt]
	\begin{center}
		\tt \href{http://oracle.com/technetwork/java/javase/downloads}{oracle.com/technetwork/java/javase/downloads}
	\end{center}
	
	\item Install the \emph{Eclipse} or \emph{IntelliJ} IDE:\\[-16pt]
	\begin{center}
		\tt \href{https://eclipse.org/downloads}{eclipse.org/downloads} \\
		\tt \href{https://www.jetbrains.com/idea/download}{jetbrains.com/idea/download}
	\end{center}
	Eclipse has better support for Protelis (buggy on ScaFi), while IntelliJ has better support for ScaFi (no Protelis syntax highlighting).
	
	\item
	Install a YAML support plugin of your choice.
	
	\item
	If you plan to use Protelis on Eclipse, install the \emph{Protelis} plugin from \verb|Help > Eclipse Marketplace| for syntax highlighting (not available for IntelliJ).
	
	\item
	If you plan to use ScaFi on Eclipse, install the \emph{Scala IDE} available from \verb|Help > Eclipse Marketplace|. For using ScaFi on IntelliJ, click on \verb|File > Project Structure > Global Libraries|. In the window that will open, search for \verb|Create > Download|, then select version \verb|2.12.3| and press \verb|OK|. Wait for the download to end then press \verb|OK| again.
	
	\item 
	Download the sample project, available at:\\[-16pt]
	\begin{center}
		\href{https://bitbucket.org/gaudrito/alchemist-example}{bitbucket.org/gaudrito/alchemist-example}
	\end{center}
	
	\item 
	Import the downloaded folder as a Gradle project.
	\begin{itemize}
		\item
		\textbf{\underline{Eclipse:}} click on \verb|File > Import|, then select the type \verb|Gradle > Gradle Project|. Click on \verb|Next| twice, then select the downloaded folder through \verb|Browse| in the upper right corner. Complete the procedure by clicking on \verb|Finish|. If errors appear, you may also need to right-click on the newly created project and then click on \verb|Gradle > Refresh Gradle Project|.
		\item
		\textbf{\underline{IntelliJ:}} click on \verb|File > Open| then select the \verb|build.gradle| file within the downloaded folder. Click on \verb|Open as Project| then compile the resulting form as in the following:
		\begin{center}
			\includegraphics[width=\linewidth]{img/import-intelliJ.png}
		\end{center}
		%If you plan to use ScaFi, open file \verb|src/main/example.scala|. A yellow banner will appear: click there on \verb|Setup Scala SDK|.
	\end{itemize}
\end{enumerate}


\section{The sample project}


\subsection{Run the simulations}

In order to execute the project, you need to create a new \emph{run configuration}.
\begin{itemize}
	\item
	\textbf{\underline{Eclipse:}} click on \verb|Run > Run Configurations|, select type \emph{``Java Application''} then click on the \emph{``New''} button (in the upper left corner). In the \emph{``Main''} tab enter the following:
	\begin{description}
		\item[Project:] the sample project
		\item[Main class:] \verb|it.unibo.alchemist.Alchemist|
	\end{description}
	In the \emph{``Arguments''} tab enter the following:
	\begin{description}
		\item[Program arguments:] \verb|-g src/main/resources/example.aes| \\ \verb|-y src/main/resources/protelis.yml|
	\end{description}
	You can then click on \verb|Run| to launch the Alchemist graphical interface.
	
	\item
	\textbf{\underline{IntelliJ:}} click on \verb|Add Configuration > + > Application| and then set:
	\begin{description}
		\item[Main class:] \verb|it.unibo.alchemist.Alchemist|
		\item[Program arguments:] \verb|-g src/main/resources/example.aes| \\ \verb|-y src/main/resources/scafi.yml|
		\item[Use classpath of module:] \verb|Alchemist_Example_main|
		\item[Shorten command line:] \verb|JAR manifest|
	\end{description}
	Press \verb|OK| and then the green play button. The window should resemble the following:
	\begin{center}
		\includegraphics[width=\linewidth]{img/runconfig-intelliJ.png}
	\end{center}
	\item 
	\textbf{\underline{Command line:}} it is also possible to launch the simulations without an editor, using the commands:
	\begin{center}
		\begin{tabular}{ll}
			\verb|./gradlew runProtelis| & \verb|gradlew.bat runProtelis| \\
			\verb|./gradlew runScafi| & \verb|gradlew.bat runScafi|
		\end{tabular}
	\end{center}
	depending on whether you want to run Protelis or ScaFi, and on whether your platform is Unix/Linux/MacOS or Windows.
\end{itemize}
Once the Alchemist graphical interface is ready, you can:
\begin{itemize}
	\item press \verb|P| to play/pause the simulation;
	\item press \verb|L| to toggle display of links between devices;
	\item press \verb|M| to toggle display of the marker;
	\item mouse scroll wheel to zoom in/out and drag to move the viewport;
	\item double click on a node to view additional details on its contents;
	\item change the graphical effects (upper left side of the window).
\end{itemize}


\subsection{Inspect and modify the project}

In the project explorer (left side) you can inspect the folder structure of the sample project. Expand the needed folders to find the main files of this project:
\begin{itemize}
	\item \verb|src/main/resources|
	\begin{itemize}
		\item \texttt{protelis.yml}: the description of the Protelis simulation scenario.
		\item \texttt{scafi.yml}: the description of the Scafi simulation scenario.
		\item \texttt{example.aes}: the graphical effects displayed by the simulator interface.
	\end{itemize}
	\item \verb|src/main/protelis|
	\begin{itemize}
		\item \texttt{example.pt}: the Protelis program to be run.
	\end{itemize}
	\item \verb|src/main/scala|
	\begin{itemize}
		\item \texttt{example.scala}: the Scafi program to be run.
	\end{itemize}
\end{itemize}

\subsubsection{The program}

The (commented) program contains few helper functions, including a function \verb|elapsed| counting elapsed rounds, \verb|constant| to obtain values that do not change across rounds, \verb|ABF| computing the adaptive Bellman-Ford algorithm. The \verb|main| function starts with two blocks setting the source and getting distance information respectively. The last blocks regulate the movement of devices: first by computing a \texttt{timeToGo} after which movement starts, and then a \texttt{target} towards which the device should move.

\subsubsection{Simulation scenario}

The simulation scenario contains few sections. First, some general information is set: the chosen language (Protelis or ScaFi), seeds for the random number generator, the network model (in this case, connection happens when the euclidean distance is smaller than 1).

In section \texttt{pools} are reported things to be linked later, describing the events to be executed (program execution, Brownian movement, moviment towards a target), together with a temporal distribution and other parameters.

Finally, in section \texttt{displacements} the devices are displaced according to geometric shapes (point, circle, rectangle), and are associated to events to be executed (and possibly custom data contents).

\subsubsection{Graphical effects}

The graphical effects file is not meant to be manually edited, but modified directly from the Alchemist graphical interface instead, clicking on the rectangular shape in the upper left side, close to the arrows, with a \verb|DS| label on it. A dialog will appear, showing that the current effect is tuning node colors through the ``molecule'' \verb|abf| (value exported into the simulator through \verb|put|), together with parameters tuning the color palette.


\section{Exercises}

Edit the file \verb|example.pt| or \verb|example.scala|, incrementally, in order to compute the following in every device.
\begin{enumerate}
	\item The number of neighbour devices.
	\item The maximum number of neighbour devices ever witnessed by the current device.
	\item The maximum number of neighbour devices ever witnessed by any device in the network.
	\item Move towards the neighbour with the lowest number of neighbours.
	\item Move away from the neighbour with the highest number of neighbours.
	\item Move as if the device was attracted by the neighbour with the lowest number of neighbours, and repulsed by the neighbour with the highest number of neighbours.
	\item Move as if the device was repulsed by every neighbour, and by the four walls of the rectangular box between points \verb|[-4,-3]| and \verb|[4,3]|.
\end{enumerate}


\subsection{Hints}

\begin{itemize}
	\item
	In the first few exercises, start by reasoning on when/where to use \verb|nbr| (``collecting from neighbours'') and \verb|rep| (``collecting from the past'').
	\item
	In order to move a device, you need to store a coordinate in the \verb|target| variable of the simulator, with \verb|env.put("target", ...)| in Protelis, or \verb|node.put("target", ...)| in ScaFi. Part of \verb|example.*| is already handling this, so you can just edit that part.
	\item
	You can use the defined function \verb|getCoordinates()| to get the position of the current device. Coordinates can be composed as physical vectors in Protelis:
	\begin{center}
		\verb|[1,3] + [2,-1] == [3,2]| \\
		\verb|[2,4] * 0.5    == [1,2]|
	\end{center}
	Similar operators are also defined in \verb|example.scala|.
	\item
	In the last few exercises, you can model attraction/repulsion using the classical \emph{inverse square law}. More precisely, if $\vec{v}$ is the vector between two objects, the resulting force is $\vec{v} / |\vec{v}|^3$ where $|\vec{v}| = \sqrt{v_x^2 + v_y^2}$.
\end{itemize}

\section{Links}

\begin{itemize}
	\item The official Protelis webpage:\\[-16pt]
	\begin{center}
		\tt \href{http://protelis.github.io}{protelis.github.io}
	\end{center}

	\item The official Scafi webpage:\\[-16pt]
	\begin{center}
		\tt \href{https://scafi.github.io/}{scafi.github.io}
	\end{center}

	\item The official Alchemist webpage:\\[-16pt]
	\begin{center}
		\tt \href{http://alchemistsimulator.github.io}{alchemistsimulator.github.io}
	\end{center}
%	
%	\item Un introduzione all'Aggregate Programming, con presentazione:\\[-16pt]
%	\begin{center}
%		\tt
%		\href{http://www.sti.uniurb.it/events/sfm16quanticol/slides/beal_1.pdf}{www.sti.uniurb.it/events/sfm16quanticol/slides/beal\_1.pdf} \\
%		\href{http://www.sti.uniurb.it/events/sfm16quanticol/slides/beal_2.pdf}{www.sti.uniurb.it/events/sfm16quanticol/slides/beal\_2.pdf} \\
%		\href{http://www.sti.uniurb.it/events/sfm16quanticol/slides/beal_3.pdf}{www.sti.uniurb.it/events/sfm16quanticol/slides/beal\_3.pdf}
%	\end{center}
%	e con articolo:\\[-16pt]
%	\begin{center}
%		\tt \href{http://web.mit.edu/jakebeal/www/Publications/QUANTICOL16-AggregateProgramming.pdf}{web.mit.edu/jakebeal/www/Publications/\\QUANTICOL16-AggregateProgramming.pdf}
%	\end{center}
\end{itemize}

\end{document}
