real urand() {
    return 0.1+0.8*unitrand();
}

pen shader(real val) {
    return hsv(360*val, 1, 1);
}

void drawnode(picture pic, real scale, pair pos, pen col) {
    pen fg = 0.5*col;
    pen bk = col;
    fill(pic, circle(pos, scale*.19), black);
    fill(pic, circle(pos, scale*.16), bk);
}

int[] S = {2, 3, 4, 6, 10, 15};      // frames
pair DIM = (12,3);                   // area
srand(2);                            // random seed

for (int k=0; k<S.length; ++k) {
    picture pic;
    unitsize(pic, 1cm);
    for (real i=0; i<4*S[k]; ++i) {
        for (real j=0; j<S[k]; ++j) {
            pair P = ((i+urand())*3/S[k], (j+urand())*3/S[k]);
            real V = length(P - 0.5*DIM) * 1.8 / length(DIM);
            drawnode(pic, 3/S[k], P, shader(V));
        }
    }
    shipout("approximation" + string(k+1), pic);
}
