unitsize(1cm);
import patterns;
import roundedpath;

add("hatchfront",hatch(.5mm, gray));
add("hatchback", hatch(.5mm, NW, gray));
add("hatchsoft", hatch(.5mm, NW, gray(0.65)));
add("crosshatch",crosshatch(.5mm, gray));

real NODESIZE = 0.3;

string align(string s) {
    return "\phantom{d}" + s + "\phantom{p}";
}
string it(string s) {
    return "\textit{\phantom{dp}\hspace{-13pt}" + s + "}";
}

pair rdev(real r) {
    return ((unitrand(),unitrand())-(0.5,0.5)) * r;
}

path rect(pair a, pair b, real r = 0.01) {
    return roundedpath(a+rdev(r) -- (a.x, b.y)+rdev(r) -- b+rdev(r) -- (b.x, a.y)+rdev(r) -- cycle, 0.2);
}

void main(int seed) {
    srand(seed);
    draw((0,0) -- (0,8), EndArrow);
    draw((0,0) -- (8,0), EndArrow);
    label(rotate(90)*align("space"), (.15,7.15));
    label(align("time"), (7.2,.15));

    label(scale(0.8)*it("independent"), (.05,2), W);
    label(scale(0.8)*it("continuous"), (.05,4), W);
    label(scale(0.8)*it("discrete"), (.05,6), W);
    label(rotate(90)*scale(0.8)*it("ind."),  (2,.05), S);
    label(rotate(90)*scale(0.8)*it("cont."), (4,.05), S);
    label(rotate(90)*scale(0.8)*it("disc."), (6,.05), S);

    for (int i=1; i<4; ++i) {
        draw(2*(i,0) -- 2*(i,.05));
        draw(2*(0,i) -- 2*(.05,i));
        draw(2*(i,.05) -- 2*(i,1), mediumgray+dotted);
        draw(2*(.05,i) -- 2*(1,i), mediumgray+dotted);
        for (int j=1; j<3; ++j) {
            draw(2*(i,j) -- 2*(i,j+1-NODESIZE), heavygray+dotted, EndArrow(5));
            draw(2*(j,i) -- 2*(j+1-NODESIZE,i), heavygray+dotted, EndArrow(5));
        }
    }

    for (int x=1; x<4; ++x)
        for (int y=1; y<4; ++y) {
            filldraw(circle(2*(x,y), 2*NODESIZE), paleblue, deepblue+2);
        }
        
    guide g;
    for (int i=0; i<360; i+=45) g = g :: dir(i)*(0.5+unitrand())/3;
    g = g :: cycle;
    path wobble = g;

    // SI TI
    draw((2,2), black+2.5);
    // SI TC
    draw((3.7,2) -- (4.3,2), black+1.5);
    // SI TD
    draw((5.7,2), black+2.5);
    draw((6.0,2), black+2.5);
    draw((6.3,2), black+2.5);
    draw((5.7,2) -- (6.3,2), black+dotted);
    // SC TI
    filldraw(shift(2,4)*wobble, pattern("hatchback"), black+1.5);
    // SC TC
    int n1 = 1;
    for (; ; ++n1)
        if (point(wobble,0.5*n1).y > point(wobble,0.5*(n1-1)).y && point(wobble,0.5*n1).y > point(wobble,0.5*(n1+1)).y)
            break;
    int n2 = -1;
    for (; ; --n2)
        if (point(wobble,0.5*n2).y < point(wobble,0.5*(n2-1)).y && point(wobble,0.5*n2).y < point(wobble,0.5*(n2+1)).y)
            break;
    n2 += 16;
    fill(shift(3.7,4)*scale(0.4,0.9)*wobble, pattern("hatchsoft"));
    draw(shift(3.7,4)*scale(0.4,0.9)*subpath(wobble, 0.5*n2, 0.5*n1+8), heavygray+dotted);
    for (int i=n2-15; i<n1; ++i)
        draw(shift(scale(0.4,0.9)*point(wobble,0.5*i))*((3.7,4) -- (4.3,4)), gray+dotted);
    for (int i=n1+1; i<n2; ++i)
        draw(shift(scale(0.4,0.9)*point(wobble,0.5*i))*((3.7,4) -- (4.3,4)), heavygray);
    draw(shift(scale(0.4,0.9)*point(wobble,0.5*n1))*((4.3,4) -- (3.7,4)) -- shift(3.7,4)*scale(0.4,0.9)*subpath(wobble, 0.5*n1, 0.5*n2) -- shift(scale(0.4,0.9)*point(wobble,0.5*n2))*((3.7,4) -- (4.3,4)), black+1);
    filldraw(shift(4.3,4)*scale(0.4,0.9)*wobble, pattern("hatchback"), black+1);
    // SC TD
    filldraw(shift(5.7,4)*scale(0.3,0.9)*wobble, pattern("hatchback"), black+0.7);
    filldraw(shift(6.0,4)*scale(0.3,0.9)*wobble, pattern("hatchback"), black+0.7);
    filldraw(shift(6.3,4)*scale(0.3,0.9)*wobble, pattern("hatchback"), black+0.7);
    // SD TI
    pair[] graph = {(0,0), dir(-90)*0.3, dir(-20)*0.4, dir(50)*0.3, dir(130)*0.35, dir(190)*0.4};
    for (pair p : graph)
        draw(shift(2,6)*p, black+2.5);
    for (int i=2; i<=5; ++i)
        draw(shift(2,6)*(graph[0] -- graph[i]), black+dotted);
    for (int i=1; i<=2; ++i)
        draw(shift(2,6)*(graph[i] -- graph[i+1]), black+dotted);
    // SD TC
    for (int i=2; i<=5; ++i)
        for (int j=0; j<=2; ++j)
            draw(shift(3.7+0.3*j,6)*xscale(0.3)*(graph[0] -- graph[i]), (j==1 ? gray : black)+dotted);
    for (int i=1; i<=2; ++i)
        for (int j=0; j<=2; ++j)
            draw(shift(3.7+0.3*j,6)*xscale(0.3)*(graph[i] -- graph[i+1]), (j==1 ? gray : black)+dotted);
    for (pair p : graph) {
        draw(shift(3.7,6)*xscale(0.3)*p, black+2);
        draw(shift(4.3,6)*xscale(0.3)*p, black+2);
        draw(shift(3.7,6)*xscale(0.3)*p -- shift(4.3,6)*xscale(0.3)*p);
    }
    // SD TD
    for (pair p : graph) {
        draw(shift(5.7,6)*xscale(0.5)*p, black+2);
        draw(shift(6.3,6)*xscale(0.5)*p, black+2);
    }
    void connect(real x1, pair a, real x2=x1, pair b) {
        a = shift(x1,6)*xscale(0.5)*a;
        b = shift(x2,6)*xscale(0.5)*b;
        pair d = 0.04*unit(b-a);
        a += d;
        b -= d;
        if (a.x < b.x)
            draw(a -- b, EndArrow(2));
        else
            draw(b -- a, EndArrow(2));
    }
    for (int i=2; i<=5; ++i)
        for (int j=0; j<=1; ++j)
            connect(5.7+0.6*j, graph[0], graph[i]);
    for (int i=1; i<=2; ++i)
        for (int j=0; j<=1; ++j) if (j<1 || i<2)
            connect(5.7+0.6*j, graph[i], graph[i+1]);
    connect(5.7, graph[3], 6.3, graph[4]);
    connect(5.7, graph[3], 6.3, graph[5]);
    connect(5.7, graph[2], 6.3, graph[1]);
    connect(6.3, graph[4], 6.3, graph[5]);
}

main(4);
shipout("models");

label(scale(0.7)*"point", (1.2,1.5));
label(scale(0.7)*"interval", (4.0,1.2));
label(scale(0.7)*"sequence", (6.95,1.4));
label(scale(0.7)*"area", (1.15,4.4));
label(scale(0.7)*"area $\times$", (4.05,4.85));
label(scale(0.7)*"interval", (4.65,4.7));
label(scale(0.7)*"area $\times$", (6.9,4.5));
label(scale(0.7)*"sequence", (7.2,4.2));
label(scale(0.7)*"graph", (1.8,6.8));
label(scale(0.7)*"graph $\times$", (3.9,7.0));
label(scale(0.7)*"interval", (4.1,6.8));
label(scale(0.7)*"event", (6.2,6.8));
label(scale(0.7)*"structure", (7.0,6.6));
shipout("models_labelled");

picture pic;
unitsize(pic, 1cm);

filldraw(pic, rect((.7,.7), (5.16,5)), palered, deepred+2);
filldraw(pic, rect((5,5.15), (7.7,7)), palered, deepred+2);
filldraw(pic, rect((1,5.15), (3,7)), palered, deepred+2);
label(scale(0.8)*"\textbf{2-general model}", (6.35,7.25), deepred);
label(scale(0.8)*"\textbf{6-limit models}", (2.93,.5), deepred);
label(scale(0.8)*"\textbf{7-spatial algorithms}", (2,7.25), deepred);
add(pic, currentpicture);
shipout("models_lectured", pic);
