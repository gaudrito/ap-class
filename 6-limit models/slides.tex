\documentclass[10pt]{beamer} %,aspectratio=169,handout
	\usetheme{CambridgeUS} %CambridgeUS, Goettingen, Singapore, Madrid
	\setbeamertemplate{footline}[frame number]
	\setbeamercovered{transparent}
	\beamertemplatenavigationsymbolsempty

\usepackage{multimedia}
\usepackage{tabularx}
\usepackage{listings}
\usepackage{forloop}
\usepackage{mathtools}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{wasysym}
\newcolumntype{M}[1]{>{\centering\arraybackslash}m{#1}}
\setlength\tabcolsep{4pt}

\input{../macros}

\newcounter{prog}
\newcommand{\progress}[2]{$\forloop{prog}{0}{\value{prog} < #1}{\bullet}\forloop{prog}{0}{\value{prog} < #2}{\circ}$}
\newcounter{secslide}
\newcounter{sectotal}
\newcounter{secother}
\newcommand{\resetsec}[1]{\setcounter{secslide}{0}\setcounter{sectotal}{#1}}
\newcommand{\slidecount}{\addtocounter{secslide}{1}\setcounter{secother}{\value{sectotal} - \value{secslide}}\subsection[\progress{\value{secslide}}{\value{secother}}]{slide}}

\lstdefinelanguage{hfc}{
	basicstyle=\lst@ifdisplaystyle\small\fi\ttfamily,
	frame=single,
	basewidth=0.5em,
	sensitive=true,
	morestring=[b]",
	morecomment=[l]{//},
	morecomment=[n]{/*}{*/},
	commentstyle=\color{OliveGreen},
	keywordstyle=\color{blue}\textbf, keywords={def}, otherkeywords={=>},
	keywordstyle=[2]\color{red}\textbf, keywords=[2]{rep,nbr,share,if,let,in},
	keywordstyle=[3]\color{violet}, keywords=[3]{mux,sumHood,countHood,everyHood,anyHood,minHood,maxHood,foldHood,maxHoodPlusSelf,nbrDist},
	keywordstyle=[4]\color{orange}\textbf, keywords=[4]{spawn},
	keywordstyle=[5]\color{blue}, keywords=[5]{false,true,infinity}
}

\definecolor{darkgreen}{rgb}{0.0, 0.5, 0.0}
\definecolor{darkred}{rgb}{0.5, 0.0, 0.0}
\definecolor{darkblue}{rgb}{0.2, 0.2, 0.7}

\newcommand{\highlight}[1]{\textcolor{darkblue}{#1}}
\newcommand{\superlight}[1]{\textcolor{darkred}{#1}}
\newcommand{\greenlight}[1]{\textcolor{darkgreen}{#1}}
\newcommand{\DOI}[1]{\href{http://www.doi.org/#1}{\underline{#1}}}
\newcommand{\ACMDOI}[1]{\href{https://dl.acm.org/doi/#1}{\underline{#1}}}
\newcommand{\CITE}[5]{\ssymbol{#1}[#2. \emph{#3.} #4. \DOI{#5}]}

\theoremstyle{plain}
	\newtheorem{proposition}[theorem]{Proposition}
\theoremstyle{remark}
	\newtheorem{question}[theorem]{Question}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{Aggregate Programming for the Internet of Things}
\author{Giorgio Audrito, Ferruccio Damiani}
\institute{{\small Dipartimento di Informatica\\[3pt] Universit\`a degli Studi di Torino}}
%\date{}
\titlegraphic{
	%\raisebox{-0.5\height}{\includegraphics[width=2.0cm]{../images/logo_horizon.jpg}} \quad
	\raisebox{-0.5\height}{\includegraphics[width=2.0cm]{../images/logo_torino.pdf}} \quad
	%\raisebox{-0.5\height}{\includegraphics[width=2.0cm]{../images/logo_sanpaolo.png}}
}

\begin{document}
\lstset{language={hfc}}

\section[~]{Title}
	\begin{frame}
		\titlepage
	\end{frame}

	\begin{frame}{Conceptual map}
		\vspace{-3mm}
		\begin{block}{}
		\begin{center}
			\includegraphics[height=0.86\textheight]{../images/map2.pdf}
		\end{center}
		\end{block}
	\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\resetsec{7}
\section{An informal overview}

	\slidecount{}
	\begin{frame}{Outline}
		\begin{itemize}
			\item \highlight{An informal overview}
			\begin{itemize}
				\item General models of distributed computation
				\item Some simple examples
				\item A hierarchy of models
			\end{itemize}
			\bigskip
			\item Space-time continuity
			\bigskip
			\item Conclusions
		\end{itemize}
	\end{frame}

	\slidecount{}
	\begin{frame}[t]{General models of distributed computation}
		\begin{block}{}
			\begin{itemize}
				\item event structures: $\eventS$ \highlight{events}, $\neigh$ \highlight{message} DAG and $<$ \highlight{causality} partial order
				\onslide<2->{\item \highlight{space-time values} $\dvalue : \eventS \to X$ are labelling (functions) of event structures}
				\onslide<3->{\item composable \highlight{space-time functions} $f(\dvalue_\text{in}) = \dvalue_\text{out}$ are functions on s-t values}
			\end{itemize}
		\end{block}

		\begin{center}
			\raisebox{-0.5\height}{\includegraphics[height=28mm]{img/estruct_input.pdf}}
			\raisebox{-0.5\height}{$\stackrel{f}{\longrightarrow}$}
			\raisebox{-0.5\height}{\includegraphics[height=28mm]{img/estruct_gossip.pdf}}
		\end{center} \pause\pause\pause

		\begin{block}{}
			\begin{itemize}
				\item \highlight{programs} built by composing functions with global interpretation\ldots \pause
				\item \ldots which is still somewhat \highlight{hard to grasp} \pause
				\item we want something more \highlight{intuitive}!
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{(Less) general models of distributed computation}
		\begin{block}{}
			\begin{itemize}
				\item some functions do \highlight{require} all information in event structures to be expressed
				\begin{itemize}
					\item \highlight{granular space} information, \highlight{granular time} information
				\end{itemize}
				\onslide<2->{\item may others be described in a \highlight{simpler} way?
				\begin{itemize}
					\item maybe space or time is \highlight{not that relevant} at all?
					\item maybe the information could \highlight{not be so granular}?
				\end{itemize}}
			\end{itemize}
		\end{block}

		\begin{center}
			\raisebox{-0.5\height}{\includegraphics<1-2| handout:0>[height=28mm]{img/estruct_input.pdf}\includegraphics<3->[height=28mm]{img/otherdomain_input.pdf}}
			\raisebox{-0.5\height}{\only<1-2| handout:0>{$\stackrel{f}{\longrightarrow}$}\only<3->{$\stackrel{f^?}{\longrightarrow}$}}
			\raisebox{-0.5\height}{\includegraphics<1-2| handout:0>[height=28mm]{img/estruct_gossip.pdf}\includegraphics<3->[height=28mm]{img/otherdomain_output.pdf}}
		\end{center} \pause\pause
		\vspace{-10pt}

		\begin{block}{the idea}
			\begin{itemize}
				\item replace \highlight{event structures} $\ap{\eventS, \neigh, <}$ with something else $\mathcal{M}^?$ \pause
				\item \highlight{values} $v^? : \mathcal{M}^? \to X$ will be functions from that $\mathcal{M}^?$ to usual local values $X$ \pause
				\item \highlight{functions} $f^? : v^? \to v^?$ will still be functions from values to values
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{Some simple examples}
		\begin{block}{$\defK \texttt{increase}(a) \{ a + 1 \}$}
			\begin{itemize}
				\item both space and time are not relevant \pause
				\item $\mathcal{M} = \bp{p}$ can be a single \highlight{point} \pause
				\item $v : \bp{p} \to X$ is just a value in $X$ \pause
				\item $f$ usual function from $X$ to $X$, with $f(a) = a+1$
			\end{itemize}
		\end{block} \pause
		\bigskip

		\begin{block}{$\defK \; \texttt{adder}(a) \; \{ \repK(0) \{(x) \toSymK{x + a} \} \}$}
			\begin{itemize}
				\item space is not relevant (time is) \pause
				\item can we define $f : X \to X$ as before? \pause \superlight{NO! undefined!} \pause
				\item $\mathcal{M}$ can be an ordered \highlight{sequence} of points \pause
				\item $v$ is a sequence of values in $X$, $f$ transforms sequences \pause
				\item can we say $f(\ap{a_0, a_1, \ldots, a_n}) = \sum_{0}^{n} a_i$? \pause \superlight{NO! not composable!} \pause
				\item $f(\ap{a_0, a_1, \ldots, a_n}) = \ap{a_0, a_0+a_1, \ldots, \sum_{0}^{n} a_i}$ \greenlight{YES!}
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{Some quite simple examples}
		\begin{block}{$\defK \; \texttt{timeCounter}() \; \{ \repK(0) \{(\xname) \toSymK{} \xname + \texttt{deltaTime}() \} \}$}
			\begin{itemize}
				\item space is not relevant, time is\ldots\pause but not granularly: \highlight{continuously!} \pause
				\item $\mathcal{M}$ could be a \highlight{real interval} $[t_\text{min}, t_\text{max}] \subseteq \mathbb{R}$ \pause
				\item $v : [t_\text{min}, t_\text{max}] \to \mathbb{R}$ a real function on that interval \pause
				\item $f() = v$ such that $v(t) = t-t_\text{min}$ \pause
				\item \highlight{we could also interpret it on sequences} (or event structures, by the way)
			\end{itemize}
		\end{block} \pause
		\bigskip

		\begin{block}{$\defK \; \texttt{nbrCount}() \; \{ \texttt{sumHood}( \nbrK\{1\} ) \}$}
			\begin{itemize}
				\item time is not relevant (space is) \pause
				\item $\mathcal{M}$ can be a \highlight{graph} $\ap{V, E}$ \pause
				\item $v$ is a graph labelling \pause
				\item $f$ transforms labels (\texttt{nbrCount} labels by \highlight{degree})
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{Some not quite simple examples}
		\begin{block}{$\defK \; \texttt{distanceTo}(s) \; \{ \shareK (d) \{ (d) \toSymK \texttt{mux}(s, 0, \texttt{minHood}(\texttt{nbrDist}() + d)) \}$}
			\begin{itemize}
				\item time seems relevant\ldots \pause except not really: \highlight{wait enough} and see! \pause
				\item so we could use a \highlight{graph} for $\mathcal{M}$ as before\ldots \pause
				\item but space is relevant in a \highlight{continuous} way \pause
				\item $\mathcal{M} \subseteq \mathbb{R}^3$ could be a geometrical \highlight{area} in space, $v : \mathcal{M} \to X$, $f(v_i) = v_o$ \pause
				\item $v_i(p) = p \in S$ for some source subset $S \subseteq \mathcal{M}$ \pause
				\item $v_o(p) = \dist(p, S)$ is the geometrical distance of $p$ to $S$
			\end{itemize}
		\end{block} \pause
		\bigskip

		\begin{block}{$\defK \; \texttt{gossipMin}(v) \; \{ \shareK (v) \{ (x) \toSymK \texttt{min}(\texttt{minHood}(x), v) \}$}
			\begin{itemize}
				\item can we \highlight{forget time} here as well? \pause not so much\ldots \pause
				\item but we could consider time and space as \highlight{continuous} \pause
				\item $\mathcal{M} \subseteq \mathbb{R}^4$ could be a geometrical object in \highlight{space and time} \pause
				\item ok, this is getting out of hand, let's recap
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{A hierarchy of models}
		\begin{center}
			\includegraphics<1| handout:0>[width=0.73\linewidth]{img/models.pdf}
			\includegraphics<2->[width=0.73\linewidth]{img/models_labelled.pdf}
			\newline
			\begin{tikzpicture}[overlay]
				\fill<3->[orange,rounded corners] (-0.16\textwidth,3.53cm) rectangle (0.16\textwidth,4.13cm);
				\node<3-> at (0,3.83cm) {\textcolor{white}{there may be more}};
			\end{tikzpicture}
		\end{center}
	\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\resetsec{7}
\section{Space-time continuity}

	\slidecount{}
	\begin{frame}{Outline}
		\begin{itemize}
			\item An informal overview
			\bigskip
			\item \highlight{Space-time continuity}
			\begin{itemize}
				\item What is space-time continuity?
				\item Metric on space-time values
				\item Consistency
			\end{itemize}
			\bigskip
			\item Conclusions
		\end{itemize}
	\end{frame}

	\slidecount{}
	\begin{frame}{What is space-time continuity?}
		\begin{block}{$\defK \; \texttt{timeCounter}() \; \{ \repK(0) \{(\xname) \toSymK{} \xname + \texttt{deltaTime}() \} \}$}
			\begin{itemize}
				\item also space independent, $\mathcal{M}$ can be a \highlight{real interval} $[t_\text{min}, t_\text{max}] \subseteq \mathbb{R}$ \pause
				\item or $\mathcal{M}$ can be a \highlight{space-time region} in $\mathbb{R}^4$
			\end{itemize}
		\end{block} \pause
		\medskip

		\begin{block}{$\defK \; \texttt{distanceTo}(s) \; \{ \shareK (d) \{ (d) \toSymK \texttt{mux}(s, 0, \texttt{minHood}(\texttt{nbrDist}() + d)) \}$}
			\begin{itemize}
				\item also time independent, $\mathcal{M}$ can be a spatial \highlight{area} $\subseteq \mathbb{R}^3$ \pause
				\item or $\mathcal{M}$ can be a \highlight{space-time region} in $\mathbb{R}^4$
			\end{itemize}
		\end{block} \pause
		\medskip

		\begin{block}{$\defK \; \texttt{gossipMin}(v) \; \{ \shareK (v) \{ (x) \toSymK \texttt{min}(\texttt{minHood}(x), v) \}$}
			\begin{itemize}
				\item $\mathcal{M}$ has to be a \highlight{space-time region} in $\mathbb{R}^4$ \pause
				\item \emph{how can we characterise these functions?}
				\item \emph{how we relate the continuous behaviour with the concrete one?}
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{What is space-time continuity?}
		\begin{block}{}
			\begin{itemize}
				\only<1-8| handout:1>{
				\item we need to give a definition of \highlight{continuous limit} for a discrete process \pause\pause\pause\pause\pause
				\item let's get some inspiration from \highlight{analysis}!
				}
				\only<9-| handout:2>{
				\item we can do \highlight{just the same} for space-time functions
				\onslide<10->{\item with a notion of $\dist(v_e, v_c) < \delta$ for $v_e$ on event structure, $v_c$ continuous}
				}
			\end{itemize}
		\end{block} \pause

		\begin{center}
			\only<1-6| handout:1>{\vspace{22pt}}
			\only<7-| handout:2>{\vspace{-10pt}}
			\includegraphics<1| handout:0>[height=3cm]{img/approximation1.pdf}
			\includegraphics<2| handout:0>[height=3cm]{img/approximation2.pdf}
			\includegraphics<3| handout:0>[height=3cm]{img/approximation3.pdf}
			\includegraphics<4| handout:0>[height=3cm]{img/approximation4.pdf}
			\includegraphics<5| handout:0>[height=3cm]{img/approximation5.pdf}
			\includegraphics<6| handout:1>[height=3cm]{img/approximation6.pdf}
			\includegraphics<7-| handout:2>[height=5.1cm]{img/limit.pdf}
			\only<1-6| handout:1>{\vspace{10pt}}
			\only<7-| handout:2>{\vspace{-18pt}}
		\end{center}

		\begin{block}{limit of a partial function $f$ on a metric space $\mathcal{M}$}
			$\lim_{x \rightarrow c} f(x) = L$ iff for every $\epsilon > 0$ there is a $\delta > 0$ such that: \pause
			\begin{itemize}
				\item for every $x \in \domof{f}$ with $\dist(x, c) < \delta$, we have $\dist(f(x), L) < \epsilon$
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{Metric on space-time values}
		\begin{block}{}
			\begin{itemize}
				\item $v_e : \eventS \to X$ space-time value on \highlight{event structure} $\ap{\eventS, \neigh, <}$ \pause
				\item space-time \highlight{positions} $\vec{p}_\eventId \in \mathcal{M}$ for all events $\eventId$ in $\eventS$, with $p^4_\eventId = t_\eventId < t_{\eventId'}$ if $\eventId < \eventId'$ \pause
				\item $v_c : \mathcal{M} \to X$ space-time value on \highlight{space-time region} $\mathcal{M} \subseteq \mathbb{R}^4$ \pause
				\item $\dist(v_e, v_c) < \ap{\delta, R, N}$ iff: \pause
				\begin{itemize} \normalsize
					\item $\forall \eventId \in \eventS$, $\vp{v_e(\eventId) - v_c(\vec{p}_\eventId)}_X < \delta$ (values are \highlight{close}) \pause
					\item $\forall \eventId_1 \neigh \eventId_2$, $\vp{\vec{p}_{\eventId_1} - \vec{p}_{\eventId_2}}_\mathcal{M} < R$ (connection is \highlight{granular}) \pause
					\item \emph{events have $N$-\highlight{large} neighbourhoods where communication is $N$-\highlight{fast}} \pause
					\item \emph{\ldots and these neighbourhoods \highlight{cover} the whole $\mathcal{M}$}
				\end{itemize}
			\end{itemize}
		\end{block} \pause

		\begin{block}{$N$-large and $N$-fast covers $\mathcal{M}$}
			for every $\eventId$ there is a $\rho_\eventId > 0$ such that: \pause
			\begin{itemize}
				\item $\mathcal{M} \subseteq \bigcup_\eventId \mathcal{B}^4_{N \rho_\eventId}(\vec{p}_\eventId)$ (events \highlight{cover} $\mathcal{M}$ with their $N\rho_\eventId$ surroundings)\footnote{$\mathcal{B}^n_r(c)$ is the $r$-radius sphere in $\mathbb{R}^n$ centred in $c$.} \pause
				\item $\forall \eventId \in \eventS$, $\mathcal{B}^4_{N \rho_\eventId}(\vec{p}_\eventId) \cap \mathcal{M} \subseteq \bigcup_{\substack{\eventId \neigh \eventId' \\ \eventId' \neigh \eventId}} \mathcal{B}^4_{\rho_\eventId}(\vec{p}_{\eventId'})$ \\ ($\rho_\eventId$ surroundings of neighbours \highlight{cover} the $N \rho_\eventId$ surrounding)
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{Metric on space-time values}
		\begin{block}{why is this ``$N$-large, $N$-fast''?}
			\emph{$\rho_\eventId$ surroundings of neighbours \highlight{cover} the $N \rho_\eventId$ surrounding} \pause
			\begin{itemize}
				\item need $\approx N^4$ $\rho_\eventId$-balls to cover a $N \rho_\eventId$-ball \highlight{$\longrightarrow$ $O(N^4)$ many neighbours} \pause
				\item there are neighbours in every spatial direction $\vec{u}$ and close in time: \pause
				\begin{itemize} \normalsize
					\item $\vec{p}_\eventId + (\sqrt{N^2-1} \rho_\eventId \vec{u}, \rho_\eventId)$ is within $N \rho_\eventId$-ball \pause
					\item some $\vec{p}_{\eventId'}$ is within $\rho_\eventId$ of it, thus is after $\eventId$ \pause
					\item worst-case is $\vec{p}_{\eventId'} = (\frac{\sqrt{N^2-1}(N^2-2)\rho_\eventId}{N^2} \vec{u}, \frac{2(N^2-1)\rho_\eventId}{N^2})$ \pause
					\item transmission speed $\eventId \neigh \eventId'$ is at least $\frac{N^2-2}{2 \sqrt{N^2-1}}$ \highlight{$\longrightarrow$ $O(N)$ speed}
				\end{itemize}
			\end{itemize}
		\end{block} \pause
		\bigskip

		\begin{block}{why we need it?}
			\begin{itemize}
				\item without $N$ we may have \highlight{very dense} $\eventS$ where data flows \highlight{ridiculously slow} \pause
				\item no hope to get \highlight{similar results} for different data flow speeds
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{Consistency}
		\begin{block}{discrete process limit}
			\begin{itemize}
				\item given $f$ space-time \highlight{function} on event structures \pause
				\item $\mathcal{M}$ space-time \highlight{region} and $v_i, v_o : \mathcal{M} \to X$ \pause
				\item $\lim_{x \rightarrow v_i} f(x) = v_o$ iff for every $\ap{\delta, R, N}$ there is $\ap{\delta', R', N'}$ such that: \pause
				\begin{itemize}
					\item for every $x$ space-time value with $\dist(x, v_i) < \ap{\delta', R', N'}$, \pause
					\item we have $\dist(f(x), v_o) < \ap{\delta, R, N}$
				\end{itemize}
			\end{itemize}
		\end{block} \pause
		\bigskip

		\begin{block}{space-time consistency}
			\begin{itemize}
				\item a space-time function $f$ on discrete event structures $\eventS$ \pause
				\item is \highlight{consistent} with a continuous function $g$ on space-time regions $\mathcal{M}$ \pause
				\item iff for every $v : \mathcal{M} \to X$, $\lim_{x \rightarrow v} f(x) = g(v)$
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{Consistency}
		\begin{block}{important!}
			this allows to describe programs with common mathematical notation
		\end{block} \pause
		\medskip

		\begin{block}{$\defK \; \texttt{timeCounter}() \; \{ \repK(0) \{(\xname) \toSymK{} \xname + \texttt{deltaTime}() \} \}$}
			$\texttt{timeCounter}()(\vec{p},t) = t$
		\end{block} \pause
		\medskip

		\begin{block}{$\defK \; \texttt{distanceTo}(s) \; \{ \shareK (d) \{ (d) \toSymK \texttt{mux}(s, 0, \texttt{minHood}(\texttt{nbrDist}() + d)) \}$}
			$\texttt{distanceTo}(s)(\vec{p},t) = \dist(\vec{p}, \bp{x \in \mathcal{M} : ~ s(x) = \texttt{true}})$
		\end{block} \pause
		\medskip

		\begin{block}{$\defK \; \texttt{gossipMin}(v) \; \{ \shareK (v) \{ (x) \toSymK \texttt{min}(\texttt{minHood}(x), v) \}$}
			$\texttt{gossipMin}(v)(\vec{p},t) = \min \bp{v(\vec{q},s) : s < t \text{ and } \ap{\vec{q},s} \in \mathcal{M}}$
		\end{block}
	\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\resetsec{2}
\section{Conclusions}

	\slidecount{}
	\begin{frame}{Conclusions}
		\only<0| handout:1>{\vspace{-6pt}}
		\begin{block}{}
			\begin{itemize}
				\item several \highlight{models} allow to describe distributed programs in a convenient way \pause
				\item every program is describable only in \highlight{some} of these models \pause
				\item continuous models allow to use the language of \highlight{mathematics}
			\end{itemize}
		\end{block}
		\only<1-| handout:0>{\bigskip}

		\begin{center}
			\includegraphics<1-3>[height=3cm]{img/approximation.png} \only<0| handout:1>{\\}
			\includegraphics<4->[height=3cm]{img/blockflow.pdf}
		\end{center}
	\end{frame}

	\slidecount{}
	\begin{frame}{Conclusions}
		\begin{center}
			\includegraphics<1| handout:0>[width=0.73\linewidth]{img/models_labelled.pdf}
			\includegraphics<2->[width=0.73\linewidth]{img/models_lectured.pdf}
		\end{center}
	\end{frame}

\end{document}
