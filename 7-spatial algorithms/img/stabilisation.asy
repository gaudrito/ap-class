pen shader(real val) {
    return hsv(360*val, 1, 1);
}

void drawnode(picture pic, pair pos, pen col) {
    pen fg = 0.5*col;
    pen bk = col;
    fill(pic, circle(pos, .19), black);
    fill(pic, circle(pos, .16), bk);
}

int  F = 6;         // frames
pair DIM = (12,3);   // area
srand(2);           // random seed

pair[] P;
real[] A, B;
int N = 0;
for (int i=0; i<DIM.x; ++i) {
    for (int j=0; j<DIM.y; ++j) {
        P[N] = (i+0.1+0.8*unitrand(), j+0.1+0.8*unitrand());
        A[N] = unitrand();
        B[N] = length(P[N] - 0.5*DIM) * 1.8 / length(DIM);
        ++N;
    }
}

for (int j=1; j<=F; ++j) {
    picture pic;
    unitsize(pic, 1cm);
    for (int i=0; i<N; ++i) {
        A[i] = 0.7*A[i] + 0.3*B[i];
        drawnode(pic, P[i], shader(A[i]));
    }
    shipout("stabilisation" + string(j), pic);
}
