\documentclass[10pt]{beamer} %,aspectratio=169,handout
	\usetheme{CambridgeUS} %CambridgeUS, Goettingen, Singapore, Madrid
	\setbeamertemplate{footline}[frame number]
	\setbeamercovered{transparent}
	\beamertemplatenavigationsymbolsempty

\usepackage{multimedia}
\usepackage{tabularx}
\usepackage{listings}
\usepackage{forloop}
\usepackage{mathtools}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{wasysym}
\newcolumntype{M}[1]{>{\centering\arraybackslash}m{#1}}
\setlength\tabcolsep{4pt}

\input{../macros}

\newcounter{prog}
\newcommand{\progress}[2]{$\forloop{prog}{0}{\value{prog} < #1}{\bullet}\forloop{prog}{0}{\value{prog} < #2}{\circ}$}
\newcounter{secslide}
\newcounter{sectotal}
\newcounter{secother}
\newcommand{\resetsec}[1]{\setcounter{secslide}{0}\setcounter{sectotal}{#1}}
\newcommand{\slidecount}{\addtocounter{secslide}{1}\setcounter{secother}{\value{sectotal} - \value{secslide}}\subsection[\progress{\value{secslide}}{\value{secother}}]{slide}}

\lstdefinelanguage{hfc}{
	basicstyle=\lst@ifdisplaystyle\small\fi\ttfamily,
	frame=single,
	basewidth=0.5em,
	sensitive=true,
	morestring=[b]",
	morecomment=[l]{//},
	morecomment=[n]{/*}{*/},
	commentstyle=\color{gray},
	keywordstyle=\color{blue}\textbf, keywords={def}, otherkeywords={=>},
	keywordstyle=[2]\color{red}\textbf, keywords=[2]{rep,nbr,share,if,let,in},
	keywordstyle=[3]\color{violet}, keywords=[3]{mux,sumHood,countHood,everyHood,anyHood,minHood,maxHood,foldHood,maxHoodPlusSelf,nbrDist,snd},
	keywordstyle=[4]\color{orange}\textbf, keywords=[4]{spawn},
	keywordstyle=[5]\color{blue}, keywords=[5]{false,true,infinity}
}

\definecolor{darkgreen}{rgb}{0.0, 0.5, 0.0}
\definecolor{darkred}{rgb}{0.5, 0.0, 0.0}
\definecolor{darkblue}{rgb}{0.2, 0.2, 0.7}

\newcommand{\highlight}[1]{\textcolor{darkblue}{#1}}
\newcommand{\superlight}[1]{\textcolor{darkred}{#1}}
\newcommand{\greenlight}[1]{\textcolor{darkgreen}{#1}}
\newcommand{\DOI}[1]{\href{http://www.doi.org/#1}{\underline{#1}}}
\newcommand{\ACMDOI}[1]{\href{https://dl.acm.org/doi/#1}{\underline{#1}}}
\newcommand{\CITE}[5]{\ssymbol{#1}[#2. \emph{#3.} #4. \DOI{#5}]}

\theoremstyle{plain}
\newtheorem{proposition}[theorem]{Proposition}
\theoremstyle{remark}
\newtheorem{question}[theorem]{Question}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{Aggregate Programming for the Internet of Things}
\author{Giorgio Audrito, Ferruccio Damiani}
\institute{{\small Dipartimento di Informatica\\[3pt] Universit\`a degli Studi di Torino}}
%\date{}
\titlegraphic{
	%\raisebox{-0.5\height}{\includegraphics[width=2.0cm]{../images/logo_horizon.jpg}} \quad
	\raisebox{-0.5\height}{\includegraphics[width=2.0cm]{../images/logo_torino.pdf}} \quad
	%\raisebox{-0.5\height}{\includegraphics[width=2.0cm]{../images/logo_sanpaolo.png}}
}

\begin{document}
\lstset{language={hfc}}

\section[~]{Title}
	\begin{frame}
		\titlepage
	\end{frame}

	\begin{frame}{Conceptual map}
		\vspace{-3mm}
		\begin{block}{}
		\begin{center}
			\includegraphics[height=0.86\textheight]{../images/map8.pdf}
		\end{center}
		\end{block}
	\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\resetsec{6}
\section{Time independence}

	\slidecount{}
	\begin{frame}{Outline}
		\begin{itemize}
			\item \highlight{Time independence}
			\begin{itemize}
				\item What is time-independence?
				\item Stabilisation
				\item Self-stabilisation
			\end{itemize}
			\bigskip
			\item Spatial algorithms
			\bigskip
			\item Conclusions
		\end{itemize}
	\end{frame}

	\slidecount{}
	\begin{frame}{A hierarchy of models}
		\begin{center}
			\includegraphics[width=0.73\linewidth]{img/models_labelled.pdf}
		\end{center}
	\end{frame}

	\slidecount{}
	\begin{frame}{What is time-independence?}
		\begin{block}{$\defK \; \texttt{nbrCount}() \; \{ \texttt{sumHood}( \nbrK\{1\} ) \}$}
			\begin{itemize}
				\item $M$ can be a \highlight{graph}, $v$ a graph \highlight{labelling}, $f$ transforms labels \pause
				\item \texttt{nbrCount} labels by \highlight{degree}\ldots \emph{in which graph?} \pause
				\item \highlight{how can we use this in the real world of events?}
			\end{itemize}
		\end{block} \pause
		\bigskip

		\begin{block}{$\defK \; \texttt{distanceTo}(s) \; \{ \shareK (\infty) \{ (d) \toSymK \texttt{mux}(s, 0, \texttt{minHood}(\texttt{nbrDist}() + d)) \}$}
			\begin{itemize}
				\item time somewhat independent, provided we \highlight{wait enough} \pause
				\item \emph{what does it mean exactly?} \pause
				\item \highlight{how can we use this in the real world of events?}
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{Stabilisation} \pause\pause\pause\pause\pause\pause
		\begin{block}{stabilising space-time value}
			\begin{itemize}
				\item \highlight{infinite} event structure $\ap{\eventS, \neigh, <}$ with device information $\deviceId_\eventId$ \pause
				\item every $\eventId$ on $\deviceId$ after a certain $\eventId_\deviceId$ holds the same \highlight{limit value} $v(\eventId)$ \pause
				\item \ldots and the same \highlight{limit topology} $\bp{\deviceId_{\eventId'} : ~ \eventId' \neigh \eventId}$
			\end{itemize}
		\end{block} \pause

		\begin{center}
			\includegraphics<1| handout:0>[width=0.73\linewidth]{img/stabilisation1.pdf}
			\includegraphics<2| handout:0>[width=0.73\linewidth]{img/stabilisation2.pdf}
			\includegraphics<3| handout:0>[width=0.73\linewidth]{img/stabilisation3.pdf}
			\includegraphics<4| handout:0>[width=0.73\linewidth]{img/stabilisation4.pdf}
			\includegraphics<5| handout:0>[width=0.73\linewidth]{img/stabilisation5.pdf}
			\includegraphics<6->[width=0.73\linewidth]{img/stabilisation6.pdf}
		\end{center}

		\begin{block}{limit of a stabilising space-time value $v_s : \eventS \to X$}
			\begin{itemize}
				\item $V = \bp{\deviceId, \ldots}$ set of devices in $\eventS$, connected by limit topology \pause
				\item $v_g : V \to X$ associates $\deviceId$ to the limit value of $v_s$ on $\deviceId$
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{Self-stabilisation}
		\begin{block}{stabilising function}
			$f$ is stabilising iff given stabilising $v_i$, also $f(v_i) = v_o$ is stabilising \pause

			\begin{itemize}
				\item so, is \highlight{stabilisation} giving us time independence? \pause \superlight{NO!} \pause
				\item \texttt{gossipMin} is stabilising but \highlight{time matters}
			\end{itemize}
		\end{block} \pause
		\bigskip

		\begin{block}{self-stabilisation}
			$f$ is \highlight{self-stabilising} iff given two stabilising inputs $v_1$, $v_2$ with the same limit, \\ \pause
			also $f(v_1)$, $f(v_2)$ are stabilising to the same limit \pause

			\begin{itemize}
				\item we can define $g$ function on limits: here is the \highlight{graph} model!
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{Self-stabilisation}
		\begin{block}{achtung!}
			\begin{itemize}
				\item self-stabilisation is \highlight{very important} \pause
				\item as the input changes, the result \highlight{adjusts} following the limit behaviour \pause
				\item we will also need a measure for \highlight{how closely} it follows that (we'll see)
			\end{itemize}
		\end{block} \pause
		\bigskip

		\begin{block}{spatial algorithms}
			\begin{itemize}
				\item compute properties of \highlight{space} and distributed data \pause
				\item doing so in a self-stabilising way \pause
				\item the two most important are \highlight{distance estimation} and \highlight{data collection}
			\end{itemize}
		\end{block}
	\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\resetsec{9}
\section{Spatial algorithms}

	\slidecount{}
	\begin{frame}{Outline}
		\begin{itemize}
			\item Time independence
			\bigskip
			\item \highlight{Spatial algorithms}
			\begin{itemize}
				\item Distance estimation
				\item Data collection
			\end{itemize}
			\bigskip
			\item Conclusions
		\end{itemize}
	\end{frame}

	\slidecount{}
	\begin{frame}{Distance estimation}
		\begin{block}{why is it important?}
			\begin{itemize}
				\item it computes \highlight{distances} from given sources, but also\ldots \pause
				\item builds a \highlight{spanning forest} of shortest-paths that can \highlight{guide other algorithms} \pause
				\item in its more general form it can also \highlight{spread} information, as in a \highlight{broadcast}
			\end{itemize}
		\end{block}
		\bigskip

		\begin{center}
			\includegraphics<1| handout:0>[width=0.7\linewidth]{img/gradient.pdf}
			\includegraphics<2->[width=0.7\linewidth]{img/gradient_tree.pdf}
		\end{center}
	\end{frame}

	\slidecount{}
	\begin{frame}{Distance estimation}
		\begin{block}{$\defK \; \texttt{distanceTo}(s) \; \{ \shareK (\infty) \{ (d) \toSymK \texttt{mux}(s, 0, \texttt{minHood}(\texttt{nbrDist}() + d)) \}$}
			\begin{itemize}
				\item \highlight{Adaptive} Bellman-Ford algorithm (we can start from \highlight{wrong} values) \pause
				\item over-estimates disappear quickly (\highlight{diameter} rounds) \pause
				\item under-estimates disappear slowly: \highlight{rising value} problem or \highlight{count-to-infinity} \pause
				\item \ldots they can only grow by the \highlight{smallest link} in the network per round \pause
				\item other algorithms solve this problem (BIS, ULT\ldots) \pause
				\item tree can be constructed through $\texttt{minHood}(\texttt{pair}(\texttt{nbrDist}() + d, \nbrK\{ID\}))$
			\end{itemize}
		\end{block}

		\begin{center}
			\includegraphics<1-5>[width=0.7\linewidth]{img/gradient.pdf}
			\includegraphics<6-| handout:0>[width=0.7\linewidth]{img/gradient_tree.pdf}
		\end{center}
	\end{frame}

	\slidecount{}
	\begin{frame}{Distance estimation}
		\begin{block}{G block for spreading}
			\begin{center}
				$ \defK \; G(\ell, f) \; \{ \shareK (\ell) \{ (x) \toSymK \only<3->{g(}\min(\texttt{minHood}(f(x)), \ell)\only<3->{)} \} $
			\end{center}
			\begin{itemize}
				\item $\ell$ local value considered in minimisation together with $f(x)$ \pause
				\item $f$ \highlight{pointwise}, \highlight{monotonic} ($x < y \Rightarrow f(x) < f(y)$), \highlight{progressive} ($x < f(x)$) \pause
				\item \ldots there may also be a \highlight{rising} $g$ handling the raising value problem
			\end{itemize}
		\end{block} \pause

		\begin{block}{distance}
			\begin{itemize}
				\item $\ell = 0$ for sources, $\infty$ for others
				\item $f(x) = x + \texttt{nbrDist}()$ is pointwise, monotonic and progressive
			\end{itemize}
		\end{block} \pause

		\begin{block}{broadcast}
			\begin{itemize}
				\item $\ell = [0,v]$ for sources broadcasting $v$, or $[\infty,\texttt{null}]$ for others
				\item $f([i,v]) = [i+1,v]$ is pointwise, monotonic and progressive
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{Data collection}
		\begin{block}{why is it important?}
			\begin{itemize}
				\item provides a \highlight{global} information about the network: think of \highlight{MapReduce}! \pause
				\item this information can then be shared by a \highlight{broadcast}
			\end{itemize}
		\end{block} \pause
		\vspace{-5pt}

		\begin{center}
			\includegraphics[width=0.55\linewidth]{img/algorithm-collection.png}
		\end{center}

		\vspace{-11pt}
		\begin{block}{what do we need to do it?}
			\begin{itemize}
				\item a \highlight{spanning tree} or similar structure to guide the collection \pause
				\item \ldots the output of the \highlight{G block!}
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{Data collection}
		\begin{block}{}
			\begin{itemize}
				\item gossip is not good! \superlight{$\longrightarrow$ does not self-stabilise} \pause
				\item need to avoid cycles: data flows only \highlight{descending distance} \pause
				\item every device computes a \highlight{partial estimate} for devices further away \pause
				\item several \highlight{strategies}: single- or multi-path depending where you send your data \pause
				\item need to express the algorithm in the point of view of \highlight{what you receive}
			\end{itemize}
		\end{block}
		\begin{center}
			\includegraphics[width=0.6\linewidth]{img/algorithm-collection.png}
		\end{center}
	\end{frame}

\slidecount{}
\begin{frame}[fragile]{Data collection}
	\begin{block}{single-path collection}
\begin{lstlisting}[]
def single-path(dist, v, null, f) {
  share (v) { (x) =>
    let parent = snd(minHood(nbr{[dist,ID]})) in // neigh with min dist
    f(foldHood(mux(nbr{parent} == ID, x, null), f), v)
} }                               // acc neigh whose parent is me
\end{lstlisting}
	\vspace{-10pt}
	\begin{itemize}
		\item \lstinline|dist| given by \highlight{$G$ block} \pause
		\item \lstinline|v| values to be \highlight{collected} \pause
		\item \lstinline|acc| collecting \highlight{function}, with a \lstinline|null| value
	\end{itemize}
	\end{block} \pause
	\bigskip

	\begin{block}{works and self-stabilises, but\ldots}
		\begin{itemize}
			\item while $G$ adjusts, \highlight{parents} change often \pause
			\item \ldots disconnecting the tree and \highlight{losing} values
		\end{itemize}
	\end{block}
\end{frame}

\slidecount{}
\begin{frame}[fragile]{Data collection}
	\begin{block}{C block for collection}
\begin{lstlisting}[]
def C(d, v, n, f) {
  share (v) { (x) =>
    f(mux(nbr{d} > d, x, n), v)
} }
\end{lstlisting}
	\vspace{-10pt}
	\begin{itemize}
		\item most \highlight{general} form, still self-stabilising \pause
		\item filter \highlight{all} neighbours with a higher $d$ \pause
		\item feed the field to $f$, who can reduce it however it likes \pause
		\item allows \highlight{multi-path} strategies (value sent to many neighbours) \textbf{[collection]}
	\end{itemize}
	\end{block}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Conclusions}

	\slidecount{}
	\begin{frame}{Conclusions}
		\begin{block}{}
			\begin{itemize}
				\item \highlight{self-stabilisation} allows to model time-independent functions on \highlight{graphs} \pause
				\item implements the idea of automatic \highlight{adjustment} upon changes \pause
				\item a general \highlight{spreading} (G) block can calculate distance and broadcast \pause
				\item a general \highlight{collection} (C) block can provide a \highlight{global} view (reduce)
			\end{itemize}
		\end{block} \pause
		\bigskip

		\begin{block}{G-C-G combination}
			\begin{itemize}
				\item G creates a spanning tree for communication \pause
				\item C uses the spanning tree for gathering the information \pause
				\item G broadcasts the result to every device in the network
			\end{itemize}
		\end{block}
	\end{frame}

\end{document}
