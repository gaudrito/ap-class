\documentclass[10pt]{beamer} %,aspectratio=169,handout
	\usetheme{CambridgeUS} %CambridgeUS, Goettingen, Singapore, Madrid
	\setbeamertemplate{footline}[frame number]
	\setbeamercovered{transparent}
	\beamertemplatenavigationsymbolsempty

\usepackage{multimedia}
\usepackage{tabularx}
\usepackage{listings}
\usepackage{forloop}
\usepackage{mathtools}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{wasysym}
\newcolumntype{M}[1]{>{\centering\arraybackslash}m{#1}}
\setlength\tabcolsep{4pt}

\input{../macros}

\newcounter{prog}
\newcommand{\progress}[2]{$\forloop{prog}{0}{\value{prog} < #1}{\bullet}\forloop{prog}{0}{\value{prog} < #2}{\circ}$}
\newcounter{secslide}
\newcounter{sectotal}
\newcounter{secother}
\newcommand{\resetsec}[1]{\setcounter{secslide}{0}\setcounter{sectotal}{#1}}
\newcommand{\slidecount}{\addtocounter{secslide}{1}\setcounter{secother}{\value{sectotal} - \value{secslide}}\subsection[\progress{\value{secslide}}{\value{secother}}]{slide}}

\lstdefinelanguage{hfc}{
	basicstyle=\lst@ifdisplaystyle\small\fi\ttfamily,
	frame=single,
	basewidth=0.5em,
	sensitive=true,
	morestring=[b]",
	morecomment=[l]{//},
	morecomment=[n]{/*}{*/},
	commentstyle=\color{gray},
	keywordstyle=\color{blue}\textbf, keywords={def}, otherkeywords={=>},
	keywordstyle=[2]\color{red}\textbf, keywords=[2]{rep,nbr,share,if,let,in},
	keywordstyle=[3]\color{violet}, keywords=[3]{mux,sumHood,countHood,everyHood,anyHood,minHood,maxHood,foldHood,maxHoodPlusSelf,nbrDist,snd},
	keywordstyle=[4]\color{orange}\textbf, keywords=[4]{spawn},
	keywordstyle=[5]\color{blue}, keywords=[5]{false,true,infinity}
}

\definecolor{darkgreen}{rgb}{0.0, 0.5, 0.0}
\definecolor{darkred}{rgb}{0.5, 0.0, 0.0}
\definecolor{darkblue}{rgb}{0.2, 0.2, 0.7}

\newcommand{\highlight}[1]{\textcolor{darkblue}{#1}}
\newcommand{\superlight}[1]{\textcolor{darkred}{#1}}
\newcommand{\greenlight}[1]{\textcolor{darkgreen}{#1}}
\newcommand{\DOI}[1]{\href{http://www.doi.org/#1}{\underline{#1}}}
\newcommand{\ACMDOI}[1]{\href{https://dl.acm.org/doi/#1}{\underline{#1}}}
\newcommand{\CITE}[5]{\ssymbol{#1}[#2. \emph{#3.} #4. \DOI{#5}]}

\theoremstyle{plain}
\newtheorem{proposition}[theorem]{Proposition}
\theoremstyle{remark}
\newtheorem{question}[theorem]{Question}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{Aggregate Programming for the Internet of Things}
\author{Giorgio Audrito, Ferruccio Damiani}
\institute{{\small Dipartimento di Informatica\\[3pt] Universit\`a degli Studi di Torino}}
%\date{}
\titlegraphic{
	%\raisebox{-0.5\height}{\includegraphics[width=2.0cm]{../images/logo_horizon.jpg}} \quad
	\raisebox{-0.5\height}{\includegraphics[width=2.0cm]{../images/logo_torino.pdf}} \quad
	%\raisebox{-0.5\height}{\includegraphics[width=2.0cm]{../images/logo_sanpaolo.png}}
}

\begin{document}
\lstset{language={hfc}}

\section[~]{Title}
	\begin{frame}
		\titlepage
	\end{frame}

	\begin{frame}{Conceptual map}
		\vspace{-3mm}
		\begin{block}{}
		\begin{center}
			\includegraphics[height=0.86\textheight]{../images/map3.pdf}
		\end{center}
		\end{block}
	\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\resetsec{5}
\section{Abstract models vs real world}

	\slidecount{}
	\begin{frame}{Outline}
		\begin{itemize}
			\item \highlight{Abstract models vs real world}
			\begin{itemize}
				\item A hierarchy of models
				\item Inverting the abstraction
				\item What can we do?
			\end{itemize}
			\bigskip
			\item Bounding transient length
			\bigskip
			\item Controlling transient errors
		\end{itemize}
	\end{frame}

	\slidecount{}
	\begin{frame}[t]{A hierarchy of models}
		\begin{center}
			\begin{tikzpicture}[overlay]
				\node[opacity=1,inner sep=0pt] at (0,-3.1cm){\includegraphics[width=0.73\linewidth]{img/models_labelled.pdf}};
				\fill<2->[orange,rounded corners] (-0.24\textwidth,-2.53cm) rectangle (0.24\textwidth,-3.13cm);
				\node<2-> at (0,-2.83cm) {\textcolor{white}{how reliable these abstractions are?}};
			\end{tikzpicture}
		\end{center}
	\end{frame}

	\slidecount{}
	\begin{frame}[t]{Inverting the abstraction}
		\begin{center}
			\begin{tikzpicture}[remember picture,overlay]
				\node<1-7| handout:0>[opacity=0.5,inner sep=0pt] at (0,-3.1cm){\includegraphics[width=0.73\linewidth]{img/models_labelled.pdf}};
			\end{tikzpicture}
		\end{center}
		\vspace{-1.6cm}
		\begin{block}{abstractions}
			\begin{itemize}
				\item different ways of representing the behaviour of a same \highlight{function} $f$ \pause
				\item up/right are more \highlight{precise} but complex \pause
				\item down/left are more \highlight{abstract}, compact and simple \pause
			\end{itemize}
		\end{block}

		\only<4->{
		\begin{block}{the abstraction process}
			\begin{minipage}{0.6\linewidth}
			\begin{itemize}
				\item given a \highlight{more precise} representation
				\onslide<1-3,5->{\item if some \highlight{regularity} properties are satisfied,}
				\onslide<1-3,6->{\item we get another \highlight{more abstract} representation}
				\onslide<1-3,7->{\item we go \superlight{up} to \greenlight{down} and \superlight{right} to \greenlight{left}}
			\end{itemize}
			\end{minipage}
			\begin{minipage}{0.39\linewidth}
			\centering
			\begin{tabular}{ccc}
				&& \superlight{precise} \\ \pause
				& $\swarrow$ & \\ \pause
				\greenlight{abstract}  && \\ \pause
			\end{tabular}
			\medskip
			\end{minipage}
		\end{block} \pause
		}

		\only<8->{
		\begin{center}
			\textbf{can we invert the process?} \\
			\begin{tabular}{ccc}
				&& \superlight{precise} \\
				& $\nearrow$ & \\
				\greenlight{abstract}  &&
			\end{tabular}
		\end{center}
		}
	\end{frame}

	\slidecount{}
	\begin{frame}{Inverting the abstraction}
		\begin{block}{space independence \onslide<2->{\greenlight{$\longrightarrow$ OK!}}}
			\begin{itemize}
				\item is an \highlight{exact} property (no limits involved) \pause
				\item behaviour on \highlight{sequences} induces behaviour on \highlight{event structures} exactly \pause
			\end{itemize}
		\end{block}
		\medskip

		\begin{block}{time independence \onslide<1-2,5->{\superlight{$\longrightarrow$ NO!}}}
			\begin{itemize}
				\item is only a \highlight{limit} property! \pause
				\item cannot reconstruct what happens during \highlight{transitory} phase \pause
				\item nor \highlight{how long} the transitory phase is \pause
			\end{itemize}
		\end{block}
		\medskip

		\begin{block}{time/space continuity \superlight{$\longrightarrow$ NO!}}
			\begin{itemize}
				\item obviously a \highlight{limit} property as well, same problems
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{What can we do?}
		\begin{block}{what is the problem?}
			\begin{itemize}
				\item knowing a limit behaviour may say \highlight{little to nothing} on actual behaviour \pause
				\item except sometimes \highlight{it does}, and we need to tell these cases apart \pause
			\end{itemize}
		\end{block}
		\medskip

		\begin{block}{what do we need?}
			\begin{itemize}
				\item information on how close we can \highlight{bound} true behaviour to the limit \pause
				\item \ldots that is \highlight{composable!} \pause
			\end{itemize}
		\end{block}
		\medskip

		\begin{block}{what are the solutions?}
			\begin{itemize}
				\item \highlight{many} possible solutions to this problem \pause
				\item may bound \highlight{difference} of transient and limit values (\highlight{real-time} guarantees) \pause
				\item may bound \highlight{length} of transient phases (stability analysis)
			\end{itemize}
		\end{block}
	\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\resetsec{9}
\section{Bounding transient length}

	\slidecount{}
	\begin{frame}{Outline}
		\begin{itemize}
			\item Abstract models vs real world
			\bigskip
			\item \highlight{Bounding transient length}
			\begin{itemize}
				\item Time-independence (again)
				\item The synchronous case
				\item The asynchronous case
				\item How can we prove it?
			\end{itemize}
			\bigskip
			\item Controlling transient errors
		\end{itemize}
	\end{frame}

	\slidecount{}
	\begin{frame}{Time-independence (again)}
		\begin{block}{stabilising space-time value}
			\begin{itemize}
				\item \highlight{infinite} event structure $\ap{\eventS, \neigh, <}$ with device information $\deviceId_\eventId$ \pause
				\item every $\eventId$ on $\deviceId$ after a certain $\eventId_\deviceId$ holds the same \highlight{limit value} $v(\eventId)$ \pause
				\item \ldots and the same \highlight{limit topology} $\bp{\deviceId_{\eventId'} : ~ \eventId' \neigh \eventId}$ \pause
			\end{itemize}
		\end{block}
		\medskip

		\begin{block}{$f$ is self-stabilising iff}
			\begin{itemize}
				\item given two stabilising inputs $v_1$, $v_2$ with the same limit, \pause
				\item also $f(v_1)$, $f(v_2)$ are stabilising to the same limit \pause
			\end{itemize}
		\end{block}
		\medskip

		\begin{center}
			\textbf{yes, but when?} \\ \pause
			and how we \highlight{measure} time anyway?
		\end{center}
	\end{frame}

	\slidecount{}
	\begin{frame}{The synchronous case}
		\begin{block}{synchronous networks}
			\begin{itemize}
				\item a network is \highlight{synchronous} if events of devices happen all together \pause
				\item can measure time in number of \highlight{rounds} \pause
				\item \ldots count how many rounds before \highlight{everybody} reaches the limit
			\end{itemize}
		\end{block}

		\begin{center}
			\includegraphics[width=0.6\linewidth]{img/estruct_synch.pdf}
		\end{center}
	\end{frame}

	\slidecount{}
	\begin{frame}{The synchronous case}
		\begin{block}{general composable formulation}
			\begin{itemize}
				\item given some properties $p_1, \ldots p_n$ of network and input \pause
				\item $r(p_1, \ldots p_n)$ rounds \highlight{after} network and input are stable \pause
				\item \ldots the output is also stable
			\end{itemize}
		\end{block}

		\begin{center}
			\includegraphics[width=0.6\linewidth]{img/estruct_synch.pdf}
		\end{center}
	\end{frame}

	\slidecount{}
	\begin{frame}{The synchronous case}
		\begin{block}{example: ABF hop-count distance (unitary edge lengths)}
			\begin{itemize}
				\item if the maximum distance in hops of a node from the source is $D$ \pause
				\item $D$ rounds \highlight{after} network and source are stable, output is also stable \pause
			\end{itemize}
		\end{block}
		\medskip

		\begin{block}{example: single-path collection}
			\begin{itemize}
				\item if the maximum distance in hops of a node from the source is $D$ \pause
				\item $D$ rounds \highlight{after} network, source and distances are stable, output is also stable \pause
			\end{itemize}
		\end{block}
		\medskip

		\begin{block}{example: G+C combination}
			\begin{itemize}
				\item if the maximum distance in hops of a node from the source is $D$ \pause
				\item $2D$ rounds \highlight{after} network and source are stable, output is also stable \pause
			\end{itemize}
		\end{block}

		\begin{center}
			\textbf{composition works!}
		\end{center}
	\end{frame}

	\slidecount{}
	\begin{frame}{The asynchronous case}
		\begin{block}{how we measure time?}
			\begin{itemize}
				\item there may be \highlight{many} different ways \pause
				\item \highlight{full round} of execution passes after every device computed \highlight{at least once} \pause
				\item may work as analogous to \highlight{round} count in synchronous networks
			\end{itemize}
		\end{block}

		\begin{center}
			\includegraphics[width=0.6\linewidth]{img/estruct_gossip.pdf}
		\end{center}
	\end{frame}

	\slidecount{}
	\begin{frame}{The asynchronous case}
		\begin{block}{example: ABF hop-count distance (unitary edge lengths)}
			\begin{itemize}
				\item if the maximum distance in hops of a node from the source is $D$
				\item $D$ full rounds \highlight{after} network and source are stable, output is also stable
			\end{itemize}
		\end{block} \pause
		\medskip

		\begin{block}{example: single-path collection}
			\begin{itemize}
				\item if the maximum distance in hops of a node from the source is $D$
				\item $D$ full rounds \highlight{after} network, source, distances are stable, output is also stable
			\end{itemize}
		\end{block} \pause
		\medskip

		\begin{block}{example: G+C combination}
			\begin{itemize}
				\item if the maximum distance in hops of a node from the source is $D$
				\item $2D$ full rounds \highlight{after} network and source are stable, output is also stable
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{How can we prove it?}
		\begin{block}{induction is the new black} \pause
			\begin{itemize}
				\item \textbf{base case}: some property $P$ holds for $0$ \pause
				\item \textbf{inductive case}: if $P$ holds for $n$, then holds for $n+1$ \pause
				\item then $P$ holds for every $n$! \pause
			\end{itemize}
		\end{block}
		\medskip

		\begin{block}{induction for stabilisation}
			\begin{itemize}
				\item $P(n) = $ after $n$ rounds, nodes satisfying $Q(n)$ are stable \pause
				\item \textbf{base case}: immediately when input stable, something stabilises \pause
				\item \textbf{inductive case}: stabilisation spreads every round \pause
				\item everything is stable after $n$ such that $Q(n)$ holds everywhere
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{How can we prove it?}
		\begin{block}{example: ABF hop-count distance (unitary edge lengths)}
			\begin{itemize}
				\item $n$ full rounds \highlight{after} network and source are stable, \pause
				\item distance is stable in nodes up to distance $n$ from the \highlight{source} \pause
				\item \ldots and is greater than $n$ in other nodes \pause
			\end{itemize}
		\end{block}
		\medskip

		\begin{block}{example: single-path collection}
			\begin{itemize}
				\item $n$ full rounds \highlight{after} network, source, distances are stable, \pause
				\item collection is stable in nodes up to distance $n$ from the \highlight{edge} \pause
			\end{itemize}
		\end{block}
		\medskip

		\begin{block}{Lyapunov functions may also help}
			\begin{itemize}
				\item function $f$ of the current configuration \pause
				\item prove that can only \highlight{decrease} and has a \highlight{minimum} \pause
				\item then it has to stop changing
			\end{itemize}
		\end{block}
	\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\resetsec{5}
\section{Controlling transient errors}

	\slidecount{}
	\begin{frame}{Outline}
		\begin{itemize}
			\item Abstract models vs real world
			\bigskip
			\item Bounding transient length
			\bigskip
			\item \highlight{Controlling transient errors}
			\begin{itemize}
				\item Controlling for time-independent
				\item Controlling for continuous
				\item Examples
			\end{itemize}
		\end{itemize}
	\end{frame}

	\slidecount{}
	\begin{frame}{Controlling for time-independent}
		\begin{block}{general idea}
			\begin{itemize}
				\item definitely \highlight{many} ways to do it \pause
				\item need to measure \highlight{distance} between transient and limit \pause
				\item and bound it on \highlight{initial} distance and passing of \highlight{rounds} \pause
			\end{itemize}
		\end{block}
		\medskip

		\begin{block}{how to measure distance?}
			\begin{itemize}
				\item \textbf{Hamming norm:} distance is \highlight{number} of devices with different value \pause
				\item \textbf{$L^\infty$ norm:} distance is \highlight{maximum} across devices of distances \pause
				\item \textbf{$L^1$ norm:} distance is \highlight{mean} across devices of distances \pause
				\item \textbf{$L^2$ norm:} distance is \highlight{square root} of the mean of \highlight{square} distances\ldots \pause
			\end{itemize}
		\end{block}
		\medskip

		\begin{block}{how to bound it?}
			\begin{itemize}
				\item just in any way you can
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{Controlling for continuous}
		\begin{block}{general idea}
			\begin{itemize}
				\item again \highlight{many} ways to do it \pause
				\item need to fix \highlight{density properties} of the network \pause
				\item and bound \highlight{distance} between real value and continuous limit \pause
			\end{itemize}
		\end{block}
		\medskip

		\begin{block}{how to measure distance?}
			\begin{itemize}
				\item Hamming, $L^\infty$, $L^2$, $L^1$ norms usually work pretty well ($L^\infty$ mostly) \pause
			\end{itemize}
		\end{block}
		\medskip

		\begin{block}{which density properties?}
			\begin{itemize}
				\item minimum number of \highlight{devices} per volume of space \pause
				\item maximum \highlight{temporal} interval between rounds \pause
				\item maximum and minimum radius of \highlight{connection}\ldots
			\end{itemize}
		\end{block}
	\end{frame}

	\slidecount{}
	\begin{frame}{Examples}
		\begin{block}{continuous ABF [real-time]}
			\begin{itemize}
				\item if at least one \highlight{device} in a radius of $\rho$ \pause
				\item at least one \highlight{round} every $T$ \pause
				\item minimum \highlight{connection} $R$ \pause
				\item device movement \highlight{speed} at most $v$ \pause
				\item \highlight{ABF} distance $d(\eventId)$ is at most $\frac{R}{R - 2\rho - 2vT} \hat{d}(\eventId)$ \highlight{Euclidean} distance \pause
			\end{itemize}
		\end{block}
		\medskip

		\begin{block}{transient of single-path summation [w.i.p.]}
			\begin{itemize}
				\item $n$ rounds \highlight{after} stabilisation of input values $v_i$ (not sources/distances) \pause
				\item all values are below $n \sum_i v_i + K$ (\highlight{linear} growth of error) \pause
				\item not so good, but at least is not \highlight{exponential}
			\end{itemize}
		\end{block}
	\end{frame}

\end{document}
