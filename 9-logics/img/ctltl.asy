texpreamble("\usepackage{amssymb}");
include roundedpath;
include math;

picture ltl;
unitsize(ltl, 1mm);
draw(ltl, (0,0) -- (150,0), black+1, Arrows);
for (int i=15; i<140; i+=10)
    filldraw(ltl, shift(i,0)*unitcircle, i == 75 ? lightgray : black, (i == 75 ? black : gray)+1);
shipout("ltl", ltl);

guide[] multipath(int n) {
    int b = rand()%4;
    if (n == 0 || b == 0) return new guide[]{(0,0)};
    guide[] r;
    for (int i=0; i<b; ++i) {
        guide[] s = multipath(n-1);
        for (guide g : s)
            r[r.length] = (0,0) {E}::{E} shift(20, 5*n*i-2.5*(b-1)*n)*g;
    }
    return r;
}

picture ctl;
unitsize(ctl, 1mm);
guide[] r;
r = multipath(3);
for (guide g : r)
    for (int i=1; i<length(g); ++i)
        filldraw(ctl, shift(point(g, i))*unitcircle, black, gray+1);
for (guide g : r)
    draw(ctl, g, black, EndArrow);
r = multipath(3);
for (guide g : r)
    for (int i=1; i<length(g); ++i)
        filldraw(ctl, rotate(180)*shift(point(g, i))*unitcircle, black, gray+1);
for (guide g : r)
    draw(ctl, rotate(180)*g, black, EndArrow);
filldraw(ctl, unitcircle, lightgray, black+1);
shipout("ctl", ctl);
