texpreamble("\usepackage{amssymb}");
include roundedpath;
include math;

bool DEBUG = false;

int MX = 10, MY = 4;

real PN = 0.8;
real PE = 0.8;
real PD = 0.2;

real SEP = 0.7;


real luminance(pen p) {
    real[] c = colors(rgb(p));
    return sqrt( 0.299*c[0]^2 + 0.587*c[1]^2 + 0.114*c[2]^2 );
}

pen darkest(pen a, pen b) {
    if (a == b) return a;
    if (a == olive) return b;
    if (b == olive) return a;
    return black;
    return luminance(a) < luminance(b) ? a : b;
}

pen lighter(pen a) {
    return 0.8*white + 0.2*a;
}

pair getdim(real[][] E) {
    pair dim = (0, 0.8*E.length);
    for (int i=0; i<E.length; ++i)
        for (int j=0; j<E[i].length; ++j)
            dim = (max(dim.x, E[i][j]), dim.y);
    return dim+(1.2,.6);
}

void drawarrow(picture pic = currentpicture, pair s, pair e, pen c = black) {
    pair dir = unit(e-s);
    s = s+0.22*dir;
    e = e-0.22*dir;
    real w = 0.05;
    int n = max((int)(length(e-s)/w-3), 0);
    path p = s -- e;
    //for (int i=0; i<n; ++i) p = p -- s+(w/2+w*i)*dir+rotate(i%2==0 ? 90 : -90)*(w/2*dir);
    //p = p -- s+(w*n*dir) -- e;
    draw(pic, p, c, Arrows(4));
}

void drawnode(picture pic = currentpicture, pair pos, string l = "", pen col = black) {
    pen fg = length(l)>0 ? col : lighter(col);
    pen bk = length(l)>0 ? lighter(col) : col;
    fill(pic, circle(pos, .19), fg);
    fill(pic, circle(pos, .16), bk);
    if (length(l) > 0) label(pic, scale(0.7)*("$"+l+"$"),pos, fg);
}

pair[][] nodes;
bool[][] visible;
for (int i=0; i<MX; ++i) {
    nodes[i] = new pair[];
    visible[i] = new bool[];
    for (int j=0; j<MY; ++j) {
        nodes[i][j] = (-1,-1);
        pair p = (i,j) + (unitrand(), unitrand());
        if (i>0 && visible[i-1][j]) p = (max(p.x, nodes[i-1][j].x+SEP), p.y);
        if (i>0 && j>0 && visible[i-1][j-1]) p = (max(p.x, nodes[i-1][j-1].x+SEP), p.y);
        if (j>0 && visible[i][j-1]) p = (p.x, max(p.y, nodes[i][j-1].y+SEP));
        if (i>0 && j<MY-1 && visible[i-1][j+1]) p = (p.x, min(p.y, nodes[i-1][j+1].y-SEP));
        nodes[i][j] = p;
        visible[i][j] = unitrand() < PN;
    }
}

bool accepted(int i1, int j1, int i2, int j2, real p) {
    if (i2<0 || j2<0 || j2>=MY || !visible[i2][j2]) return false;
    if (length(nodes[i1][j1] - nodes[i2][j2]) > 2) return false;
    if (length(nodes[i1][j1] - nodes[i2][j2]) < 1) p += 0.1;
    return unitrand() < p;
}

picture arrows;
unitsize(arrows, 1cm);

for (int i=0; i<MX; ++i)
    for (int j=0; j<MY; ++j) if (visible[i][j]) {
        if (accepted(i,j,i-1,j,  PE)) drawarrow(arrows, nodes[i][j], nodes[i-1][j]);
        if (accepted(i,j,i,  j-1,PE)) drawarrow(arrows, nodes[i][j], nodes[i][j-1]);
        if (accepted(i,j,i-1,j+1,PD)) drawarrow(arrows, nodes[i][j], nodes[i-1][j+1]);
        if (accepted(i,j,i-1,j-1,PD)) drawarrow(arrows, nodes[i][j], nodes[i-1][j-1]);
    }

path X = roundedpath((0.3,0.1) -- (0,3.8) -- (4.0,4.2) -- (9,4.1) -- (9.9,3.1) -- (10,0) -- (3.0,-0.1) -- cycle, 0.5);
path CCA = roundedpath((2.3,0) -- (1.5,0.3) -- (2.2,1.2) -- (2.2,3.1) -- (3.3,4.1) -- (6.9,4.0) -- (7.3,0.3) -- cycle, 0.4);
path CA = roundedpath((3.0,0.6) -- (3.1,3.7) -- (5.9,3.9) -- (6.9,2.9) -- (6.9,1.8) -- (5.7,0.7) -- cycle, 0.5);
path A = roundedpath((4.1,1.3) -- (4.2,2.2) -- (5.1,3.1) -- (5.9,3.1) -- (5.9,2.3) -- (5.0,1.4) -- cycle, 0.3);
path B = roundedpath((3.0,0.6) -- (3.1,3.7) -- (6.2,3.9) -- (5.3,0.7) -- cycle, 0.5);
path D = roundedpath((1.1,2.3) -- (1.0,3.3) -- (3.1,3.2) -- (2.9,2.4) -- cycle, 0.4);

void drawset(picture pic = currentpicture, string name, path contour, pair pin, pen col) {
    real intensity = colors(gray(col))[0];
    pen pale  = 0.75*white + 0.25*col;
    pen heavy = min(0.75, 0.25/intensity)*col;
    filldraw(pic, contour, pale, heavy+1);
    label(pic, "$"+name+"$", pin, heavy);
}

void drawnodes(picture pic = currentpicture, pen col, pen[][] cols = new pen[][] {}) {
    filldraw(pic, X, invisible, invisible+1);
    add(pic, arrows);
    for (int i=0; i<MX; ++i) {
        if (i >= cols.length) cols[i] = new pen[] {};
        for (int j=0; j<MY; ++j) {
            if (j >= cols[i].length) cols[i][j] = col;
            if (visible[i][j]) drawnode(pic, nodes[i][j], DEBUG ? string(i)+string(j) : "", cols[i][j]);
        }
    }
}


picture space;
unitsize(space, 1cm);

drawset(space, "X",      X,  (7.7, 3.9), lightgray);
drawset(space, "C(C(A))",CCA,(6.4, 0.6), blue);
drawset(space, "C(A)",   CA, (5.1, 1.0), magenta);
drawset(space, "A",      A,  (5.25,2.05),red);

shipout("space", space);


picture graph;
unitsize(graph, 1cm);

add(graph, space);
drawnodes(graph, black);

shipout("graph", graph);


picture home1;
unitsize(home1, 1cm);

drawset(home1, "P",         A, (5.25,2.05),green);
drawnodes(home1, gray, new pen[][] {
    {},
    {},
    {gray, gray, gray, black},
    {gray, yellow},
    {},
    {grey, grey, black},
    {},
    {gray, yellow}
});

shipout("home1", home1);


picture home1l;
unitsize(home1l, 1cm);

drawset(home1l, "\Diamond P",CA,(5.1, 1.0), cyan);
drawset(home1l, "P",         A, (5.25,2.05),green);
drawnodes(home1l, gray, new pen[][] {
    {},
    {},
    {gray, gray, gray, black},
    {gray, yellow},
    {},
    {grey, grey, yellow},
    {},
    {gray, black}
});

shipout("home1l", home1l);


picture home1g;
unitsize(home1g, 1cm);

drawset(home1g, "\mathcal{F}P",X, (7.7, 3.9), cyan);
drawset(home1g, "P",           A, (5.25,2.05),green);
drawnodes(home1g, gray, new pen[][] {
    {},
    {},
    {gray, gray, gray, yellow},
    {gray, yellow},
    {},
    {grey, grey, yellow},
    {},
    {gray, yellow}
});

shipout("home1g", home1g);


picture home2;
unitsize(home2, 1cm);

drawset(home2, "A", CA, (5.1, 1.0), blue);
drawnodes(home2, gray, new pen[][] {
    {gray, gray, black},
    {},
    {},
    {gray, black},
    {},
    {gray, black, gray, black},
    {},
    {},
    {gray, gray, black}
});

shipout("home2", home2);


picture home2l;
unitsize(home2l, 1cm);

drawset(home2l, "A", CA, (5.1, 1.0), blue);
drawnodes(home2l, gray, new pen[][] {
    {gray, gray, heavyred},
    {},
    {},
    {gray, heavygreen},
    {},
    {gray, heavyred, gray, heavygreen},
    {},
    {},
    {gray, gray, heavyred}
});

shipout("home2l", home2l);


picture home2g;
unitsize(home2g, 1cm);

drawset(home2g, "A", CA, (5.1, 1.0), blue);
drawnodes(home2g, gray, new pen[][] {
    {gray, gray, heavyred},
    {},
    {},
    {gray, heavyred},
    {},
    {gray, heavyred, gray, heavyred},
    {},
    {},
    {gray, gray, heavyred}
});

shipout("home2g", home2g);


picture home3;
unitsize(home3, 1cm);

drawset(home3, "\bot", B, (5.0, 1.0), magenta);
drawnodes(home3, gray, new pen[][] {
    {blue, gray, gray, blue},
    {gray, gray, heavyred},
    {},
    {heavyred, gray, heavyred},
    {},
    {gray, gray, heavyred},
    {},
    {gray, heavyred},
    {gray, gray, blue},
    {blue}
});

shipout("home3", home3);


picture home4;
unitsize(home4, 1cm);

drawset(home4, "\bot", D, (1.3, 3.05), magenta);
drawnodes(home4, gray, new pen[][] {
    {blue, gray, gray, blue},
    {gray, gray, heavyred},
    {gray, gray, heavyred},
    {},
    {heavyred, heavyred},
    {},
    {gray, heavyred},
    {heavyred, heavyred},
    {gray, gray, blue},
    {blue}
});

shipout("home4", home4);
