\documentclass[10pt]{beamer} %,aspectratio=169,handout
	\usetheme{CambridgeUS} %CambridgeUS, Goettingen, Singapore, Madrid
	\setbeamertemplate{footline}[frame number]
	\setbeamercovered{transparent}
	\beamertemplatenavigationsymbolsempty

\usepackage{multimedia}
\usepackage{tabularx}
\usepackage{listings}
\usepackage{forloop}
\usepackage{mathtools}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{wasysym}
\usepackage{stmaryrd}
\newcolumntype{M}[1]{>{\centering\arraybackslash}m{#1}}
\setlength\tabcolsep{4pt}

\input{../macros}

\newcounter{prog}
\newcommand{\progress}[2]{$\forloop{prog}{0}{\value{prog} < #1}{\bullet}\forloop{prog}{0}{\value{prog} < #2}{\circ}$}
\newcounter{secslide}
\newcounter{sectotal}
\newcounter{secother}
\newcommand{\resetsec}[1]{\setcounter{secslide}{0}\setcounter{sectotal}{#1}}
\newcommand{\slidecount}{\addtocounter{secslide}{1}\setcounter{secother}{\value{sectotal} - \value{secslide}}\subsection[\progress{\value{secslide}}{\value{secother}}]{slide}}

\lstdefinelanguage{hfc}{
	basicstyle=\lst@ifdisplaystyle\small\fi\ttfamily,
	frame=single,
	basewidth=0.5em,
	sensitive=true,
	morestring=[b]",
	morecomment=[l]{//},
	morecomment=[n]{/*}{*/},
	commentstyle=\color{gray},
	keywordstyle=\color{blue}\textbf, keywords={def}, otherkeywords={=>},
	keywordstyle=[2]\color{red}\textbf, keywords=[2]{rep,nbr,share,if,let,in},
	keywordstyle=[3]\color{violet}, keywords=[3]{mux,sumHood,countHood,everyHood,locHood,allHood,anyHood,minHood,maxHood,foldHood,maxHoodPlusSelf,nbrDist,snd},
	keywordstyle=[4]\color{orange}\textbf, keywords=[4]{spawn},
	keywordstyle=[5]\color{blue}, keywords=[5]{false,true,infinity}
}

\definecolor{darkgreen}{rgb}{0.0, 0.5, 0.0}
\definecolor{darkred}{rgb}{0.5, 0.0, 0.0}
\definecolor{darkblue}{rgb}{0.2, 0.2, 0.7}
\definecolor{palegray}{rgb}{0.9, 0.9, 0.9}

\makeatletter
\newcommand{\ssymbol}[1]{${}^{\@fnsymbol{#1}}$}
\makeatother

\newcommand{\highlight}[1]{\textcolor{darkblue}{#1}}
\newcommand{\superlight}[1]{\textcolor{darkred}{#1}}
\newcommand{\greenlight}[1]{\textcolor{darkgreen}{#1}}
\newcommand{\DOI}[1]{\href{http://www.doi.org/#1}{\underline{#1}}}
\newcommand{\ACMDOI}[1]{\href{https://dl.acm.org/doi/#1}{\underline{#1}}}
\newcommand{\CITE}[5]{\ssymbol{#1}[#2. \emph{#3.} #4. \DOI{#5}]}

\theoremstyle{plain}
\newtheorem{proposition}[theorem]{Proposition}
\theoremstyle{remark}
\newtheorem{question}[theorem]{Question}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{Aggregate Programming for the Internet of Things}
\author{Giorgio Audrito, Ferruccio Damiani, Gianluca Torta}
\institute{{\small Dipartimento di Informatica\\[3pt] Universit\`a degli Studi di Torino}}
%\date{}
\titlegraphic{
	%\raisebox{-0.5\height}{\includegraphics[width=2.0cm]{../images/logo_horizon.jpg}} \quad
	\raisebox{-0.5\height}{\includegraphics[width=2.0cm]{../images/logo_torino.pdf}} \quad
	%\raisebox{-0.5\height}{\includegraphics[width=2.0cm]{../images/logo_sanpaolo.png}}
}

\begin{document}
\lstset{language={hfc}}

\section[~]{Title}
	\begin{frame}
		\titlepage
	\end{frame}

	\begin{frame}{Conceptual map}
		\vspace{-3mm}
		\begin{block}{}
		\begin{center}
			\includegraphics[height=0.86\textheight]{../images/map7.pdf}
		\end{center}
		\end{block}
	\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\resetsec{3}
\section{Runtime Verification}

\slidecount{}
\begin{frame}{Outline}
	\begin{itemize}
		\item \highlight{Runtime Verification}
		\begin{itemize}
			\item What is it?
			\item Runtime Verification for the IoT
		\end{itemize}
		\bigskip
		\item Temporal logic
		\bigskip
		\item Spatial logic
	\end{itemize}
\end{frame}

\slidecount{}
\begin{frame}{What is it?}
	\begin{block}{}
		\begin{itemize}
			\item \highlight{properties} of a computing system (safety/liveliness/correctness\ldots) \pause
			\item whenever proving them is too \highlight{hard}\ldots \pause
			\item you can \highlight{monitor} their failure instead! \pause
			\item automatic \highlight{synthesis} of monitors from specifications
		\end{itemize}
	\end{block}

	\begin{center}
		\includegraphics[height=3.5cm]{../2-general model/img/dev-arduino.png}
	\end{center}
\end{frame}

\slidecount{}
\begin{frame}{Runtime Verification for the IoT}
	\begin{block}{Several requirements need to be met:}
		\begin{itemize} \setlength\itemsep{5pt}
			\item fully \highlight{distributed} monitors \emph{(multi-hop networks)} \pause
			\item monitors \highlight{integrated} within the IoT system \pause
			\item \highlight{dynamic} devices and monitors \emph{(may fail, join, move)} \pause
			\item low \highlight{resource} consumption \emph{(limited device capabilities)}
		\end{itemize}
	\end{block}
	\medskip

	\centering
	\raisebox{-0.5\height}{ \hspace{-6.5pt}
		\includegraphics<1-2>[height=3.5cm]{../1-introduction/img/network.pdf}
		\includegraphics<3|handout:0>[height=3.5cm]{../1-introduction/img/challenge-dynamic.jpg}
		\includegraphics<4|handout:0>[height=3.5cm]{../2-general model/img/dev-arduino.png}
	}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\resetsec{8}
\section{Temporal logic}

\slidecount{}
\begin{frame}{Outline}
	\begin{itemize}
		\item Runtime Verification
		\bigskip
		\item \highlight{Temporal logic}
		\begin{itemize}
			\item Linear Time Logics (LTL)
			\item Branching Time Logics (CTL)
			\item Past-CTL on Event Structures
			\item Sample Applications
			\item Monitoring Past-CTL in Field Calculus
		\end{itemize}
		\bigskip
		\item Spatial logic
	\end{itemize}
\end{frame}

\slidecount{}
\begin{frame}{Linear Time Logics (LTL)}
	\begin{block}{}
		\centering{\framebox[\linewidth]{$
			\begin{array}{lcl@{\hspace{3mm}}r}
				\formula & \!\!\!\BNFcce\!\!\! & \bot \BNFmid \top \BNFmid \prop \BNFmid (\neg \formula) \BNFmid (\formula \wedge \formula) \BNFmid (\formula \vee \formula) \BNFmid (\formula \Rightarrow \formula) \BNFmid (\formula \Leftrightarrow \formula)
				&{\footnotesize \mbox{logical op.}}
				\\[8pt] \pause
				& & \BNFmid (\DY \formula) \BNFmid (\formula \DS \formula) \BNFmid (\DP \formula) \BNFmid (\DH \formula)
				&{\footnotesize \mbox{future op.}}
				\\[8pt] \pause
				& & \BNFmid (\DX \formula) \BNFmid (\formula \DU \formula) \BNFmid (\DF \formula) \BNFmid (\DG \formula)
				&{\footnotesize \mbox{past op.}}
				\\[3pt]
			\end{array}
		$}}
	\end{block}
	\bigskip
	\bigskip
	\bigskip

	\begin{center}
		\includegraphics[width=\linewidth]{img/ltl.pdf}
	\end{center}
\end{frame}

\slidecount{}
\begin{frame}{Branching Time Logics (CTL)}
	\begin{block}{}
		\centering{\framebox[\linewidth]{$
			\begin{array}{lcl@{\hspace{3mm}}r}
				\formula & \!\!\!\BNFcce\!\!\! & \bot \BNFmid \top \BNFmid \prop \BNFmid (\neg \formula) \BNFmid (\formula \wedge \formula) \BNFmid (\formula \vee \formula) \BNFmid (\formula \Rightarrow \formula) \BNFmid (\formula \Leftrightarrow \formula)
				&{\footnotesize \mbox{logical op.}}
				\\[8pt] \pause
				& & \BNFmid (\DY \formula) \BNFmid (\AY \formula) \BNFmid (\EY \formula) \BNFmid (\formula \DS \formula) \BNFmid (\formula \AS \formula) \BNFmid (\formula \ES \formula)
				&{\footnotesize \mbox{future op.}}
				\\[3pt]
				& & \BNFmid (\DP \formula) \BNFmid (\AP \formula) \BNFmid (\EP \formula) \BNFmid (\DH \formula) \BNFmid (\AH \formula) \BNFmid (\EH \formula) &
				\\[8pt] \pause
				& & \BNFmid (\DX \formula) \BNFmid (\AX \formula) \BNFmid (\EX \formula) \BNFmid (\formula \DU \formula) \BNFmid (\formula \AU \formula) \BNFmid (\formula \EU \formula)
				&{\footnotesize \mbox{past op.}}
				\\[3pt]
				& & \BNFmid (\DF \formula) \BNFmid (\AF \formula) \BNFmid (\EF \formula) \BNFmid (\DG \formula) \BNFmid (\AG \formula) \BNFmid (\EG \formula) &
				\\[3pt]
			\end{array}
		$}}
	\end{block}
	\bigskip

	\begin{center}
		\includegraphics[width=\linewidth]{img/ctl.pdf}
	\end{center}
\end{frame}

\slidecount{}
\begin{frame}{Past-CTL on Event Structures}
	\begin{block}{}
		\begin{itemize} \setlength\itemsep{1em}
			\item $\sem{\top}{\EventS}(\eventId) = \top$, and $\sem{\prop}{\EventS}(\eventId)$ is $\dvalue_\prop(\eventId)$ \pause
			\item $\sem{\neg \formula}{\EventS}(\eventId) = \neg \sem{\formula}{\EventS}(\eventId)$ and $\sem{\formula_1 \vee \formula_2}{\EventS}(\eventId) = \sem{\formula_1}{\EventS}(\eventId) \vee \sem{\formula_2}{\EventS}(\eventId)$ \pause
			\item $\sem{\DX \formula}{\EventS}(\eventId) = \sem{\formula}{\EventS}(\eventId')$ where $\eventId'$ is the event preceding $\eventId$ on the same device \newline (if it exists, $\sem{\DX \formula}{\EventS}(\eventId) = \bot$ otherwise) \pause
			\item $\sem{\AX \formula}{\EventS}(\eventId) = \bigwedge_{\eventId' \neigh \eventId} \sem{\formula}{\EventS}(\eventId')$ ($\formula$ is true in each preceding event $\eventId'$) \pause
			\item $\sem{\formula_1 \AU \formula_2}{\EventS}(\eventId)$ holds iff for every path $\eventId_1 \neigh \ldots \neigh \eventId_n = \eventId$ such that \\ $\eventId_1$ has no neighbours, $\exists i.~ \sem{\formula_2}{\EventS}(\eventId_i)$ and $\forall j > i.~ \sem{\formula_1}{\EventS}(\eventId_j)$ \pause
			\item $\sem{\formula_1 \EU \formula_2}{\EventS}(\eventId)$ holds iff it exists a path $\eventId_1 \neigh \ldots \neigh \eventId_n = \eventId$ such that $\sem{\formula_2}{\EventS}(\eventId_1)$ holds and $\sem{\formula_1}{\EventS}(\eventId_i)$ holds for $i = 2 \ldots n$ \pause
			\item $\sem{\formula_1 \DU \formula_2}{\EventS}(\eventId)$ holds iff it exists a path $\eventId_1 \neigh \ldots \neigh \eventId_n = \eventId$ of events all on $\deviceId = \devof(\eventId)$, such that $\sem{\formula_2}{\EventS}(\eventId_1)$ holds and $\sem{\formula_1}{\EventS}(\eventId_i)$ holds for $i = 2 \ldots n$
		\end{itemize}
	\end{block}
\end{frame}

\slidecount{}
\begin{frame}{Sample Applications}
	\begin{block}{Crowd Safety}
		\emph{Whenever in safety during an alert, we do not regress:}
		$$ \mathbf{\AG} (\DX (\mathit{safe} \wedge \mathit{alert}) \to (\mathit{safe} \vee \neg\mathit{alert}))$$
	\end{block}
	\bigskip

	\begin{center}
		\raisebox{-0.5\height}{\includegraphics[width=0.49\textwidth]{img/crowd_safety.png}}
		\raisebox{-0.5\height}{\includegraphics[width=0.50\textwidth]{img/crowd_safety.pdf}}
	\end{center}
\end{frame}

\slidecount{}
\begin{frame}{Sample Applications}
	\begin{block}{Drones Recognition}
		\begin{itemize}
			\item \emph{Area $i$ gets eventually handled by a drone (liveness):} $\EF \text{done}_i$
			\item \emph{No drone is handling an area that knows to be already handled (safety):}
			$$\AG \neg (\text{done}_i \wedge \EX (\EF \text{done}_i))$$
		\end{itemize}
	\end{block}

	\begin{center}
		\raisebox{-0.5\height}{\includegraphics[width=0.49\textwidth]{img/drones_recognition.png}}
		\raisebox{-0.5\height}{\includegraphics[width=0.50\textwidth]{img/drones_recognition.pdf}}
	\end{center}
\end{frame}

\slidecount{}
\begin{frame}{Sample Applications}
	\begin{block}{Smart Home}
		\emph{Lights are on iff some people have been in the immediate vicinity in the close past:}
		\[
		{\text{light} \rightarrow (\text{people} \wedge \DX \text{people} \rightarrow \text{on}) \wedge (\neg \text{people} \wedge \DX \neg \text{people} \rightarrow \neg \text{on})}.
		\]
	\end{block}

	\begin{center}
		\raisebox{-0.5\height}{\includegraphics[width=0.49\textwidth]{img/smart_home.png}}
		\raisebox{-0.5\height}{\includegraphics[width=0.50\textwidth]{img/smart_home.pdf}}
	\end{center}
\end{frame}

\slidecount{}
\begin{frame}{Monitoring Past-CTL in Field Calculus}
	\begin{block}{} \small
		\centering
		\begin{tabular}{|@{\hspace{8.2mm}}rcl@{\hspace{29mm}}|rcl@{\hspace{13.2mm}}|}
			\hline
			$\Qp{\top}$ &=& \lstinline[mathescape]/true/ &
			$\Qp{\formula_1 \vee \formula_2}$ &=& \lstinline[mathescape]/$\Qp{\formula_1}\quad$ || $\quad\Qp{\formula_2}$/ \\
			$\Qp{\bot}$ &=& \lstinline[mathescape]/false/ &
			$\Qp{\formula_1 \wedge \formula_2}$ &=& \lstinline[mathescape]/$\Qp{\formula_1}\quad$ \&\& $\quad\Qp{\formula_2}$/ \\
			$\Qp{\prop}$ &=& \lstinline[mathescape]/q()/ &
			$\Qp{\formula_1 \Rightarrow \formula_2}$ &=& \lstinline[mathescape]/$\Qp{\formula_1}\quad$ <= $\quad\Qp{\formula_2}$/ \\
			$\Qp{\neg \formula}$ &=& \lstinline[mathescape]/!$\Qp{\formula}$/ &
			$\Qp{\formula_1 \Leftrightarrow \formula_2}$ &=& \lstinline[mathescape]/$\Qp{\formula_1}\quad$ == $\quad\Qp{\formula_2}$/ \\
			\hline
		\end{tabular} \pause
		\begin{tabular}{|rcl|}
			\hline
			$\Qp{\DX \formula}$ &=& \lstinline[mathescape]/snd(share([false,false])\{(old)=>[$\Qp{\formula}$,locHood(fst(old))]\})/ \\
			$\Qp{\AX \formula}$ &=& \lstinline[mathescape]/snd(share([true, true ])\{(old)=>[$\Qp{\formula}$,allHood(fst(old))]\})/ \\
			$\Qp{\EX \formula}$ &=& \lstinline[mathescape]/snd(share([false,false])\{(old)=>[$\Qp{\formula}$,anyHood(fst(old))]\})/ \\
			\hline \pause
			$\Qp{\formula_1 \!\DU \formula_2}$ &=& \lstinline[mathescape]/share (false) \{(old) => $\!\!\quad\Qp{\formula_2}\quad\!\!$ || ($\Qp{\formula_1}$ \&\& locHood(old))\}/ \\
			$\Qp{\formula_1 \!\AU \formula_2}$ &=& \lstinline[mathescape]/share (false) \{(old) => $\!\!\quad\Qp{\formula_2}\quad\!\!$ || ($\Qp{\formula_1}$ \&\& allHood(old))\}/ \\
			$\Qp{\formula_1 \!\EU \formula_2}$ &=& \lstinline[mathescape]/share (false) \{(old) => $\!\!\quad\Qp{\formula_2}\quad\!\!$ || ($\Qp{\formula_1}$ \&\& anyHood(old))\}/ \\
			\hline \pause
			$\Qp{\DF \formula}$ &=& \lstinline[mathescape]/share (false) \{(old) => $\!\!\quad\Qp{\formula}\quad\!\!$ || locHood(old)\}/ \\
			$\Qp{\AF \formula}$ &=& \lstinline[mathescape]/share (false) \{(old) => $\!\!\quad\Qp{\formula}\quad\!\!$ || allHood(old)\}/ \\
			$\Qp{\EF \formula}$ &=& \lstinline[mathescape]/share (false) \{(old) => $\!\!\quad\Qp{\formula}\quad\!\!$ || anyHood(old)\}/ \\
			\hline \pause
			$\Qp{\DG \formula}$ &=& \lstinline[mathescape]/share (true) \{(old) => $\!\!\quad\Qp{\formula}\quad\!\!$ \&\& locHood(old)\}/ \\
			$\Qp{\AG \formula}$ &=& \lstinline[mathescape]/share (true) \{(old) => $\!\!\quad\Qp{\formula}\quad\!\!$ \&\& allHood(old)\}/ \\
			$\Qp{\EG \formula}$ &=& \lstinline[mathescape]/share (true) \{(old) => $\!\!\quad\Qp{\formula}\quad\!\!$ \&\& anyHood(old)\}/ \\
			\hline
		\end{tabular}
	\end{block}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\resetsec{10}
\section{Spatial logic}

\slidecount{}
\begin{frame}{Outline}
	\begin{itemize}
		\item Runtime Verification
		\bigskip
		\item Temporal logic
		\bigskip
		\item \highlight{Spatial logic}
		\begin{itemize}
			\item Monitoring Spatial Properties
			\item Spatial Logic of Closure Spaces
			\item Sample Applications
			\item SLCS in Field Calculus
		\end{itemize}
	\end{itemize}
\end{frame}

\slidecount{}
\begin{frame}[t]{Monitoring Spatial Properties}
	\begin{block}{}
		\begin{itemize} \setlength\itemsep{8pt}
			\item introduce the \highlight{spatial logic of closure spaces} (SLCS) for expressing properties of situated, distributed systems to be monitored
			\onslide<2->{\item provide sample applications in \highlight{smart home} and \highlight{emergency} settings}
			\onslide<3->{\item outline a translation of SLCS \highlight{formulas} into a field calculus \highlight{monitor} for them}
		\end{itemize}
	\end{block}
	\bigskip

	\centering
	\raisebox{-0.5\height}{ \hspace{-6.5pt}
		\only<1|handout:0>{\begin{tikzpicture}
			\fill[white,rounded corners] (-0.4\textwidth,-0.7cm) rectangle (0.4\textwidth,2.3cm);
			\fill[orange,rounded corners] (-0.36\textwidth,-0.4cm) rectangle (0.36\textwidth,2cm);
			\node at (0,1.6cm) {\textcolor{white}{$\bot \BNFmid \top \BNFmid \prop \BNFmid (\neg \formula) \BNFmid (\formula \wedge \formula) \BNFmid (\formula \vee \formula) \BNFmid (\formula \Rightarrow \formula) \BNFmid (\formula \Leftrightarrow \formula)$}};
			\node at (0,0.8cm) {\textcolor{white}{$\BNFmid (\SI \formula) \BNFmid (\SC \formula) \BNFmid (\SB \formula) \BNFmid (\SBI \formula) \BNFmid (\SBC \formula)$}};
			\node at (0,0) {\textcolor{white}{$\BNFmid (\formula \SR \formula) \BNFmid (\formula \ST \formula) \BNFmid (\formula \SU \formula) \BNFmid (\SG \formula) \BNFmid (\SF \formula)$}};
		\end{tikzpicture}}
		\includegraphics<2|handout:0>[height=3cm]{../1-introduction/img/setting-smarthome.png}
		\only<3>{\vspace{-20pt}\begin{tikzpicture}
			\fill[white,rounded corners] (-5,0cm) rectangle (5,1cm);
			\fill[orange,rounded corners] (-6.1,-0.7cm) rectangle (-3.1,-0.1cm);
			\fill[palegray,rounded corners] (2.4,0.5cm) rectangle (-2.4,1.1cm);
			\node at (-4.6,-0.4cm) {\textcolor{white}{$\neg D \vee (O \Leftrightarrow \SF P)$}};
			\node at (-2.75,0.2cm) {\huge$\nearrow$};
			\node at (0,0.8cm) {\lstinline/!D || (O == somewhere(P))/};
			\node at (1.2,0.1cm) {\huge$\searrow$};
			\node at (2.4,-1.0cm) {\includegraphics[height=1.5cm]{../1-introduction/img/network.pdf}};
		\end{tikzpicture}}
	}
\end{frame}

\subsection{Spatial Logic of Closure Spaces}

\slidecount{}
\begin{frame}{Closure Spaces}
	\begin{block}{Definition}
		A \highlight{closure space} is a set $X$ with a \emph{closure operator} $C : 2^X \to 2^X$ such that: \\
		$C(\emptyset) = \emptyset, \qquad\qquad A \subseteq C(A), \qquad\qquad C(A \cup B) = C(A) \cup C(B)$
	\end{block} \pause

	\begin{center}
		\includegraphics<1-2|handout:0>[height=4cm]{img/space.pdf}
		\includegraphics<3->[height=4cm]{img/graph.pdf}
	\end{center} \vspace{-10pt}

	\begin{block}{}
		\begin{itemize}
			\item generalizes \highlight{topological} spaces by not demanding $C(C(A)) = C(A)$ \pause
			\item contains \highlight{quasi-discrete} spaces for which $C(A) = \bigcup_{x \in A} C(\bp{x})$ \\
			\superlight{$\longrightarrow$ characterised as graphs where $C(v)$ are the neighbours of $v$}
		\end{itemize}
	\end{block}
\end{frame}

\slidecount{}
\begin{frame}[t]{Spatial Logic of Closure Spaces (SLCS)}
	\begin{block}{}
		\only<1-3|handout:0>{$
		\begin{array}{l@{\hspace{1mm}}l@{\hspace{10mm}}r}
			\formula \; \BNFcce & \bot \BNFmid \top \BNFmid \prop \BNFmid (\neg \formula) \BNFmid (\formula \wedge \formula) \BNFmid (\formula \vee \formula) \BNFmid (\formula \Rightarrow \formula) \BNFmid (\formula \Leftrightarrow \formula)
			&{\footnotesize \mbox{logical op.}}
			\\[3pt]
			& \BNFmid (\SI \formula) \BNFmid (\SC \formula) \BNFmid (\SB \formula) \BNFmid (\SBI \formula) \BNFmid (\SBC \formula)
			&{\footnotesize \mbox{local mod.}}
			\\[3pt]
			& \BNFmid (\formula \SR \formula) \BNFmid (\formula \ST \formula) \BNFmid (\formula \SU \formula) \BNFmid (\SG \formula) \BNFmid (\SF \formula)
			&{\footnotesize \mbox{global mod.}}
			\\[3pt]
		\end{array}
		$}
		\only<4>{$
		\begin{array}{l@{\hspace{1mm}}l@{\hspace{30mm}}r}
			\formula \; \BNFcce & \top \BNFmid \prop \BNFmid (\neg \formula) \BNFmid (\formula \vee \formula) \BNFmid (\SC \formula) \BNFmid (\formula \SR \formula)
			&{\footnotesize \mbox{fundamental op.}}
		\end{array}$
		\\[12pt] \footnotesize
		$
		\begin{array}{r@{\hspace{1mm}}c@{\hspace{1mm}}lr@{\hspace{1mm}}c@{\hspace{1mm}}lr@{\hspace{1mm}}c@{\hspace{1mm}}lr@{\hspace{1mm}}c@{\hspace{1mm}}l}
			\SI \formula &\triangleq& \neg (\SC (\neg \formula)) & \SB \formula &\triangleq& (\SC \formula) \wedge \neg (\SI \formula) &
			\SBI \formula &\triangleq& \formula \wedge \neg (\SI \formula ) & \SBC \formula &\triangleq& (\SC \formula) \wedge \neg \formula \\
			\formula \ST \formulalt &\triangleq& \formula \SR (\SC \formulalt) & \formula \SU \formulalt &\triangleq& \formula \wedge \SI \neg (\neg \formulalt \SR \neg \formula) &
			\SF \formula &\triangleq& \top \SR \formula & \SG \formula &\triangleq& \neg \SF \neg \formula
		\end{array}
		$}
	\end{block} \pause
	\smallskip

	\begin{block}{Local modalities}
		\begin{itemize}
			\item $\SC \formula$ \highlight{(closure)} holds at points with some neighbour satisfying $\formula$\only<3->{\ldots}
			\only<2|handout:0>{\item $\SI \formula$ \highlight{(interior)} holds at points with all neighbours satisfying $\formula$
			\item $\SB \formula$ \highlight{(boundary)} holds at points with some (not all) neighbours satisfying $\formula$
			\item $\SBI \formula$ \highlight{(interior boundary)} holds where $\formula$ and some neighbour $\neg \formula$
			\item $\SBC \formula$ \highlight{(closure boundary)} holds where $\neg \formula$ and some neighbour $\formula$}
		\end{itemize}
	\end{block} \pause
	\smallskip

	\begin{block}{Global modalities}
		\begin{itemize}
			\item $\formula \SR \formulalt$ \highlight{(reaches)} holds at the start of paths satisfying $\formula$ ending in $\formulalt$\only<1-2,4->{\ldots}
			\only<3|handout:0>{\item $\formula \ST \formulalt$ \highlight{(touches)} like reachability but $\formula$ may not hold in the ending point
			\item $\formula \SU \formulalt$ \highlight{(surrounded by)} holds in areas satisfying $\formula$ whose neighbours satisfy $\formulalt$
			\item $\SG \formula$ \highlight{(everywhere)} holds if $\formula$ is true everywhere
			\item $\SF \formula$ \highlight{(somewhere)} holds if $\formula$ is true somewhere}
		\end{itemize}
	\end{block} \pause
	~\\[9mm]
	\centering
	two modalities are \highlight{fundamental}, the rest is derived \\
	($\SR$ chosen for presentation convenience)
	\bigskip
	\bigskip
\end{frame}

\subsection{Sample Applications}

\slidecount{}
\begin{frame}{Sample Smart-Home Applications}
	\begin{block}{Electrical devices are on when people is present}
		\begin{itemize}
			\item $P$ true on points who are \highlight{sensing people}
			\item $D$ true on points which are \highlight{electrical devices}, $O$ true if they \highlight{are on}
		\end{itemize}
	\end{block} \pause

	\begin{center}
		\includegraphics<1-2|handout:0>[height=3.8cm]{img/home1.pdf}
		\includegraphics<3-4>[height=3.8cm]{img/home1l.pdf}
		\includegraphics<5|handout:0>[height=3.8cm]{img/home1g.pdf}
	\end{center} \vspace{-11.5pt}

	\begin{block}{}
		\begin{itemize}
			\item nearby people only:~ $D \Rightarrow (O \Leftrightarrow \SC P)$ \\
			\superlight{$\longrightarrow$ if I am an electrical device, I should be on iff \underline{a neighbour} senses people} \pause\pause
			\item farther away people: $D \Rightarrow (O \Leftrightarrow \SF P)$ \\
			\superlight{$\longrightarrow$ if I am an electrical device, I should be on iff \underline{anybody} senses people}
		\end{itemize}
	\end{block}
\end{frame}

\slidecount{}
\begin{frame}{Sample Smart-Home Applications}
	\begin{block}{High stereo level result in agreement on lowering}
		\begin{itemize}
			\item $L$ true on stereos with \highlight{high level}
			\item $A$ true on points agreeing on \highlight{lowering} the volume
		\end{itemize}
	\end{block} \pause

	\begin{center}
		\includegraphics<1-2|handout:0>[height=3.8cm]{img/home2.pdf}
		\includegraphics<3-4>[height=3.8cm]{img/home2l.pdf}
		\includegraphics<5|handout:0>[height=3.8cm]{img/home2g.pdf}
	\end{center} \vspace{-11.5pt}

	\begin{block}{}
		\begin{itemize}
			\item nearby people only:~ $L \Rightarrow (\SI A)$ \\
			\superlight{$\longrightarrow$ if I am an high level stereo, \underline{every neighbour} agrees on lowering} \pause\pause
			\item farther away people: $L \Rightarrow (\SG A)$ \\
			\superlight{$\longrightarrow$ if I am an high level stereo, \underline{everybody} agrees on lowering}
		\end{itemize}
	\end{block}
\end{frame}


\slidecount{}
\begin{frame}{Sample Emergency Applications}
	\begin{block}{Can reach the internet through non-busy devices: $\neg B \SR I$}
		\begin{itemize}
			\item $B$ ({\color{red} red}) true on \highlight{busy} devices
			\item $I$ ({\color{blue} blue}) true if device has \highlight{internet} connection
		\end{itemize}
	\end{block} \pause

	\begin{center}
		\includegraphics<1|handout:0>[height=3.8cm]{img/home3.pdf}
		\includegraphics<2>[height=3.8cm]{img/home4.pdf}
	\end{center} \vspace{-11.5pt}

	\begin{block}{Dangerous areas are surrounded by devices who can reach safely a base:}
		\begin{center}
			\highlight{$D \Rightarrow (D \SU (\neg D \SR B))$}
		\end{center} \vspace{-11pt}
		\begin{itemize}
			\item $D$ ({\color{red} red}) true on \highlight{dangerous} areas
			\item $B$ ({\color{blue} blue}) true if device is on a \highlight{base}
		\end{itemize}
	\end{block}
\end{frame}

\subsection{SLCS in Field Calculus}

\slidecount{}
\begin{frame}[fragile]{SLCS Translation in Field Calculus}
	\begin{block}{}
		\begin{tabular}{@{\hspace{6mm}}c@{\hspace{6mm}}|@{\hspace{2mm}}l@{\hspace{10mm}}|@{\hspace{2mm}}c@{\hspace{2mm}}|@{\hspace{2mm}}l@{\hspace{10mm}}}
			$\top$ & \lstinline|true| &
			$\formula_1 \vee \formula_2$ & \lstinline!F1 || F2! \\
			$\bot$ & \lstinline|false| &
			$\formula_1 \wedge \formula_2$ & \lstinline!F1 && F2! \\
			$\prop$ & \lstinline|q()| &
			$\formula_1 \Rightarrow \formula_2$ & \lstinline!F1 <= F2! \\
			$\neg \formula$ & \lstinline|!F| &
			$\formula_1 \Leftrightarrow \formula_2$ & \lstinline!F1 == F2! \\
			$\SI \formula$ & \lstinline!allHood(nbr{F})! &
			$\SC \formula$ & \lstinline!anyHood(nbr{F})! \\
			$\formula_1 \SR \formula_2$ & \multicolumn{3}{l}{\lstinline!if (F1) {somewhere(F2)} {false}!} \\
		\end{tabular}
	\end{block} \pause
	\smallskip

	\begin{block}{}
		\begin{itemize}
			\item \lstinline|somewhere(F)| if \lstinline|F| holds in some \highlight{reachable device} computing the function \pause
			\begin{lstlisting}
def somewhere(F) { BIS(F, V, R) < D }
			\end{lstlisting}
			\vspace{-7pt}
			\begin{itemize}
				\item \lstinline|BIS|\ssymbol{1} computes (optimally) \highlight{distance} from closest device where \lstinline|F| holds \\
				\superlight{$\longrightarrow$ optimally $\neq$ exact: cannot know things instantaneously}
				\onslide<1-2,4->{\item \lstinline|V| is the average \highlight{information speed} in the network (algorithm parameter)}
				\onslide<1-2,4->{\item \lstinline|R| is the device \highlight{communication range} (algorithm parameter)}
				\onslide<1-2,5->{\item \lstinline|D| is the network \highlight{diameter} \superlight{$\longrightarrow$ if closest \lstinline|F| is farther, it doesn't exist}}
			\end{itemize}
		\end{itemize}
		\vspace{5pt}
		\scriptsize
		\CITE{1}{G Audrito, F Damiani, M Viroli}{Optimal Single-Path Information Propagation in Gradient-based Algorithms}{Science of Computer Programming 166, 2018}{10.1016/j.scico.2018.06.002}
	\end{block}
\end{frame}

\slidecount{}
\begin{frame}{SLCS Translation in Field Calculus}
	\begin{block}{The translation $\PROGRAM$ of a formula $\formula$ is:}
		\begin{itemize}
			\item \highlight{efficient:} resources linear in (formula length) $\times$ (neighbourhood size)
			\onslide<2->{\item \highlight{self-stabilising:} if network stops changing, converges to the interpretation of $\formula$}
			\onslide<3->{\item \highlight{reactive:} follows changes with a speed proportional to the optimal one} \\
			\onslide<4->{\superlight{$\longrightarrow$ provided that parameters are correct}}
		\end{itemize}
	\end{block}

	\begin{tikzpicture}
		\fill[white,rounded corners] (-0.4\textwidth,-0.7cm) rectangle (0.4\textwidth,2.3cm);
		\fill[orange,rounded corners] (-3.9,0.2cm) rectangle (-0.9,1.4cm);
		\fill[palegray,rounded corners] (5.7,0.2cm) rectangle (0.9,1.4cm);
		\node at (-2.4,0.8cm) {\textcolor{white}{$\neg D \vee (O \Leftrightarrow \SF P)$}};
		\node at (0,0.75cm) {\huge$\rightarrow$};
		\node at (3.3,0.8cm) {\lstinline/!D || (O == somewhere(P))/};
	\end{tikzpicture}

	\onslide<5->{\begin{block}{The V, R, D parameters involved in the translation may be:}
		\begin{itemize}
			\item estimated at system design (most of the times)
			\item computed by other field calculus programs (worst case)
		\end{itemize}
	\end{block}}
\end{frame}

\slidecount{}
\begin{frame}[fragile]{Parameter estimation}
	\begin{block}{}
\begin{lstlisting}
def gossipMax(v) {
  share (-infinity) { (old) => max(maxHood(old), v) }
}
def R() {
  gossipMax(maxHood(nbrDist()))
}
def V() {
  let count = share ([0,0]) { (old) =>
    [fst(old) + sumHood(nbrDist()), snd(old) + sumHood(nbrLag())]
  } in fst(count) / snd(count)
}
def D(leader) {
  2*gossipMax(BIS(leader, V(), R()))
}
\end{lstlisting}
	\end{block}
	\smallskip

	\begin{center}
		\lstinline|leader| is any node that is connected to the network at all times
	\end{center}
\end{frame}

\end{document}
