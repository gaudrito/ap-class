unitsize(1cm);
import math;
import roundedpath;
settings.tex = "pdflatex";
texpreamble("\renewcommand{\familydefault}{\sfdefault}");

///////////////////////
// GENERAL FUNCTIONS //
///////////////////////

// square of real number
real sqr(real x) {
    return x*x;
}

///////////////////////

// given point on circle
pair gpoc(real r, real deg) {
    return r*dir(deg);
}
pair gpoc(pair c, real r, real deg) {
    return c + gpoc(r, deg);
}

// given point on ellipse
pair gpoe(pair l, real deg) {
    pair v = dir(deg);
    return v / sqrt(sqr(v.x/l.x) + sqr(v.y/l.y));
}
pair gpoe(pair a, pair b, real deg) {
    pair c = (b+a)/2;
    pair l = (b-a)/2;
    return c + gpoe(l, deg);
}

// given point on rectangle
pair gpor(pair l, real deg) {
    pair v = dir(deg);
    return v / max(abs(v.x/l.x), abs(v.y/l.y));
}
pair gpor(pair a, pair b, real deg) {
    pair c = (a+b)/2;
    pair l = (b-a)/2;
    return c + gpor(l, deg);
}

// given point on square
pair gpos(real r, real deg) {
    return gpor((r,r), deg);
}
pair gpos(pair c, real r, real deg) {
    return c + gpos(r, deg);
}

///////////////////////

// random point on circle
pair rpoc(real r) {
    return r*dir(unitrand()*360);
}
pair rpoc(pair c, real r) {
    return c + rpoc(r);
}

// random point on ellipse
pair rpoe(pair l) {
    path e = ellipse((0,0), l.x, l.y);
    return arcpoint(e, unitrand() * arclength(e));
}
pair rpoe(pair a, pair b) {
    pair c = (b+a)/2;
    pair l = (b-a)/2;
    return c + rpoe(l);
}

// random point on rectangle
pair rpor(pair l) {
    if (unitrand()*(l.x+l.y) < l.x)
        return (l.x*(2*unitrand()-1), unitrand()<.5 ? l.y : -l.y);
    else
        return (unitrand()<.5 ? l.x : -l.x, (2*unitrand()-1)*l.y);
}
pair rpor(pair a, pair b) {
    pair c = (b+a)/2;
    pair l = (b-a)/2;
    return c + rpor(l);
}

// random point on square
pair rpos(real r) {
    return rpor((r,r));
}
pair rpos(pair c, real r) {
    return c + rpos(r);
}

///////////////////////

// random point within circle
pair rpwc(real r) {
    return sqrt(unitrand())*rpoc(r);
}
pair rpwc(pair c, real r) {
    return c + rpwc(r);
}

// random point within ellipse
pair rpwe(pair l) {
    return sqrt(unitrand())*rpoe(l);
}
pair rpwe(pair a, pair b) {
    pair c = (b+a)/2;
    pair l = (b-a)/2;
    return c + rpwe(l);
}

// random point within rectangle
pair rpwr(pair l) {
    return (l.x*(2*unitrand()-1), (2*unitrand()-1)*l.y);
}
pair rpwr(pair a, pair b) {
    pair c = (b+a)/2;
    pair l = (b-a)/2;
    return c + rpwr(l);
}

// random point within square
pair rpws(real r) {
    return rpwr((r,r));
}
pair rpws(pair c, real r) {
    return c + rpws(r);
}

///////////////////////

real ROFFS = 0.05; // randomization factor

// randomized rectangle
path rect(pair a, pair b, real r = ROFFS) {
    real d = min(b.x-a.x, b.y-a.y) * r;
    return rpws(a, d) -- rpws((a.x, b.y), d) -- rpws(b, d) -- rpws((b.x, a.y), d) -- cycle;
}

// randomized rounded rectangle
path round(pair a, pair b, real r = ROFFS) {
    real d = 2 * min(b.x-a.x, b.y-a.y) * r;
    return roundedpath(rect(a, b, r), d);
}

// randomized ellipse
path ellip(pair a, pair b, real r = ROFFS) {
    real d = min(b.x-a.x, b.y-a.y) * r;
    a = rpws(a, d);
    b = rpws(b, d);
    pair c = (a+b)/2;
    pair l = (b-a)/2;
    return shift(c)*rotate(90*unitrand()*r)*scale(l.x,l.y)*unitcircle;
}

// applies style to one-line text
int NSTYLE = 0; // normal style
int BSTYLE = 1; // boldface style
int ISTYLE = 2; // italic style
int TSTYLE = 3; // true-type style
string[] STYLES = {"", "\textbf", "\textit", "\texttt"};
string style(string txt, int s) {
    return "\phantom{d}" + STYLES[s] + "{" + txt + "}\phantom{p}";
}

// multi-row label in box
void multilabel(picture pic = currentpicture, real size = 1, string[] txt, pair a, pair b, pen p = black) {
    real x = (a.x+b.x)/2;
    real dy = (a.y-b.y)/(txt.length+1);
    for (int i=0; i<txt.length; ++i)
        label(pic, scale(size)*txt[i], (x, b.y + (i+1)*dy), p);
}

///////////////////////
// SPECIAL FUNCTIONS //
///////////////////////

pair SIZE = (2.5, 1); // half-size of the boxes
real DSEP = 0.95; // separation between contiguous numbers
int CUR = -1; // current lecture

void lecture(picture pic = currentpicture, pair c, int n, int m=n, string[] txt, pen front) {
    path p = round(c-SIZE, c+SIZE);
    filldraw(pic, p, 0.95*white + 0.05*front, front+2);
    for (int i=0; i<txt.length; ++i)
        txt[i] = (i+1==txt.length && substr(txt[i], length(txt[i])-1)=="]") ? style("\scriptsize "+txt[i], BSTYLE) : style(txt[i], NSTYLE);
    multilabel(pic, 1, txt, c-SIZE, c+SIZE, front);
    real start = SIZE.y - (m-n)*DSEP/2; // unitrand()*(2*SIZE.x+4*SIZE.y-(m-n)*DSEP) -SIZE.y;
    for (int i=n; i<=m; ++i) {
        pair cnum = arcpoint(p, start + (i-n)*DSEP);
        filldraw(pic, circle(cnum, 0.3) , 0.4*white + 0.6*front, front+2);
        label(pic, scale(1)*style(string(i), TSTYLE), cnum, white);
    }
}

pair edge(pair c, pair dir) {
    return c + scale(SIZE.x, SIZE.y)*gpos(1, degrees(dir));
}

void connect(picture pic = currentpicture, pair start, pair end, pen p = solid) {
    pair d = unit(start-end) * 0.1;
    start += d;
    end += d;
    draw(pic, start -- end, black+1+p, EndArrow(10));
}

///////////////////////
//    MAIN   CODE    //
///////////////////////

pair[] P = {
    (+1,+1),
    
    (-8,-1),
    (-8,-7.5),
    (-8,-10),
    
    (-2,-1.5),
    (-2,-4),
    (-2,-6.5),
    (-2,-11.25),

    (+4,-8.75),
    (+4,-11.75),

    (+10,-3),
    (+10,-10.5),
};

int[][] parents = {
    {},
    
    {0},
    {1},
    {2, -8},
    
    {1},
    {4},
    {5},
    {8},
    
    {4,-2},
    {8},
    
    {4},
    {10,8},
};

string[][] txt = {
    {"INTRODUCTION", "[aggregate,survey]"},
    
    {"GENERAL", "MODEL", "[universality]"},
    {"LIMIT", "MODELS", "[self-stabilisation,consistency]"},
    {"GUARANTEES", "[real-time,stability]"},
    
    {"CALCULUS", "[calculus,share]"},
    {"SEMANTICS", "[calculus,share]"},
    {"SPACE-TIME", "PROCESSES", "[processes]"},
    {"SPACE-TIME", "MONITORING", "[time-monitor,space-monitor]"},
    
    {"SPATIAL", "ALGORITHMS", "[self-stabilisation]"},
    {"ALGORITHMIC", "SEMINAR", "PAPERS"},
    
    {"SIMULATION", "BASICS", "[fcpp,fcpp-gui]"},
    {"SIMULATION", "EXERCISES"},
};

int[] num = {
    1,
    2,6,9,
    3,5,11,10,
    7,12,
    4,8,
};
int[] endnum = {
    1,
    2,6,9,
    3,5,11,10,
    7,15,
    4,8,
};

bool[] enabled;

void enable(int i) {
    if (enabled[i]) return;
    enabled[i] = true;
    for (int j : parents[i])
        enable(abs(j));
}

void map(picture pic = currentpicture) {
    srand(7);
    for (int i=0; i<parents.length; ++i)
        enabled[i] = CUR == -1;
    if (CUR >= 0) enable(CUR);
    for (int i=0; i<parents.length; ++i) {
        for (int k : parents[i]) {
            int j = abs(k);
            pen p = (k < 0 ? dotted : solid) + (enabled[i] ? black : mediumgray);
            
            if (i==1)
                connect(pic, edge(P[j], W), edge(P[i], NE), p);
            else if (i==7)
                connect(pic, edge(P[j], SW), edge(P[i], NE), p);
            else if (i==8 && j==4)
                connect(pic, edge(P[j], SE), edge(P[i], N), p);
            else if (i==11 && j==8)
                connect(pic, edge(P[j], SE), edge(P[i], W), p);
            else if (P[j].y-P[i].y < 1.6)
                connect(pic, edge(P[j], P[i].x > P[j].x ? E : W), edge(P[i], P[i].x > P[j].x ? W : E), p);
            else
                connect(pic, edge(P[j], S), edge(P[i], N), p);
        }
    }
    for (int i=0; i<parents.length; ++i)
        lecture(pic, P[i], num[i], endnum[i], txt[i], i==CUR ? deepred : enabled[i] ? black : mediumgray);
}

for (CUR=0; CUR<parents.length; ++CUR) {
    picture pic;
    unitsize(pic, 1cm);
    map(pic);
    shipout("map" + string(CUR), pic);
}

CUR = -1;
map();
shipout("Conceptual Map");
