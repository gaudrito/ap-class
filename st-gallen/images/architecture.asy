unitsize(1cm);
settings.tex = "pdflatex";
import roundedpath;

real STROKE = 2;

transform scale(pair s) {
    return scale(s.x, s.y);
}

pair rv() {
    return dir(unitrand()*360)*0.03;
}

path rect(pair s, pair l) {
    return roundedpath(s+rv() -- (s.x,l.y)+rv() -- l+rv() -- (l.x,s.y)+rv() -- cycle, 0.2);
}

path bigarrow() {
    return (-1,1) -- (1,1) -- (1,2) -- (3,0) -- (1,-2) -- (1,-1) -- (-1,-1) -- cycle;
}

void box(picture pic=currentpicture, string text, pair c, pair l, pen p, real k = 2.6, bool center = true) {
    pen pale = k == 2.0 ? white : p + white + white + white;
    pen deep = (k == 2.0 ? 0.3 : 0.5)*p;
    filldraw(pic, rect(c-l,c+l), pale, deep+1.5*STROKE);
    string tt = "\texttt{\phantom{d}"+text+"\phantom{p}}";
    if (center)
        label(pic, scale(k)*tt, c);
    else
        label(pic, scale(k)*tt, align=E, c-(l.x-0.05,0));
}

pair textsize(string text) {
    return (0.4+length(text)*0.2,0.45);
}

void round(picture pic=currentpicture, string text, pair c, pen p) {
    pair l = textsize(text);
    pen pale = p + white + white + white;
    pen deep = 0.5*p;
    filldraw(pic, shift(c)*scale(l)*unitcircle, pale, deep+1.5*STROKE);
    string tt = "\texttt{\phantom{d}"+text+"\phantom{p}}";
    label(pic, scale(1.8)*tt, c);
}

pair arcdir(pair l, pair v) {
    real k = ((v.x/l.x)^2 + (v.y/l.y)^2)^(-0.5);
    return k*v;
}

void connect(picture pic=currentpicture, string ss, pair sc, string es, pair ec, pen p = solid, guide waypoints = nullpath) {
    pair ls = textsize(ss) + (0.1,0.1);
    pair le = textsize(es) + (0.1,0.1);
    if (waypoints == nullpath) {
        sc += arcdir(ls, ec - sc);
        ec -= arcdir(le, ec - sc);
    } else {
        sc += arcdir(ls, point(waypoints, 0) - sc);
        ec -= arcdir(le, ec - point(waypoints, length(waypoints)));
    }
    draw(pic, sc :: waypoints :: ec, black+2+p, EndArrow(10));
}

void components(picture pic=currentpicture, pair c, pair s, bool arrows = true) {
    transform t = shift(c)*scale(s);
    box(pic, "", t*(0, -3), scale(s)*(10, 4), blue);
    box(pic, "\textrm{components}", t*(-5, 1), xscale(s.x)*(3, 0.4), blue, 2.0);
    filldraw(pic, shift(t*(-8,1))*scale(0.7)*unitcircle, palegray, (0.3*blue)+1.5*STROKE);
    label(pic, scale(2.5)*"$2$", t*(-8,1));
    string[] bs;
    pair[]   bc;
    pen[]    bp;
    void block(string ns, pair nc, pen np = cyan) {
        bs[bs.length] = ns;
        bc[bc.length] = t*nc;
        bp[bp.length] = np;
    }
    pen median = 0.5*cyan + 0.5*magenta;
    block("calculus",   (-7.5,-0.6), cyan);     // 0
    block("positioner", (-7.2,-2.3), median);   // 1
    block("connector",  (-6.5,-4.8), median);   // 2
    block("randomizer", (-3.1,-1.1), cyan);     // 3
    block("scheduler",  (-2.3,-3.0), cyan);     // 4
    block("timer",      (+1.9,-4.1), cyan);     // 5
    block("storage",    (+0.3,-0.1), cyan);     // 6
    block("logger",     (+3.0,-2.3), median);   // 7
    block("identifier", (+7.2,-0.5), median);   // 8
    block("spawner",    (+6.7,-3.8), median);   // 9
    block("displayer",  (+0.6,-6.0), magenta);  // 10
    for (int i=0; i<bc.length; ++i)
        round(pic, bs[i], bc[i], bp[i]);
    void link(int is, int ie, pen p = solid, guide w = nullpath) {
        connect(pic, bs[is], bc[is], bs[ie], bc[ie], p, w);
    }
    pen dots = gray(0.35) + linetype(new real[] {0,2});
    link(2, 1);
    link(2, 3, dots);
    link(2, 5, dots);
    link(4, 3, dots);
    link(5, 4, dots);
    link(5, 7, dots);
    link(5, 8, dots,        t*(4.2,-3.0));
    link(5, 9, dots);
    link(7, 3, dots);
    link(7, 6);
    link(7, 8, dots);
    link(9, 3, dots,        t*(1.5,-3.2));
    link(9, 8);
    link(10,1, 0.7*magenta);
    link(10,5, 0.7*magenta);
    link(10,6, 0.7*magenta, t*(0.3,-3.5));
    link(10,8, 0.7*magenta, t*(4.9,-3.3));
    if (arrows) {
        filldraw(shift(t*(10,-2)+(1.5,0))*scale(0.4)*bigarrow(), palegray, black+STROKE);
        filldraw(shift(t*(10,-4)+(1.5,0))*scale(0.4)*bigarrow(), palegray, black+STROKE);
    }
    box(pic, "node", t*(10,-2), 1.2*textsize("node"), red);
    box(pic, "net",  t*(10,-4), 1.2*textsize("net"),  red);
}

void aggregate(picture pic=currentpicture, pair c, pair s) {
    transform t = shift(c)*scale(s);
    box(pic, "", t*(0, -3), scale(s)*(7, 4), green);
    box(pic, "\textrm{aggregate functions}", t*(-2.5, 1), xscale(s.x)*(4.5, 0.4), green, 2.0);
    filldraw(pic, shift(t*(-7,1))*scale(0.7)*unitcircle, palegray, (0.3*green)+1.5*STROKE);
    label(pic, scale(2.5)*"$3$", t*(-7,1));
    
    void arrows(real y) {
        filldraw(pic, shift(t*(-3.4,y))*shift(0,-0.2)*scale(0.25)*rotate(90)*bigarrow(), palegray, black+STROKE);
        filldraw(pic, shift(t*(+4.6,y))*shift(0,-0.2)*scale(0.25)*rotate(90)*bigarrow(), palegray, black+STROKE);
    }
    box(pic, "\textrm{applications}",    t*(0.6,-0.5), scale(s)*(6.0,0.7), 1.0*cyan + 0.0*magenta, 1.8, false);
    arrows(-1.4);
    box(pic, "\textrm{libraries}",       t*(0.6,-2.3), scale(s)*(6.0,0.7), 0.6*cyan + 0.3*magenta, 1.8, false);
    label(pic, scale(0.8)*"\texttt{dispersion}", t*(0.5,-2.0));
    label(pic, scale(1.2)*"\texttt{tracker}",    t*(4.2,-2.2));
    label(pic, scale(0.4)*"\texttt{allocator}",  t*(2.5,-2.5));
    arrows(-3.2);
    box(pic, "\textrm{building blocks}", t*(0.6,-4.1), scale(s)*(6.0,0.7), 0.3*cyan + 0.6*magenta, 1.8, false);
    label(pic, scale(1.0)*"\texttt{distance}",   t*(1.4,-3.9));
    label(pic, scale(1.3)*"\texttt{collection}", t*(3.9,-3.8));
    label(pic, scale(0.7)*"\texttt{election}",   t*(2.5,-4.4));
    label(pic, scale(0.4)*"\texttt{partition}",  t*(5.7,-4.2));
    arrows(-5.0);
    box(pic, "\textrm{built-ins}",       t*(0.6,-5.9), scale(s)*(6.0,0.7), 0.0*cyan + 1.0*magenta, 1.8, false);
    label(pic, scale(0.9)*"\texttt{nbr}",        t*(-0.8,-6.0));
    label(pic, scale(1.3)*"\texttt{fold\_hood}", t*(1.5,-5.6));
    label(pic, scale(1.1)*"\texttt{old}",        t*(3.2,-6.2));
    label(pic, scale(0.7)*"\texttt{map\_hood}",  t*(5.5,-6.0));
    label(pic, scale(0.5)*"\texttt{self}",       t*(4.4,-5.7));
}

void structures(picture pic=currentpicture, pair c, pair s) {
    transform t = shift(c)*scale(s);
    
    filldraw(pic, shift(t*(-9.5,1))*shift(0,-0.3)*scale(0.6)*rotate(90)*bigarrow(), palegray, black+STROKE);
    filldraw(pic, shift(t*(+9.5,1))*shift(0,-0.3)*scale(0.6)*rotate(90)*bigarrow(), palegray, black+STROKE);
    box(pic, "", t*(0, -0.5), scale(s)*(18, 1.5), red);
    box(pic, "\textrm{data structures}", t*(0, -2), xscale(s.x)*(4, 0.4), red, 2.0);
    filldraw(pic, shift(t*(-4,-2))*scale(0.7)*unitcircle, palegray, (0.3*red)+1.5*STROKE);
    label(pic, scale(2.5)*"$1$", t*(-4,-2));
    label(pic, scale(1.4)*"\texttt{vec<n>}",     t*( 5.5,+0.1));
    label(pic, scale(2.0)*"\texttt{field<T>}",   t*(11.5,-0.2));
    label(pic, scale(1.2)*"\texttt{array<T,n>}", t*(15.5,-1.4));
    label(pic, scale(1.7)*"\texttt{tuple<T...>}",t*( 7.5,-1.1));
    
    label(pic, scale(1.9)*"\texttt{trace}",              t*(-9,-0.5));
    label(pic, scale(1.7)*"\texttt{distribution}",       t*(-13,-0.0));
    label(pic, scale(1.5)*"\texttt{aggregator}",         t*(-3,-0.2));
    label(pic, scale(1.3)*"\texttt{tagged\_tuple}",      t*(-16, -1.1));
    label(pic, scale(1.1)*"\texttt{multitype\_map}",     t*(-6, -1.0));
    label(pic, scale(0.9)*"\texttt{context}",            t*(-11, -1.3));
    label(pic, scale(1.6)*"\texttt{...}",t*(-0.5, -0.7));
}

picture comp;
unitsize(comp, 1cm);
components(comp, (0,0), (1,1));
shipout("component-hierarchy", comp);

aggregate((18.7,0), (1,1));
components((0,0), (1,1));
structures((7.85,-8.8), (1,1));
shipout("architecture");
