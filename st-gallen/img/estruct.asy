unitsize(1cm);
include roundedpath;
include math;
settings.tex = "pdflatex";

string[] devs = {"desktop", "laptop", "tablet", "smartphone", "arduino", "camera", "smokedetector", "thermostat"};
for (int i=0; i<devs.length; ++i) devs[i] = graphic("dev-" + devs[i] + ".png");

real luminance(pen p) {
    real[] c = colors(rgb(p));
    return sqrt( 0.299*c[0]^2 + 0.587*c[1]^2 + 0.114*c[2]^2 );
}

pen darkest(pen a, pen b) {
    if (a == b) return a;
    if (a == deepblue) return b;
    if (b == deepblue) return a;
    return black;
    return luminance(a) < luminance(b) ? a : b;
}

pen lighter(pen a) {
    return 0.9*white + 0.1*a;
}

pair getdim(real[][] E) {
    pair dim = (0, 0.8*E.length);
    for (int i=0; i<E.length; ++i)
        for (int j=0; j<E[i].length; ++j)
            dim = (max(dim.x, E[i][j]), dim.y);
    return dim+(1.2,.6);
}

void drawarrow(picture pic, pair s, pair e, pen c, string txt = "") {
    pair dir = unit(e-s);
    s = s+0.22*dir;
    e = e-0.22*dir;
    real w = 0.05;
    int n = max((int)(length(e-s)/w-3), 0);
    path p = s;
    for (int i=0; i<n; ++i) p = p -- s+(w/2+w*i)*dir+rotate(i%2==0 ? 90 : -90)*(w/2*dir);
    p = p -- s+(w*n*dir) -- e;
    draw(pic, p, c, EndArrow);
    pair m = (s+e)/2 + rotate(90) * .15 * dir;
    label(pic, rotate(degrees(dir))*scale(0.7)*("\phantom{d}" + txt + "\phantom{p}"), m);
}

void drawnode(picture pic, pair pos, string l, pen col) {
    pen fg = length(l)!=3 ? col : lighter(col);
    pen bk = length(l)!=3 ? lighter(col) : col;
    if (length(l) != 5) fill(pic, circle(pos, .19), fg);
    else {
        fill(pic, circle(pos, .18), fg);
        draw(pic, circle(pos, .20), fg);
    }
    fill(pic, circle(pos, .16), bk);
    if (length(l) > 0) label(pic, scale(0.7)*("$"+l+"$"),pos, fg);
}

bool uniq(int[] D) {
    int L = D.length-1;
    for (int i=0; i<L; ++i)
        if (D[L] == D[i]) return false;
    return true;
}

void drawgrid(picture pic, pair pos, pair dim, bool dev, real[][] E) {
    if (dev) {
        int[] D = {(int)(devs.length*unitrand())};
        for (int i=1; i<E.length; ++i) {
            D[i] = D[i-1];
            while (!uniq(D))
                D[i] = (int)(devs.length*unitrand());
        }
        draw(pic, pos -- pos+(0,dim.y-0.5), EndArrow);
        for (int i=0; i<E.length; ++i) {
            label(pic, scale(0.03)*devs[D[i]], pos+(0.2,0.8*i+0.4));
            //label(pic, scale(0.5)*string(i+1),  pos+(0.1,0.8*i+.55));
            draw(pic, pos+(0,0.8*i+.4) -- pos+(dim.x-0.5,0.8*i+.4), mediumgray+dotted);
            //draw(pic, pos+(0,0.8*i+.4) -- pos+(0.15,0.8*i+.4));
        }
        label(pic, rotate(90)*scale(0.7)*"device",pos+(-0.2,dim.y-1.5));
        label(pic,            scale(0.7)*"time",  pos+(dim.x-1.3,-0.2));
        draw(pic, pos -- pos+(dim.x-0.5,0), EndArrow);
    } else {
        //label(pic,            scale(0.7)*"past",  pos+(       .8,-0.2));
        //label(pic,            scale(0.7)*"future",pos+(dim.x-1.2,-0.2));
        //draw(pic, pos -- pos+(dim.x-0.5,0), EndArrow);
    }
}

void plot(picture pic, pair pos, bool dev, real[][] E, pen[][] C, string[][] V, int[][][] G) {
    pair dim = getdim(E);
    drawgrid(pic, pos, dim, dev, E);
    pair[][] Ec;

    for (int i=0; i<E.length; ++i) {
        Ec[i] = new pair[];
        for (int j=0; j<E[i].length; ++j)
            Ec[i][j] = pos+(E[i][j],0.8*i+(dev ? .4 : 0.8*unitrand()));
    }
    for (int i=0; i<E.length; ++i) {
        for (int j=0; j<E[i].length; ++j) {
            drawnode(pic, Ec[i][j], V[i][j], C[i][j]);
            if (G.length > 0) {
                int x = 0;
                for (int k=0; k<E.length; ++k) if (G[i][j][k] > 0) {
                    for (int h=0; h+G[i][j][k]<=E[k].length; ++h)
                        if (E[k][h+G[i][j][k]-1] < E[i][j] && (h+G[i][j][k] == E[k].length || E[k][h+G[i][j][k]] >= E[i][j]))
                            drawarrow(pic, Ec[k][h], Ec[i][j], darkest(C[k][h], C[i][j]));
                    ++x;
                }
            }
        }
    }
}

int SEED = 5;

real[][]   Events = {
    {1.4,2.5,3.4,4.5,5.4},
    {0.7,1.5,3.3,4.0,5.35,6.2},
    {2.6,3.8,4.5,5.7},
    {1.0,1.8,3.0,4.0,5.0,6.2},
    {1.7,3.7,5.9}
};
int[][][]  Graph  = {
    {{0,0,0,0,0},{1,1,0,0,0},{1,2,1,0,0},{1,1,0,0,0},{1,2,0,0,0}},
    {{0,0,0,0,0},{1,1,0,0,0},{0,0,0,0,0},{1,1,1,0,0},{2,1,1,0,0},{1,1,1,0,0}},
    {{0,0,0,0,0},{0,1,1,1,0},{0,1,1,1,0},{0,1,1,1,0}},
    {{0,0,0,0,0},{0,2,0,1,0},{0,1,1,1,1},{0,0,2,1,2},{0,0,1,1,1},{0,0,1,1,0}},
    {{0,0,0,1,0},{0,0,0,0,1},{0,0,0,1,1}}
};

picture main(bool dev, pen[][] colors, string[][] contents) {
    DefaultHead.size=new real(pen p=currentpen) {return 1.2mm;};
    srand(SEED);
    picture pic;
    unitsize(pic, 1cm);
    plot(pic, (0,0), dev, Events, colors, contents, Graph);
    return pic;
}

pen[][]    colors;
string[][] contents;

colors = new pen[][]{
    {deepgray,deepgray,deepgray,deepgray,deepgray},
    {deepgray,deepgray,deepgray,deepgray,deepgray,deepgray},
    {deepgray,deepgray,deepgray,deepgray},
    {deepgray,deepgray,deepgray,deepgray,deepgray,deepgray},
    {deepgray,deepgray,deepgray}
};
contents = new string[][]{
    {"v","c","w","t","o"},
    {"p","l","r","u","s","d"},
    {"q","e","v","o"},
    {"p","m","j","b","q","o"},
    {"n","b","u"}
};
shipout("structure", main(false, colors, contents));
colors = new pen[][]{
    {brown,black,black,heavygreen,heavygreen},
    {brown,brown,brown,heavygreen,heavygreen,heavygreen},
    {brown,deepblue,heavygreen,heavygreen},
    {brown,brown,brown,black,heavygreen,heavygreen},
    {brown,black,heavygreen}
};
contents = new string[][]{
    {"0"," 2 "," 3 ","5","6"},
    {"0","1","0","4","6","8"},
    {"0","3","5","7"},
    {"0","1","2"," 3 ","6","8"},
    {"1"," 2 ","7"}
};
//shipout("estruct_clock", main(true, colors, contents));
contents = new string[][]{
    {"8"," 8 "," 8 ","8","8"},
    {"6","6","3","3","3","3"},
    {"4","  4  ","4","4"},
    {"7","7","7"," 7 ","7","7"},
    {"5"," 5 ","5"}
};
shipout("estruct_input", main(true, colors, contents));
contents = new string[][]{
    {"8"," 6 "," 4 ","3","3"},
    {"6","6","3","3","3","3"},
    {"4","  3  ","3","3"},
    {"7","6","4"," 4 ","3","3"},
    {"5"," 5 ","3"}
};
picture pic = main(true, colors, contents);
shipout("estruct_gossip", pic);

// aggiungi scritta "message" / "state" con riquadro
drawarrow(pic, (Events[1][1], .8*1+.4), (Events[3][2], .8*3+.4),  brown, "message");
drawarrow(pic, (Events[4][1], .8*4+.4), (Events[4][2], .8*4+.4),  black, "state");
label(pic, scale(0.7)*("\phantom{d}reboot\phantom{p}"), ((Events[1][1], .8*1+.4) + (Events[1][2], .8*1+.4))/2);

shipout("estruct_gossip_labels", pic);

Events = new real[][]{
    {1.0,2.1,3.2,4.3,5.4},
    {1.0,2.1,3.2,4.3,5.4},
    {1.0,2.1,3.2,4.3,5.4},
    {1.0,2.1,3.2,4.3,5.4},
    {1.0,2.1,3.2,4.3,5.4},
};
Graph = new int[][][]{
    {{0,0,0,0,0},{1,1,0,0,0},{1,1,1,0,0},{1,1,0,0,0},{1,1,0,0,0}},
    {{0,0,0,0,0},{0,1,0,0,0},{1,1,1,0,0},{1,1,1,0,0},{0,1,1,0,0}},
    {{0,0,0,0,0},{0,0,1,0,0},{0,1,1,1,0},{0,0,1,1,0},{0,1,1,0,1}},
    {{0,0,0,0,0},{0,1,1,1,1},{0,0,1,1,1},{0,0,1,1,0},{0,0,1,1,0}},
    {{0,0,0,0,0},{0,0,0,0,1},{0,0,0,0,1},{0,0,0,1,1},{0,0,0,1,1}}
};
colors = new pen[][]{
    {black,black,black,black,heavygreen},
    {brown,brown,black,heavygreen,heavygreen},
    {brown,brown,deepblue,heavygreen,heavygreen},
    {brown,brown,black,heavygreen,heavygreen},
    {brown,brown,black,black,heavygreen}
};
contents = new string[][]{
    {" 8 "," 6 "," 4 "," 4 ","3"},
    {"6","6"," 4 ","3","3"},
    {"4","""4","  3  ","3","3"},
    {"6","4"," 4 ","3","3"},
    {"5","5"," 5 "," 4 ","3"}
};
shipout("estruct_synch", main(true, colors, contents));
