\documentclass[10pt]{beamer} %,aspectratio=169,handout
	\usetheme{CambridgeUS} %CambridgeUS, Goettingen, Singapore, Madrid
	\setbeamertemplate{footline}[frame number]
	\setbeamercovered{transparent}
	\beamertemplatenavigationsymbolsempty

\usepackage{multimedia}
\usepackage{tabularx}
\usepackage{listings}
\usepackage{forloop}
\usepackage{mathtools}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{wasysym}
\newcolumntype{M}[1]{>{\centering\arraybackslash}m{#1}}
\setlength\tabcolsep{4pt}

\input{../macros}

\newcounter{prog}
\newcommand{\progress}[2]{$\forloop{prog}{0}{\value{prog} < #1}{\bullet}\forloop{prog}{0}{\value{prog} < #2}{\circ}$}
\newcounter{secslide}
\newcounter{sectotal}
\newcounter{secother}
\newcommand{\resetsec}[1]{\setcounter{secslide}{0}\setcounter{sectotal}{#1}}
\newcommand{\slidecount}{\addtocounter{secslide}{1}\setcounter{secother}{\value{sectotal} - \value{secslide}}\subsection[\progress{\value{secslide}}{\value{secother}}]{slide}}

\lstdefinelanguage{hfc}{
	basicstyle=\lst@ifdisplaystyle\small\fi\ttfamily,
	frame=single,
	basewidth=0.5em,
	sensitive=true,
	morestring=[b]",
	morecomment=[l]{//},
	morecomment=[n]{/*}{*/},
	commentstyle=\color{gray},
	keywordstyle=\color{blue}\textbf, keywords={def}, otherkeywords={=>},
	keywordstyle=[2]\color{red}\textbf, keywords=[2]{rep,nbr,share,if,let,in},
	keywordstyle=[3]\color{violet}, keywords=[3]{mux,sumHood,countHood,everyHood,anyHood,minHood,maxHood,foldHood,maxHoodPlusSelf,nbrDist,snd},
	keywordstyle=[4]\color{orange}\textbf, keywords=[4]{spawn},
	keywordstyle=[5]\color{blue}, keywords=[5]{false,true,infinity}
}

\definecolor{darkgreen}{rgb}{0.0, 0.5, 0.0}
\definecolor{darkred}{rgb}{0.5, 0.0, 0.0}
\definecolor{darkblue}{rgb}{0.2, 0.2, 0.7}

\newcommand{\highlight}[1]{\textcolor{darkblue}{#1}}
\newcommand{\superlight}[1]{\textcolor{darkred}{#1}}
\newcommand{\greenlight}[1]{\textcolor{darkgreen}{#1}}
\newcommand{\DOI}[1]{\href{http://www.doi.org/#1}{\underline{#1}}}
\newcommand{\ACMDOI}[1]{\href{https://dl.acm.org/doi/#1}{\underline{#1}}}
\newcommand{\CITE}[5]{\ssymbol{#1}[#2. \emph{#3.} #4. \DOI{#5}]}

\theoremstyle{plain}
\newtheorem{proposition}[theorem]{Proposition}
\theoremstyle{remark}
\newtheorem{question}[theorem]{Question}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{Aggregate Programming for the Internet of Things}
\author{Giorgio Audrito, Ferruccio Damiani}
\institute{{\small Dipartimento di Informatica\\[3pt] Universit\`a degli Studi di Torino}}
%\date{}
\titlegraphic{
	%\raisebox{-0.5\height}{\includegraphics[width=2.0cm]{../images/logo_horizon.jpg}} \quad
	\raisebox{-0.5\height}{\includegraphics[width=2.0cm]{../images/logo_torino.pdf}} \quad
	%\raisebox{-0.5\height}{\includegraphics[width=2.0cm]{../images/logo_sanpaolo.png}}
}

\begin{document}

	\begin{frame}
		\titlepage
	\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Graph Statistics}

	\begin{frame}{Outline}
		\begin{itemize}
			\item \textcolor{darkblue}{Graph Statistics}
			\begin{itemize}
				\item Graph Algorithms
				\item Neighbourhood Function
				\item HyperLogLog Counters
			\end{itemize}
			\bigskip
			\item Aggregate HyperANF
		\end{itemize}
	\end{frame}


\subsection{Graph Algorithms}

	\begin{frame}{Graph Algorithms}
		\begin{block}{In the IoT}
			Naturally arise from the underlying network structure: \pause
			\begin{itemize}
				\item G (distance calculation) \textcolor{darkblue}{$\longleftarrow$ graph algorithm} \pause
				\item C (data summarising)  \textcolor{darkblue}{$\longleftarrow$ also a graph algorithm}
			\end{itemize}
		\end{block} \pause

		\bigskip
		\begin{block}{}
			Easy problems in classical contexts, made difficult by
			\begin{itemize}
				\item very large graphs \pause
				\item local access to information \pause
				\item dynamically changing input
			\end{itemize}
		\end{block}
	\end{frame}

	\begin{frame}{Graph Algorithms}
		\begin{block}{In data mining}
			Graphs are very common in data mining as well: \pause
			\begin{itemize}
				\item social networks
				\item the Web graph...
			\end{itemize}
		\end{block} \pause

		\medskip
		\begin{block}{Similarity of issues}
			\begin{tabular}{p{0.5\linewidth}p{0.5\linewidth}}
				\begin{itemize}
					\item very large graphs
					\onslide<1,2,4->{\item restricted access to information to allow for parallelisation}
				\end{itemize}
				&
				\begin{itemize}
					\item very large graphs
					\onslide<1,2,4->{\item local access to information} \\ ~
					\onslide<1,2,5->{\item dynamically changing input}
				\end{itemize}
			\end{tabular}
		\end{block}

		\medskip
		\onslide<6->{Classical algorithms fail also in this context!} \onslide<7>{\textcolor{darkblue}{$\longleftarrow$ use approximated methods}}
	\end{frame}


\subsection{Neighbourhood Function}

	\begin{frame}[t]{The Neighbourhood Function}
		\begin{definition}
			Let $G = \ap{V,E}$ be a graph with $n$ vertices and $m$ edges. \pause

			\smallskip
			The \textcolor{darkblue}{neighbourhood function} $N_G(v, h)$, given $v \in V$, $h \geq 0$,

			counts the number of vertices $u \in C$ which lie within distance $h$ from $v$. \pause

			\[
			N_G(v, h) = \vp{ \bp{ u \in V : ~ \dist(v,u) \leq h } }
			\]
		\end{definition} \pause

		\bigskip
		\begin{block}{A versatile statistic on graphs}
			\begin{itemize}
				\item graph similarity \pause
				\item network classification \pause
				\item robustness monitoring \pause
				\item \textcolor{darkblue}{vertex ranking}
			\end{itemize}
		\end{block}
	\end{frame}

	\begin{frame}[t]{The Neighbourhood Function}
		\begin{definition}
			Let $G = \ap{V,E}$ be a graph with $n$ vertices and $m$ edges.

			\smallskip
			The \textcolor{darkblue}{neighbourhood function} $N_G(v, h)$, given $v \in V$, $h \geq 0$,

			counts the number of vertices $u \in C$ which lie within distance $h$ from $v$.

			\[
			N_G(v, h) = \vp{ \bp{ u \in V : ~ \dist(v,u) \leq h } }
			\]
		\end{definition}

		\bigskip
		\begin{block}{Vertex ranking} \pause
			The \textcolor{darkblue}{harmonic centrality} of a vertex $v$ is
			\[
			H(v) = \sum_{h>0} N_G(v, h)/h
			\] \pause

			Why ranking by harmonic centrality instead of \textcolor{darkblue}{page rank}?
		\end{block}
	\end{frame}

	\begin{frame}{Harmonic Centrality vs Page Rank}
		\begin{block}{Top Websites by \textcolor{darkblue}{Page Rank}}
			\begin{tabular}{cccc}
				PR & Website & HC \\
				\cline{1-3}
				1 & \texttt{gmpg.org} & 23 \\
				2 & \texttt{wordpress.org} & 5 \\
				3 & \texttt{youtube.com} & 1 \\
				4 & \texttt{livejournal.com} & 98 \\
				5 & \texttt{twitter.com} & 3 \\
				6 & \texttt{en.wikipedia.org} & 2 \\
				7 & \texttt{tumblr.com} & 39 \\
				\textcolor{red}{8} & \textcolor{red}{\texttt{promodj.com}} & \textcolor{red}{56711} & \textcolor{red}{$\longleftarrow$ false positive!} \\
				9 & \texttt{google.com} & 4 \\
				10 & \texttt{networkadvertising.org} & 394 \\
			\end{tabular}
		\end{block}
	\end{frame}

	\begin{frame}{Harmonic Centrality vs Page Rank}
		\begin{block}{Top Websites by \textcolor{darkblue}{Harmonic Centrality}}
			\begin{tabular}{ccc}
				HC & Website & PR \\
				\hline
				1 & \texttt{youtube.com} & 3\\
				2 & \texttt{en.wikipedia.org} & 6\\
				3 & \texttt{twitter.com} & 5\\
				4 & \texttt{google.com} & 9\\
				5 & \texttt{wordpress.org} & 2\\
				6 & \texttt{flickr.com} & 14\\
				7 & \texttt{facebook.com} & 17\\
				8 & \texttt{apple.com} & 31\\
				9 & \texttt{vimeo.com} & 27\\
				10 & \texttt{creativecommons.org} & 20\\
			\end{tabular}

			\bigskip
			Consistent with \textcolor{darkblue}{common sense} and other metrics!
		\end{block}
	\end{frame}

	\begin{frame}[t]{The Neighbourhood Function}
		\begin{definition}
			Let $G = \ap{V,E}$ be a graph with $n$ vertices and $m$ edges.

			\smallskip
			The \textcolor{darkblue}{neighbourhood function} $N_G(v, h)$, given $v \in V$, $h \geq 0$,

			counts the number of vertices $u \in C$ which lie within distance $h$ from $v$.

			\[
			N_G(v, h) = \vp{ \bp{ u \in V : ~ \dist(v,u) \leq h } }
			\]
		\end{definition} \pause

		\bigskip
		\begin{block}{An expensive statistic on graphs}
			\begin{itemize}
				\item $O(nm)$ time in linear memory \pause
				\item $O(n^{2.38})$ time in quadratic memory \pause
				\item faster if \textcolor{darkblue}{approximated}
			\end{itemize}
		\end{block}
	\end{frame}


\subsection{HyperLogLog Counters}

	\begin{frame}{Computing $N_G$}
		\begin{block}{Idea}
			Define $\MMM_G(v, h)$ as the set of vertices within distance $h$ from $v$, \\[3pt]
			so that $N_G(v, h) = \vp{ \MMM_G(v, h) }$. \pause Then:
			\[
			\MMM_G(v, h) = \bigcup_{(vu) \in E} \MMM_G(u, h-1)
			\]
			where $\MMM_G(v, 0) = \bp{v}$.
		\end{block} \pause

		\bigskip
		\begin{block}{}
			\begin{itemize}
				\item Still inefficient if we keep sets $\MMM_G(v, h)$ explicitly \pause
				\item Efficient if sets are abstracted by \textcolor{darkblue}{statistical counters}
			\end{itemize}
		\end{block}
	\end{frame}

	\begin{frame}[t]{HyperLogLog Counters}
		\begin{block}{Idea}
			Instead of $S \subseteq \bp{1 \ldots n}$ we keep an integer in $1 \ldots \log(n)$ \onslide<2->{\textcolor{darkblue}{$\longleftarrow \log \log(n)$ bits}}
			\begin{itemize}
				\onslide<3->{\item To each $i < n$ associate a $\log(n)$-bit hash $h(i)$}
				\onslide<4->{\item Compute the number of leading zeros $c(i)$}
				\onslide<5->{\item For a set, keep the maximum $c(S) = \max_{i \in S} c(i)$}
				\onslide<6->{\item The size \textcolor{darkblue}{estimate} is $2^{c(S)}$}
			\end{itemize}

			\[
			\begin{array}{l}
				\onslide<3->{7 \longrightarrow \texttt{\only<1-3>{00}\only<4->{\textcolor{red}{00}}101}} \\
				\onslide<4->{\phantom{7 \longrightarrow ~}\textcolor{red}{\uparrow}} \\
				\onslide<4->{\phantom{7 \longrightarrow ~}\textcolor{red}{\textit{2}}} \\
			\end{array}
			\]
		\end{block}

		\begin{block}{}
			\begin{itemize}
				\onslide<7->{\item Very \textcolor{darkblue}{succinct}}
				\onslide<8->{\item Easy to compute: $c(S \cup T) = \max(c(S), c(T))$}
				\onslide<9->{\item Quite \textcolor{darkblue}{unaccurate}}
			\end{itemize}
		\end{block}
	\end{frame}

	\begin{frame}[t]{HyperLogLog Counters}
		\begin{block}{Idea}
			Instead of $S \subseteq \bp{1 \ldots n}$ we keep \textcolor{darkblue}{$k$ integers} in $1 \ldots \log(n)$ \pause \textcolor{darkblue}{$\longleftarrow k \log \log(n)$ bits} \pause
			\begin{itemize}
				\item as before with $k$ \textcolor{darkblue}{independent} hash functions \pause
				\item estimate size through (harmonic) \textcolor{darkblue}{mean} of the $k$ counters \pause
				\item asymptotic relative \textcolor{darkblue}{standard deviation} $\sigma/\mu \leq 1.06 / \sqrt{k}$ \pause
				\item $(1 + o(1)) \cdot k \cdot \log \log (n/k)$ bits of \textcolor{darkblue}{space}
				\item join with $k$ independent ``max'' \textcolor{darkblue}{operations}
			\end{itemize}
		\end{block} \pause

		\medskip
		\begin{block}{}
			\[
			\MMM_G(v, h) = \bigcup_{(vu) \in E} \MMM_G(u, h-1)
			\]
			with HyperLogLog counters in $O(nh)$ \textcolor{darkblue}{time} and $O(n \log \log n)$ \textcolor{darkblue}{memory}
		\end{block}
	\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Aggregate HyperANF}

	\begin{frame}{Outline}
		\begin{itemize}
			\item Graph Statistics
			\bigskip
			\item \textcolor{darkblue}{Aggregate HyperANF}
			\begin{itemize}
				\item HyperANF in Field Calculus
				\item Harmonic Centrality and Leader Election
				\item Conclusions
			\end{itemize}
		\end{itemize}
	\end{frame}


\subsection{HyperANF in Field Calculus}

	\begin{frame}{HyperANF for the IoT}
		\begin{block}{Ideas}
			\begin{itemize}
				\item Computation is already local ($h$ HyperLogLog counters per node)
				\[
				c(v, h) = \max_{\text{neighbor~} u} c(u, h-1)
				\] \pause
				\item We only need to accommodate for dynamically changing input \\
				\textcolor{darkblue}{\qquad ${}^{\nwarrow}$ just rewrite it in Field Calculus}
			\end{itemize}
		\end{block} \pause

		\begin{block}{Code}
			\texttt{\small
				\km{def} \fn{HyperANF}(h) \{ \il{(num) $\rightarrow$ (HyperLogLog, list(float))}\\
				\quad \km{if} (h == 0) \{ \\
				\quad \quad \km{let} c = \fn{HyperLogLog}(\pr{uid}) \km{in} [c, \pr{list}(\fn{estimate}(c))] \\
				\quad \} \km{else} \{ \\
				\quad \quad \km{let} r = \fn{HyperANF}(h-1) \km{in} \\
				\quad \quad \km{let} c = \pr{unionhood}( \km{nbr}\{\pr{1st}(r)\} ) \km{in} [c, \pr{cons}(\pr{2nd}(r), \fn{estimate}(c))] \\
				\quad \} \\
				\}
			}
		\end{block}
	\end{frame}

	\begin{frame}{HyperANF for the IoT}
		\begin{block}{Code}
			\texttt{\small
				\km{def} \fn{HyperANF}(h) \{ \il{(num) $\rightarrow$ (HyperLogLog, list(float))}\\
				\quad \km{if} (h == 0) \{ \\
				\quad \quad \km{let} c = \fn{HyperLogLog}(\pr{uid}) \km{in} [c, \pr{list}(\fn{estimate}(c))] \\
				\quad \} \km{else} \{ \\
				\quad \quad \km{let} r = \fn{HyperANF}(h-1) \km{in} \\
				\quad \quad \km{let} c = \pr{unionhood}( \km{nbr}\{\pr{1st}(r)\} ) \km{in} [c, \pr{cons}(\pr{2nd}(r), \fn{estimate}(c))] \\
				\quad \} \\
				\}
			}
		\end{block} \pause

		\begin{block}{}
			\begin{itemize}
				\item Implicitly executed in each node (no \textcolor{darkblue}{$v$} parameter) \pause
				\item Implicit access to neighbours' values (through \textcolor{darkblue}{\texttt{nbr}}) \pause
				\item Implicitly executed \textcolor{darkblue}{iteratively} (handling dynamic changes) \pause
				\item No \textcolor{darkblue}{explicit} adjustments required from original algorithm \pause
				\item Is a new \textcolor{darkblue}{building block}!
			\end{itemize}
		\end{block}
	\end{frame}


\subsection{Harmonic Centrality and Leader Election}

	\begin{frame}{Applications of the HyperANF Building Block}
		\begin{block}{Leader Election}
			\texttt{\small
				\km{def} \fn{HarmonicCentrality}(ng) \{ \\
				\quad \km{if} (\pr{length}(ng) <= 1) \{ 0 \} \km{else} \{ \\
				\quad \quad \pr{head}(ng)/(\pr{length}(ng)-1) + \fn{HarmonicCentrality}(\pr{tail}(ng)) \\
				\quad \} \\
				\} \\
				\onslide<2->{
				\km{def} \fn{LeaderElection}(grain, metric, hmax) \{ \\
				\quad \km{let} c = \fn{HarmonicCentrality}( \pr{2nd}(\fn{HyperANF}(hmax)) ) \km{in} \\
				\quad \fn{BreakUsingUIDs}(c, grain, metric) \\
				\}
				}
			}
		\end{block}

		\begin{itemize}
			\item Recursive computation of harmonic centrality from $N_G$
			\item<2-> Use centrality as tiebreaker in existing leader election algorithms \\ (computed up to a certain \texttt{hmax})
		\end{itemize}
	\end{frame}

%	\begin{frame}[allowframebreaks]{Bibliography}
%		\nocite{hamkins:resurrection_uplifting}
%		\nocite{a:hamkins:boldface_iterated_resurrection}
%		\nocite{hamkins:boldface_resurrection}
%		\nocite{tsaprounis:on_resurrection}
%		\nocite{viale:category_forcing}
%		\nocite{a:viale:resurrection_absoluteness}
%		\nocite{neeman:absoluteness}
%		\nocite{bagaria:bfa_absoluteness}
%		\bibliographystyle{plain}
%		\bibliography{../aux/Bibliography}
%	\end{frame}

\end{document}
